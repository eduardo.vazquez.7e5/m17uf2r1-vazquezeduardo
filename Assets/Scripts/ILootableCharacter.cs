using System.Collections.Generic;
using Managers;
using Scriptable_Objects.DataHolders;
using Scriptable_Objects.Loot;
using UnityEditor;
using UnityEngine;

public interface ILootableCharacter
{
    public int Coins { get; set; }
    public List<LootData> Loot { get; set; }
    public List<int> ProbabilityWeight { get; set; }
    public void DropLoot(GameObject go)
    {
        var playerData = Resources.Load<PlayerData>("ScriptableObjectsInstances/PlayerData");
        playerData!.coins += Coins;
        
        if (Loot.Count > 0)
        {
            int sum = 0;
            foreach (var prob in ProbabilityWeight)
                sum += prob;
            int p = Random.Range(0, sum);
            bool found = ProbabilityWeight.Count == 0;
            int lootIndex = -1;
            while (!found)
            {
                found = p < ProbabilityWeight[++lootIndex];
                p -= ProbabilityWeight[lootIndex];
            }
            if (lootIndex != -1 && Loot[lootIndex] != null)
            {
                var copy = Object.Instantiate(Loot[lootIndex].lootDisplayer, go.GetComponent<Transform>().position, Quaternion.identity);
                copy.GetComponent<LootController>().LoadLootData(Loot[lootIndex]);
            }
        }
    }
}
