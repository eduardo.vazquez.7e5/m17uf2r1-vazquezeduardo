using Classes;
using Scriptable_Objects.Skills;
using Scriptable_Objects.SkillUserAI;
using UnityEngine;
public enum SkillState
{
    Available,
    InUse,
    Cooldown
}

public interface ISkillUser
{
    internal float CurrentEnergy { get; set; }
    internal float MaxEnergy { get; set; }
    internal float TimeToRecharge { get; set; }
    internal float CurrentTimeToRecharge { get; set; }
    internal float EnergyRecharged { get; set; }
    public bool UseSkill { get; set; }
    public SkillUserAI SkillUserAI { get; set; }
    
    internal Skill CurrentSkill { get; set; }
    
    public Vector2 SkillDirection { get; set; }
    public float CurrentAfterImageCooldown { get; set; }
    
    public bool HasFinishedSettingUp { get; set; }
    public GameObject PlaceHolderForSkills { get; set; }

    internal FiniteStateMachine<SkillState> SkillState { get; set; }
            
    public void StartSkillUser(GameObject go)
    {
        CurrentEnergy = MaxEnergy;
        UseSkill = false;
        
        if (CurrentSkill != null)
            LoadSkill(CurrentSkill, go);
    }

    public void LoadSkill(Skill skill, GameObject go)
    {
        CurrentSkill = skill;
        
        HasFinishedSettingUp = false;
        
        SkillState = new FiniteStateMachine<SkillState>(global::SkillState.Available);
        SetUpSkillState(go);
        SkillState.Start();

        HasFinishedSettingUp = true;
    }

    public void UpdateSkillUser(GameObject go)
    {
        if (SkillUserAI != null) 
            SkillUserAI.Calculate(go);
        if(HasFinishedSettingUp)
            SkillState.Update();
        if (CurrentEnergy < MaxEnergy)
            RechargeEnergy();
    }

    private void SetUpSkillState(GameObject go)
    {
        float currentSkillTime = 0f;
        float currentSkillCooldown = 0f;
        
        SkillState.SetNode(
            global::SkillState.Available, 
            () => { }, 
            () => { },
            _ => CurrentEnergy >= CurrentSkill.energyCost && UseSkill,
            _ => global::SkillState.InUse);
        
        SkillState.SetNode(
            global::SkillState.InUse,
            () =>
            {
                currentSkillTime = 0f;
                CurrentEnergy -= CurrentSkill.energyCost;
                CurrentSkill.SkillStart(go);
            },
            () =>
            {
                currentSkillTime += Time.deltaTime;
                CurrentSkill.SkillUpdate(go, currentSkillTime);
            },
            () => CurrentSkill.SkillEnd(go),
            _ => currentSkillTime >= CurrentSkill.skillDuration,
            _ => global::SkillState.Cooldown,
            _ => CurrentSkill.canBeCanceled && UseSkill,
            _ => global::SkillState.Cooldown);
        
        SkillState.SetNode(
            global::SkillState.Cooldown,
            () => currentSkillCooldown = 0f,
            () => currentSkillCooldown += Time.deltaTime,
            _ => currentSkillCooldown >= CurrentSkill.skillCooldown,
            _ => global::SkillState.Available);
    }

    private void RechargeEnergy()
    {
        CurrentTimeToRecharge += Time.deltaTime;
        if (CurrentTimeToRecharge >= TimeToRecharge)
        {
            CurrentTimeToRecharge -= TimeToRecharge;
            CurrentEnergy = Mathf.Min( CurrentEnergy+ EnergyRecharged, MaxEnergy);
        }
        OnEneryRecharge();
    }

    internal void FullEnergyRecharge()
    {
        CurrentEnergy = MaxEnergy;
        OnEneryRecharge();
    }

    public void OnEneryRecharge();
}