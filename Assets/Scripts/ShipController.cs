using System;
using System.Collections.Generic;
using Scriptable_Objects.MovementAI;
using Scriptable_Objects.ShipData;
using UnityEngine;
using ListOfElements = Managers.ListOfElements;

public class ShipController : DestroyableCharacter
{
    private ShipData _shipData;

    private float _acceleration = 4f;
    private float _deceleration = 4f;
    internal float maxSpeed = 10f;
    private float FrictionConstant => maxSpeed > 0.0001f ? _acceleration / maxSpeed : 0f;
    
    private MovementAI[] _movementAis;
    internal float timeToLookInFront = 1.5f;
    internal float distanceFromPlanet = 3f;
    internal float distanceFromPlayer = 2f;

    internal Vector2 orientation;
    internal bool accelerate;
    internal bool decelerate;
    internal float gravityScale;
    
    private float _constCrashDamage = 5f;
    private Animator _animator;
    private static readonly int IsAccelerating = Animator.StringToHash("isAccelerating");
    private bool _hasLoaded;

    protected override void Start()
    {
        _hasLoaded = false;
        var list = ListOfElements.Instance;
        list.listOfShips.Add(this.gameObject);
        base.Start();
        orientation = new Vector2(1, 0);
        accelerate = false;
        decelerate = false;
        gravityScale = 1f;
        _animator = GetComponent<Animator>();
    }

    protected override void Update()
    {
        if (_hasLoaded)
        {
            base.Update();
            MovementControl();
            if(_animator != null)
                _animator.SetBool(IsAccelerating, accelerate);
            OrientShip();
            MoveShip();
        }
    }

    internal override void LoadData(DestroyableCharacterData dcData)
    {
        base.LoadData(dcData);
        if (dcData is ShipData sData)
        {
            _shipData = sData;
            maxHull = _shipData.maxHull;
            currentHull = maxHull;
            _acceleration = _shipData.acceleration;
            _deceleration = _shipData.deceleration;
            maxSpeed = _shipData.maxSpeed;
            GetComponent<Rigidbody2D>().mass = _shipData.mass;
            gravityScale = _shipData.gravityScale;
            if(_shipData.animator != null)
                GetComponent<Animator>().runtimeAnimatorController = _shipData.animator;
            _movementAis = _shipData.movementAIs;
            timeToLookInFront = _shipData.timeToLookInFront;
            distanceFromPlanet = _shipData.distanceFromPlanet;
            distanceFromPlayer = _shipData.distanceFromPlayer;
            _constCrashDamage = _shipData.crashDamage;

            _hasLoaded = true;
        }
    }

    protected virtual void MovementControl()
    {
        if(!rb.isKinematic){
            bool haCalculat = false;
            for (int i = 0; !haCalculat && i < _movementAis.Length; i++)
                haCalculat = _movementAis[i].Calculate(this.gameObject);
        }
    }
        
    protected virtual void OrientShip()
    {
        double zAngle = 0;
        if (orientation != Vector2.zero)
        {
            if (orientation.y > 0)
                zAngle = 180 / Math.PI * Math.Asin(-orientation.x);
            else if (orientation.y < 0)
                zAngle = 180 - 180 / Math.PI * Math.Asin(-orientation.x);
            else if (orientation.x > 0)
                zAngle = 0;
            else
                zAngle = 180;
            rb.transform.eulerAngles = new Vector3(0, 0, (float)zAngle);
        }
    }
    
    private void MoveShip()
    {
        if (!rb.isKinematic)
        {
            Vector2 acc = Vector2.zero;
            if (accelerate)
                acc = _acceleration * orientation;
            else if (decelerate)
                acc = -_deceleration * orientation;
            Vector2 friction = Vector2.ClampMagnitude(-FrictionConstant * rb.velocity, acc.magnitude);
            rb.velocity += (acc + friction) * Time.deltaTime;
        }
    }
    
    public void InelasticCollision(float mass, Vector2 momentum)
    {
        float mass1 = rb.mass;
        rb.velocity = (momentum + rb.velocity * mass1)  / (mass + mass1);
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Ground"))
            InstaKill();
        else if (collision.gameObject.GetComponent<DestroyableCharacter>())
            if(!collision.gameObject.CompareTag(gameObject.tag))
                OnCollisionWithEnemy(collision.gameObject);
    }

    protected virtual void OnCollisionWithEnemy(GameObject enemy)
    {
        enemy.GetComponent<DestroyableCharacter>().Hurt(_constCrashDamage);
    }

    protected virtual void Attack() { }

    protected override void OnDeathDo()
    {
        var list = ListOfElements.Instance;
        list.listOfShips.Remove(this.gameObject);
        base.OnDeathDo();
    }
}