using System;
using Characters;
using Classes;
using Managers;
using Scriptable_Objects.DataHolders;
using Scriptable_Objects.ShipData;
using UnityEditor;
using UnityEngine;
using ListOfElements = Managers.ListOfElements;

public enum ShipState{
    Alive,
    Dazed,
    Stun,
    Dead
}

public class DestroyableCharacter : MonoBehaviour
{
    [SerializeField] protected DestroyableCharacterData dcData;
    public float currentHull;
    public float maxHull = 30f;
    public float magneticPoints = 0f;
    private readonly float _maxMagneticPoints = 30f;
    private readonly float _magneticPointsReduced = 10f;
    private int _rewardPoints = 0;
    
    internal float dazedTime = 1f;
    private readonly float _blinkingTime = 0.05f;
    private readonly float _constDazedTime = 5f;

    protected Rigidbody2D rb;
    protected GameObject onDeathExplosion;

    protected FiniteStateMachine<ShipState> shipState;
    private PlayerData _playerData;

    protected virtual void Start()
    {
        var list = ListOfElements.Instance;
        list.AddCharacter(this.gameObject);
        _playerData = Resources.Load<PlayerData>("ScriptableObjectsInstances/PlayerData");
        EditorUtility.SetDirty(_playerData);
        if (gameObject.CompareTag("Player") && gameObject.GetComponent<Player>() != null) 
            list.AddPlayer(this.gameObject);

        rb = GetComponent<Rigidbody2D>();

        if (dcData != null)
            LoadData(dcData);
    }

    internal virtual void LoadData(DestroyableCharacterData dcData)
    {
        this.dcData = dcData;
        maxHull = dcData.maxHull;
        _rewardPoints = dcData.rewardPoints;
        onDeathExplosion = dcData.onDeathExplosion;
        
        currentHull = maxHull;

        shipState = new FiniteStateMachine<ShipState>(ShipState.Alive);
        SetUpShipState();
        shipState.Start();
    }

    protected virtual void Update()
    {
        if(shipState != null)
            shipState.Update();
    }

    void SetUpShipState()
    {
        float currentDazedTime = 0f;
        float currentBlinkingTime = 0f;
    
        shipState.SetNode(
            ShipState.Alive, 
            () => { },
            () => { },
            _ => currentHull <=0,
            _ => ShipState.Dead,
            _ => magneticPoints >= _maxMagneticPoints,
            _ => ShipState.Stun);
        
        shipState.SetNode(
            ShipState.Dazed,
            () =>
            {
                currentDazedTime = 0f;
                currentBlinkingTime = 0f;
                GetComponent<SpriteRenderer>().enabled = false;
            },
            () => UpdateDazed(ref currentDazedTime, ref currentBlinkingTime, _blinkingTime),
            () => GetComponent<SpriteRenderer>().enabled = true,
            _ => currentDazedTime >= dazedTime,
            _ => ShipState.Alive);
        
        shipState.SetNode(
            ShipState.Stun,
            () =>
            {
                rb.isKinematic = true;
                currentDazedTime = 0f;
            },
            () =>
            {
                magneticPoints -= _magneticPointsReduced * Time.deltaTime;
                currentDazedTime += Time.deltaTime;
                if (GetComponent<ShipController>())
                {
                    float angle = (float) (6 * Math.PI * currentDazedTime);
                    Vector2 orientation = new Vector2((float) Math.Cos(angle), (float) Math.Sin(angle));
                    GetComponent<ShipController>().orientation = orientation;
                    
                }
            },
            () => rb.isKinematic = false,
            _ => magneticPoints<= 0,
            _ => ShipState.Alive);
        
        shipState.SetNode(
            ShipState.Dead,
            OnDeathDo,
            () => { });
    }

    private void UpdateDazed(ref float currentDazedTime, ref float currentBlinkingTime, float blinkingTime)
    {
        currentDazedTime += Time.deltaTime;
        currentBlinkingTime += Time.deltaTime;
        if (currentBlinkingTime >= blinkingTime)
        {
            currentBlinkingTime -= blinkingTime;
            GetComponent<SpriteRenderer>().enabled = ! GetComponent<SpriteRenderer>().enabled;
        }
    }

    protected virtual void OnDeathDo()
    {
        Instantiate(onDeathExplosion, GetComponent<Transform>().position, new Quaternion(0, 0, 0, 0));
        if (this is ILootableCharacter)
            (this as ILootableCharacter)!.DropLoot(this.gameObject);
        if (_rewardPoints > 0)
        {
            _playerData.points += _rewardPoints;
            EventManager.CallOnPlayerPointChange();
        }
        var list = ListOfElements.Instance;
        list.RemoveCharacter(this.gameObject);
        EventManager.CallOnCharacterDestroyed();
        Destroy(this.gameObject);
    }

    public virtual void Hurt(float damage)
    {
        if (shipState.GetCurrentState() != ShipState.Dazed && shipState.GetCurrentState() != ShipState.Dead)
        {
            currentHull -= damage;
            if(currentHull > 0f)
            {
                dazedTime = _constDazedTime * damage / maxHull;
                shipState.SetTrigger(ShipState.Alive, ShipState.Dazed);
            }
        }
    }

    public virtual void Heal(float heal)
    {
        if (shipState.GetCurrentState() != ShipState.Dead)
        {
            currentHull = Mathf.Min(currentHull+heal, maxHull);
        }
    }

    internal virtual void InstaKill()
    {
        currentHull = -1f;
        shipState.SetTrigger(shipState.GetCurrentState(),ShipState.Dead);
    }
}
