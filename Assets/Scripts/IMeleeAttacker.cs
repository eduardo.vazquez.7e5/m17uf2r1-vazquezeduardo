using System.Collections.Generic;
using Characters;
using Classes;
using Managers;
using Scriptable_Objects.Weapons;
using UnityEngine;

public enum AttackState
{
    Available,
    Cooldown
}

public interface IMelleeAttacker
{
    internal MeleeWeapon MeleeWeapon { get; set; }

    internal FiniteStateMachine<AttackState> AttackState { get; set; }
    
    internal List<GameObject> WeaponInstances { get; set; }

    public void LoadWeapon(MeleeWeapon weapon, GameObject go)
    {
        MeleeWeapon = weapon;
        go.GetComponent<AudioSource>().clip = weapon.attackingSound;
        
        AttackState = new FiniteStateMachine<AttackState>(global::AttackState.Cooldown);
        SetUpAttackState();
        AttackState.Start();
        
        MeleeWeapon.SetUp(go);
    }

    public void UpdateMelee()
    {
        if (AttackState != null)
            AttackState.Update();
    }

    void SetUpAttackState()
    {
        float currentCoolDownTime = 0f;
        
        AttackState.SetNode(
            global::AttackState.Available,
            () => { },
            () => { });
        
        AttackState.SetNode(
            global::AttackState.Cooldown,
            () => currentCoolDownTime = 0f,
            () => currentCoolDownTime += Time.deltaTime,
            _ => currentCoolDownTime >= MeleeWeapon.cooldownTime,
            _ => global::AttackState.Available);
    }

    public virtual void MeleeAttack(GameObject go)
    {
        if (AttackState != null && AttackState.GetCurrentState() == global::AttackState.Available)
        {
            MeleeWeapon.Attack(go);
            if(this is Player player)
                EventManager.CallOnPlayerHasShot();
            AttackState.SetTrigger(global::AttackState.Available, global::AttackState.Cooldown);
        }
    }

    public virtual void End(GameObject go)
    {
        MeleeWeapon.End(go);
    }
}