using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Characters;
using Managers;
using Scriptable_Objects;
using Scriptable_Objects.DataHolders;
using UnityEditor.Tilemaps;
using UnityEngine;
using ListOfElements = Managers.ListOfElements;
using Random = System.Random;

public class BackgroundScript : MonoBehaviour
{
    [SerializeField] private Sprite backgroundSprite;
    private int _rows;
    private int _cols;
    private float _spriteWidth;
    private float _spriteHeight;
    private float _radiusOfLight = 1f;
    private float _minOpacity = 0.8f;
    private List<SpriteRenderer> _listOfTiles;
    private List<GameObject> _listOfPlanets;
    private Transform _tr;
    [SerializeField] private GameObject backgroundTilePrefab;
    void Start()
    { 
        Camera cam = Camera.main;
        float camHeight = 2f * cam.orthographicSize;
        float camWidth = camHeight * cam.aspect;
        _spriteWidth = backgroundSprite.bounds.size.x;
        _spriteHeight = backgroundSprite.bounds.size.y;
        _rows = (int) Math.Floor(camHeight / _spriteHeight + 0.5f) + 2;
        _cols = (int) Math.Floor(camWidth / _spriteWidth + 0.5f) + 2;
        _listOfTiles = new List<SpriteRenderer>();
        for(int i = 0 ; i < _rows; i++)
        for (int j = 0; j < _cols; j++)
            _listOfTiles.Add(CreateTile(i, j));

        _tr = GetComponent<Transform>();
        _listOfPlanets = ListOfElements.Instance.listOfPlanets;

        EventManager.OnPlanetChange += UpdateBackgroundLightning;
        UpdateBackgroundPosition();
    }

    // Update is called once per frame
    void Update()
    {
        UpdateBackgroundPosition();
    }

    private void OnDisable()
    {
        EventManager.OnPlanetChange -= UpdateBackgroundLightning;
    }

    private SpriteRenderer CreateTile(int i, int j)
    {
        var copy = Instantiate(backgroundTilePrefab, GetComponent<Transform>());
        copy.GetComponent<Transform>().position += 
            new Vector3(
                _spriteWidth * (0.5f - _cols / 2f + j), 
                _spriteHeight * (0.5f - _rows / 2f + i), 
                0);
        SpriteRenderer sr = copy.GetComponent<SpriteRenderer>();
        sr.sprite = backgroundSprite;
        return sr;
    }

    public void UpdateBackgroundPosition()
    {
        var list = ListOfElements.Instance;
        if (list.player != null)
        {
            Vector2 pos = list.player.GetComponent<Transform>().position;
            float displacementX = (pos.x - _tr.position.x) / _spriteWidth;
            float displacementY = (pos.y - _tr.position.y) / _spriteHeight;
            if (Math.Abs(displacementX) > 0.75f || Math.Abs(displacementY) > 0.75f)
            {
                int dispX = (int) Math.Floor(displacementX + 0.5f);
                int dispY = (int) Math.Floor(displacementY + 0.5f);
                _tr.position += new Vector3(_spriteWidth*dispX, _spriteHeight*dispY, 0);
            }
        }
        UpdateBackgroundLightning();
    }

    private void UpdateBackgroundLightning()
    {
        foreach (var tile in _listOfTiles)
        {
            float luminosity = _listOfPlanets.Any() ? 0f : 0.35f;
            foreach (var moon in _listOfPlanets)
            {
                Vector3 moonPos = moon.GetComponent<Transform>().position;
                Vector3 tilePos = tile.GetComponent<Transform>().position;
                float dist = new Vector2(tilePos.x-moonPos.x, tilePos.y-moonPos.y).magnitude;
                luminosity += moon.GetComponent<PlanetController>().luminosity / dist;
            }
            SpriteRenderer sr = tile.GetComponent<SpriteRenderer>();
            sr.color = GetColorFromLuminosity(luminosity);
        }
    }

    private Color GetColorFromLuminosity(float luminosity)
    {
        if (luminosity > _radiusOfLight)
        {
            float opacity =  (1-_minOpacity) * _radiusOfLight / luminosity + _minOpacity;
            return new Color(1f, 1f, 1f, opacity);
        }
        float blackness = luminosity / _radiusOfLight;
        return new Color(blackness, blackness, blackness, 1f);
    }
}