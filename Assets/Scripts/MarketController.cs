using System.Collections.Generic;
using Managers;
using Scriptable_Objects.DataHolders;
using Scriptable_Objects.Market;
using UnityEngine;
using UnityEngine.UI;

public class MarketController : MonoBehaviour
{
    [SerializeField] private List<GameObject> offerDisplayers;
    [SerializeField] private List<Offer> offersData;

    private List<int> _selectedOffers;
    
    internal void RandomizeShop()
    {
        _selectedOffers = new List<int>();
        while (_selectedOffers.Count < 5)
        {
            int newOffer;
            do
            {
                newOffer = Random.Range(0, offersData.Count);
            } while (_selectedOffers.Contains(newOffer));
            _selectedOffers.Add(newOffer);
        }
        foreach(var offer in _selectedOffers)
            offersData[offer].SetUp();
        
        UpdateOffers();
        UpdateAvailability();
    }

    public void UpdateOffers()
    {
        var playerData = Resources.Load<PlayerData>("ScriptableObjectsInstances/PlayerData");
        for(int i =0; i<offerDisplayers.Count; i++)
        {
            Offer offer = offersData[_selectedOffers[i]];
            var displayer = offerDisplayers[i];
            displayer.GetComponent<Image>().sprite = offer.banner;
            var textico = displayer.GetComponent<Transform>().Find("text").GetComponent<Text>();
            if (!playerData.hasTranslator || offer.englishText == "")
                textico.text = offer.titleDefault;
            else 
                textico.text = offer.titleEnglish;
            textico.color = offer.textColor;
        }
    }

    public void UpdateAvailability()
    {
        for(int i =0; i<offerDisplayers.Count; i++)
        {
            Offer offer = offersData[_selectedOffers[i]];
            var displayer = offerDisplayers[i];
            if (offer.IsAvailable())
                displayer.GetComponent<Image>().color = Color.white;
            else
                displayer.GetComponent<Image>().color = Color.red;
        }
    }
    
    void OnClickOffer(int i)
    {
        if (offersData[_selectedOffers[i]].IsAvailable())
        {
            var playerData = Resources.Load<PlayerData>("ScriptableObjectsInstances/PlayerData");
            UIManager.Instance.offerPanel.GetComponent<OfferPanelController>()
                .LoadOffer(offersData[_selectedOffers[i]], playerData.hasTranslator);
            UIManager.Instance.ToggleShop();
            UIManager.Instance.ToggleOffer();
        }
    }
    
    public void FirstOnClick() { OnClickOffer(0);}
    public void SecondOnClick() { OnClickOffer(1);}
    public void ThirdOnClick() { OnClickOffer(2);}
    public void FourthOnClick() { OnClickOffer(3);}
    public void FifthOnClick() { OnClickOffer(4);}
}
