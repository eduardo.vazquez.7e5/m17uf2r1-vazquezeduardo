using System.Collections.Generic;
using Classes;
using JetBrains.Annotations;
using UnityEngine;

namespace Managers
{
    public class ListOfElements : Singleton<ListOfElements>
    {
        [CanBeNull] public GameObject player;
        public List<GameObject> listOfCharacters;
        public List<GameObject> listOfShips;
        public List<GameObject> listOfMeteorites;
        public List<GameObject> listOfPlanets;

        protected override void Awake()
        {
            base.Awake();
            ResetValues();
        }

        public void ResetValues()
        {
            player = null;
            listOfCharacters = new List<GameObject>();
            listOfShips = new List<GameObject>();
            listOfMeteorites = new List<GameObject>();
            listOfPlanets = new List<GameObject>();
        }

        public void AddCharacter(GameObject go)
        {
            listOfCharacters.Add(go);
        }
        
        public void RemoveCharacter(GameObject go)
        {
            listOfCharacters.Remove(go);
        }

        public void AddShip(GameObject go)
        {
            listOfShips.Add(go);
        }

        public void RemoveShip(GameObject go)
        {
            listOfShips.Remove(go);
        }

        public void AddPlayer(GameObject go)
        {
            if (player == null)
                player = go;
        }
        
        public void AddPlanet(GameObject go)
        {
            listOfPlanets.Add(go);
            EventManager.CallOnPlanetChange();
        }

        public void RemovePlanet(GameObject go)
        {
            listOfPlanets.Remove(go);
            EventManager.CallOnPlanetChange();
        }
    }
}
