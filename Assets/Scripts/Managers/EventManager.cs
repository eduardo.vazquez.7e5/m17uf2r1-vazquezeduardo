using UnityEngine;

namespace Managers
{
    public class EventManager : Classes.Singleton<EventManager>
    {
        public delegate void PlanetChange();
        public static event PlanetChange OnPlanetChange;

        public delegate void CharacterDestroyed();
        public static event CharacterDestroyed OnCharacterDestroyed;

        public delegate void PlayerHealthChange();
        public static event PlayerHealthChange OnPlayerHealthChange;

        public delegate void PlayerEnergyChange();
        public static event PlayerEnergyChange OnPlayerEnergyChange;

        public delegate void PlayerWeaponChange();
        public static event PlayerWeaponChange OnPlayerWeaponChange;

        public delegate void PlayerSkillChange();

        public static event PlayerSkillChange OnPlayerSkillChange;

        public delegate void PlayerPointChange();

        public static event PlayerPointChange OnPlayerPointChange;

        public delegate void PlayerEntersBlackHole();

        public static event PlayerEntersBlackHole OnPlayerEntersBlackHole;

        public delegate void PlayerHasShot();

        public static event PlayerHasShot OnPlayerHasShot;

        public static void CallOnPlanetChange()
        {
            if (OnPlanetChange != null)
                OnPlanetChange();
        }

        public static void CallOnCharacterDestroyed()
        {
            if (OnCharacterDestroyed != null)
                OnCharacterDestroyed();
        }

        public static void CallOnPlayerHealthChange()
        {
            if (OnPlayerHealthChange != null)
                OnPlayerHealthChange();
        }

        public static void CallOnPlayerEnergyChange()
        {
            if (OnPlayerEnergyChange != null)
                OnPlayerEnergyChange();
        }

        public static void CallOnPlayerWeaponChange()
        {
            if (OnPlayerWeaponChange != null)
                OnPlayerWeaponChange();
        }

        public static void CallOnPlayerSkillChange()
        {
            if (OnPlayerSkillChange != null)
                OnPlayerSkillChange();
        }

        public static void CallOnPlayerPointChange()
        {
            if (OnPlayerPointChange != null)
                OnPlayerPointChange();
        }

        public static void CallOnPlayerEntersBlackHole()
        {
            if (OnPlayerEntersBlackHole != null)
                OnPlayerEntersBlackHole();
        }

        public static void CallOnPlayerHasShot()
        {
            if (OnPlayerHasShot != null)
                OnPlayerHasShot();
        }
    }
}
