using System;
using Classes;
using UnityEngine;

namespace Managers
{
    public class AudioManager : Singleton<AudioManager>
    {
        public Sound[] sounds;

        protected override void Awake()
        {
            DontDestroyOnLoad(this.gameObject);
            foreach (var sound in sounds)
            {
                sound.source = gameObject.AddComponent<AudioSource>();
                sound.source.clip = sound.clip;

                sound.source.volume = sound.volume;
                sound.source.pitch = sound.pitch;

                sound.source.outputAudioMixerGroup = sound.output;
            }
        }

        public void Play(string name, bool loop)
        {
            Sound sound = Array.Find(sounds, sound => sound.name == name);
            if (sound != null)
            {
                sound.source.loop = loop;
                sound.source.Play();
            }
        }
    }
}
