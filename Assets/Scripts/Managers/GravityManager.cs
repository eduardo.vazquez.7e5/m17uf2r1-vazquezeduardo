using Characters;
using Classes;
using UnityEngine;
using Vector2 = UnityEngine.Vector2;
using Vector3 = UnityEngine.Vector3;

namespace Managers
{
    public class GravityManager : Singleton<GravityManager>
    {
        private void Update()
        {
            ApplyGravity();
        }

        void ApplyGravity()
        {
            var list = ListOfElements.Instance;
            foreach (var planet in list.listOfPlanets)
            {
                PlanetController pc = planet.GetComponent<PlanetController>();
                for(int i=0; i< list.listOfShips.Count; i++)
                {
                    if(list.listOfShips[i] == null)
                        list.listOfShips.RemoveAt(i--);
                    else
                    {
                        GameObject ship = list.listOfShips[i];
                        Vector3 distance = ship.GetComponent<Transform>().position -
                                           planet.GetComponent<Transform>().position;
                        Vector2 dist2 = new Vector2(distance.x, distance.y);
                        if (dist2.magnitude < pc.minDistance)
                            dist2 = dist2.normalized * pc.minDistance;
                        Vector2 force = -pc.gravity * dist2 * ship.GetComponent<ShipController>().gravityScale * Mathf.Pow(dist2.magnitude, pc.power - 1);
                        ship.GetComponent<Rigidbody2D>().velocity += force * Time.deltaTime;
                    }
                }
            }
        }
    }
}