using System.Collections.Generic;
using Classes;
using SceneController;
using Scriptable_Objects;
using Scriptable_Objects.DataHolders;
using Scriptable_Objects.SceneData;
using UnityEditor;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.SceneManagement;

namespace Managers
{
    public enum GameState
    {
        Loading,
        Active,
        Paused,
        Lost,
        Won,
        InShop
    }
    
    public class GameManager : Singleton<GameManager>
    {

        [SerializeField] private Texture2D customCursor;
        [SerializeField] private OrderedScenesCollectorScriptableObject scenesCollector;
        
        private int _currentScene;
        private GameObject _currentController;
        private bool _isRunningAnScene;

        private List<SceneData> _listOfScenes;
        private ListOfElements _listOfElements;
        private Inventory _inventory;
        private PlayerData _playerData;

        private GameState _gameState;
        public bool IsGamePaused => _gameState == GameState.Paused;
        public bool IsGameActive => _gameState == GameState.Active;
        public bool IsGameInShop => _gameState == GameState.InShop;

        private float _timeToResultScreen = 3f;
        private float _currentTimeToResultScreen;
        
        void Start()
        {
            _gameState = GameState.Loading;
            _listOfScenes = scenesCollector.listOfScenes;
            Cursor.SetCursor(customCursor, Vector2.zero,CursorMode.ForceSoftware);
            _listOfElements = ListOfElements.Instance;
            _inventory = Resources.Load<Inventory>("ScriptableObjectsInstances/Inventory");
            _playerData = Resources.Load<PlayerData>("ScriptableObjectsInstances/PlayerData");
            EditorUtility.SetDirty(_playerData);
            _currentScene = 0;
            _currentController = null;
            _isRunningAnScene = false;
            _currentTimeToResultScreen = 0;
            _gameState = GameState.Active;
        }
        
        void Update()
        {
            if (_gameState != GameState.Loading)
            {
                if (_currentController == null)
                    _isRunningAnScene = false;
                if (!_isRunningAnScene && _gameState == GameState.Active)
                {
                    _isRunningAnScene = true;
                    if (_currentScene < _listOfScenes.Count)
                    {
                        if (_listOfScenes[_currentScene] is TutorialData)
                        {
                            _currentController = Instantiate(scenesCollector.tutorialController, 
                                GetComponent<Transform>().position,
                                Quaternion.identity);
                            _currentController.GetComponent<SceneController.SceneController>()
                                .LoadData(_listOfScenes[_currentScene]);
                        }
                        else if (_listOfScenes[_currentScene] is TimedSceneData)
                        {
                            _currentController = Instantiate(scenesCollector.timedController, 
                                GetComponent<Transform>().position,
                                Quaternion.identity);
                            _currentController.GetComponent<SceneController.SceneController>()
                                .LoadData(_listOfScenes[_currentScene]);
                        }
                        else if (_listOfScenes[_currentScene] is ScriptedWaveData)
                        {
                            _currentController = Instantiate(scenesCollector.scriptedWaveController, 
                                GetComponent<Transform>().position,
                                Quaternion.identity);
                            _currentController.GetComponent<SceneController.SceneController>()
                                .LoadData(_listOfScenes[_currentScene]);
                        }
                        else if (_listOfScenes[_currentScene] is RandomWaveData)
                        {
                            _currentController = Instantiate(scenesCollector.randomWaveController, 
                                GetComponent<Transform>().position,
                                Quaternion.identity);
                            _currentController.GetComponent<SceneController.SceneController>()
                                .LoadData(_listOfScenes[_currentScene]);
                        }

                        _currentScene++;
                    }
                }

            }

            if (_gameState == GameState.Won || _gameState == GameState.Lost)
            {
                _currentTimeToResultScreen += Time.deltaTime;
                if (_currentTimeToResultScreen >= _timeToResultScreen)
                    SceneManager.LoadScene(2);
            }
        }

        public void PauseGame()
        {
            Time.timeScale = 0f;
            _gameState = GameState.Paused;
        }

        public void UnPauseGame()
        {
            Time.timeScale = 1f;
            _gameState = GameState.Active;
        }

        public void OpenShop()
        {
            Time.timeScale = 0f;
            _gameState = GameState.InShop;
        }

        public void CloseShop()
        {
            Time.timeScale = 1f;
            _gameState = GameState.Active;
        }

        public void LoseGame()
        {
            _gameState = GameState.Lost;
            _playerData.gameResult = GameResult.Defeat;
        }

        public void WinGame()
        {
            _gameState = GameState.Won;
            _playerData.gameResult = GameResult.Victory;
        }
    }
}
