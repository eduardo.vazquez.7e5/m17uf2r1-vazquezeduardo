using System;
using System.Collections.Generic;
using Characters;
using Classes;
using Scriptable_Objects;
using Scriptable_Objects.DataHolders;
using Scriptable_Objects.Weapons;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.Serialization;
using UnityEngine.UI;
using Object = UnityEngine.Object;

namespace Managers
{
    public class UIManager : Singleton<UIManager>
    {
        [SerializeField] private GameObject healthback;
        [SerializeField] private GameObject healthbar;
        [SerializeField] private GameObject healthborder;
        [SerializeField] private GameObject energyback;
        [SerializeField] private GameObject energybar;
        [SerializeField] private GameObject energyborder;
        [SerializeField] private GameObject skillDisplayer;
        [SerializeField] private GameObject bullets;
        [SerializeField] private GameObject bulletPrefab;
        [SerializeField] private GameObject ammoDisplayer;
        [SerializeField] private GameObject uiInventory;
        [SerializeField] private GameObject weaponUiDisplayer;
        [SerializeField] private GameObject uiChat;
        [SerializeField] private GameObject chatDisplayer;
        [SerializeField] private GameObject pausePanel;
        [SerializeField] private GameObject playerNameDisplayer;
        [SerializeField] private GameObject pointsDisplayer;
        [SerializeField] private GameObject coinsDisplayer;
        [SerializeField] private GameObject avatarDisplayer;
        [SerializeField] public GameObject shopPanel;
        [SerializeField] public GameObject powerupPanel;
        [SerializeField] public GameObject offerPanel;

        private ListOfElements _listOfElements;
        private Inventory _inventory;
        private PlayerData _playerData;
        private List<GameObject> _listOfChats = new List<GameObject>();

        void Start()
        {
            _listOfElements = ListOfElements.Instance;
            _inventory = Resources.Load<Inventory>("ScriptableObjectsInstances/Inventory");
            _playerData = Resources.Load<PlayerData>("ScriptableObjectsInstances/PlayerData");
            playerNameDisplayer.GetComponent<Text>().text = _playerData.playerName;
            EventManager.OnPlayerHealthChange += UpdateHealthBar;
            EventManager.OnPlayerEnergyChange += UpdateEnergyBar;
            EventManager.OnPlayerWeaponChange += UpdateWeaponDisplayer;
            EventManager.OnPlayerSkillChange += UpdateSkillDisplayer;
            EventManager.OnPlayerPointChange += UpdatePointsDisplayer;
            UpdateHealthBar();
            UpdateEnergyBar();
            UpdateWeaponDisplayer();
            UpdateSkillDisplayer();
            UpdatePlayerInfoDisplayer();
            UpdatePointsDisplayer();
        }

        void Update()
        {
            UpdateBulletDisplayer();
        }

        private void OnDestroy()
        {
            EventManager.OnPlayerHealthChange -= UpdateHealthBar;
            EventManager.OnPlayerEnergyChange -= UpdateEnergyBar;
            EventManager.OnPlayerWeaponChange -= UpdateWeaponDisplayer;
            EventManager.OnPlayerSkillChange -= UpdateSkillDisplayer;
            EventManager.OnPlayerPointChange -= UpdatePointsDisplayer;
        }

        void UpdateHealthBar()
        {
            if (healthbar != null && _listOfElements.player != null)
            {
                DestroyableCharacter ds = _listOfElements.player.GetComponent<DestroyableCharacter>();
                float proportion = Mathf.Clamp(ds.currentHull / ds.maxHull, 0, 1);
                float containerProportion = Mathf.Clamp(1f-20f/(ds.maxHull-40f), 0.5f, 1f);
                healthback.GetComponent<RectTransform>().SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, 292 * containerProportion);
                healthborder.GetComponent<RectTransform>().SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, 296 * containerProportion);
                healthbar.GetComponent<RectTransform>().SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, 292 * proportion * containerProportion - 3 );
                DestroyableCharacter health = _listOfElements.player.GetComponent<DestroyableCharacter>();
                if (health.currentHull >= 0.65 * health.maxHull)
                    avatarDisplayer.GetComponent<Image>().sprite = _playerData.GetAvatar(PlayerState.Confident);
                else if(health.currentHull >= 0.35 * health.maxHull)
                    avatarDisplayer.GetComponent<Image>().sprite = _playerData.GetAvatar(PlayerState.Worried);
                else if (health.currentHull > 0f)
                    avatarDisplayer.GetComponent<Image>().sprite = _playerData.GetAvatar(PlayerState.VeryWorried);
                else 
                    avatarDisplayer.GetComponent<Image>().sprite = _playerData.GetAvatar(PlayerState.Defeated);
            }
        }

        void UpdateEnergyBar()
        {
            if (energybar != null && _listOfElements.player != null)
            {
                Player pl = _listOfElements.player.GetComponent<Player>();
                ISkillUser su = pl.GetComponent<ISkillUser>();
                float proportion = Mathf.Clamp(su.CurrentEnergy / su.MaxEnergy, 0, 1);
                float containerProportion = Mathf.Clamp(1f - 20f / (su.MaxEnergy - 40f), 0.5f, 1f);
                energyback.GetComponent<RectTransform>().SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, containerProportion * 265f);
                energyborder.GetComponent<RectTransform>().SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, containerProportion * 270f);
                energybar.GetComponent<RectTransform>().SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal,proportion * containerProportion * 260f - 2f);
            }
        }

        void UpdateBulletDisplayer()
        {
            if (_listOfElements.player != null && bullets != null)
            {
                EraseAllBullets();
                Player pl = _listOfElements.player.GetComponent<Player>();
                if (pl.inventory != null && pl.inventory.weapons[pl.weaponIndex] is RangedWeapon)
                {
                    Vector3 position = new Vector3(-bullets.GetComponent<RectTransform>().sizeDelta.x / 2,
                        -bullets.GetComponent<RectTransform>().sizeDelta.y / 2, 0f);
                    int currentBullet = 1;
                    float marginBetWeenBullets = 7.5f;
                    float marginBetweenBursts = 5f;
                    for (int i = 0; i < pl.burstByMagazine; i++)
                    {
                        for (int j = 0; j < pl.bulletsByBurst; j++)
                        {
                            var copy = Instantiate(bulletPrefab, bullets.GetComponent<RectTransform>());
                            copy.GetComponent<RectTransform>().localPosition = position;
                            if (currentBullet > pl.CurrentBulletsByMagazine)
                                copy.GetComponent<RawImage>().color = Color.gray;
                            position += new Vector3(marginBetWeenBullets, 0f, 0f);
                            currentBullet++;
                        }

                        position += new Vector3(marginBetweenBursts, 0f, 0f);
                    }

                    Weapon weapon = _listOfElements.player.GetComponent<Player>().weapon;
                    if (weapon is Firearm)
                    {
                        var copy = Instantiate(ammoDisplayer, bullets.GetComponent<RectTransform>());
                        copy.GetComponent<RectTransform>().localPosition = position;
                        copy.GetComponent<Text>().text = "x" + _inventory.GetAmmunition(weapon);
                    }
                }
            }
        }

        void UpdateWeaponDisplayer()
        {
            EraseAllWeapons();
            var Player = _listOfElements.player;
            int weaponIndex = 0;
            if (Player != null)
                weaponIndex = Player.GetComponent<Player>().weaponIndex;
            float width = weaponUiDisplayer.GetComponent<RectTransform>().sizeDelta.x;
            float separation = 10f;
            float offset = -(_inventory.weapons.Count - 1f) * (width + separation) / 2;
            for(int i =0; i<_inventory.weapons.Count; i++)
            {
                var copy = Instantiate(weaponUiDisplayer, uiInventory.GetComponent<RectTransform>());
                copy.GetComponent<RectTransform>().position += new Vector3(offset+(width+separation)*i, 0, 0);
                copy.GetComponent<Image>().sprite = _inventory.weapons[i].weaponSprite;
                if (i != weaponIndex)
                    copy.GetComponent<Image>().color = new Color(0.5f, 0.5f, 0.5f);
            }
        }

        void UpdateSkillDisplayer()
        {
            Image image = skillDisplayer.GetComponent<Image>();
            if (_inventory.skill == null)
                image.enabled = false;
            else
            {
                image.enabled = true;
                image.sprite = _inventory.skill.inventoryIcon;
            }
        }

        void UpdatePlayerInfoDisplayer()
        {
            if (playerNameDisplayer != null)
            {
                Text nameText = playerNameDisplayer.GetComponent<Text>();
                nameText.text = _playerData.playerName;
            }
        }

        void UpdatePointsDisplayer()
        {
            if (coinsDisplayer != null)
            {
                Text coinsText = coinsDisplayer.GetComponent<Text>();
                coinsText.text = _playerData.coins + " $";
            }
            if (pointsDisplayer != null)
            {
                Text pointsText = pointsDisplayer.GetComponent<Text>();
                pointsText.text = _playerData.points + "pts";
            }
        }

        public void PublicMessage(string s)
        {
            float margin = 5f;
            var copy = Instantiate(chatDisplayer, uiChat.GetComponent<RectTransform>());
            copy.GetComponent<Text>().text = s;
            float displ = margin + copy.GetComponent<RectTransform>().sizeDelta.y;
            foreach (var chat in _listOfChats)
                chat.GetComponent<RectTransform>().position += new Vector3(0, displ);
            while(_listOfChats.Count > 6)
                _listOfChats.RemoveAt(0);
            _listOfChats.Add(copy);
        }

        private void TogglePausePanel()
        {
            if(_listOfElements.player != null && GameManager.Instance.IsGameActive)
                GameManager.Instance.PauseGame();
            else if(GameManager.Instance.IsGamePaused)
                GameManager.Instance.UnPauseGame();
        }

        void EraseAllBullets()
        {
            for (var i = bullets.transform.childCount - 1; i >= 0; i--)
                Object.Destroy(bullets.transform.GetChild(i).gameObject);
        }

        void EraseAllWeapons()
        {
            for (var i = uiInventory.transform.childCount - 1; i >= 0; i--)
                Object.Destroy(uiInventory.transform.GetChild(i).gameObject);
        }

        public void Pause()
        {
            TogglePausePanel();
            pausePanel.SetActive(GameManager.Instance.IsGamePaused);
        }

        public void ToggleShopPanel()
        {
            if(_listOfElements.player != null && GameManager.Instance.IsGameActive)
                GameManager.Instance.OpenShop();
            else if(GameManager.Instance.IsGameInShop)
                GameManager.Instance.CloseShop();
        }
        public void ToggleShop()
        {
            ToggleShopPanel();
            shopPanel.SetActive(GameManager.Instance.IsGameInShop);
        }

        public void TogglePowerUp()
        {
            ToggleShopPanel();
            powerupPanel.SetActive(GameManager.Instance.IsGameInShop);
        }

        public void ToggleOffer()
        {
            ToggleShopPanel();
            offerPanel.SetActive(GameManager.Instance.IsGameInShop);
        }
    }
}
