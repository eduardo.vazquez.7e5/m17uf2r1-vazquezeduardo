using System;
using Classes;
using Scriptable_Objects;
using UnityEngine;

public class OrbController : MonoBehaviour
{
    [SerializeField] private OrbScriptableObject orbData;
    private float _damage;
    private float _frequency = 0f;
    private float _xRadius = 0f;
    private float _yRadius = 0f;
    private float _offset = 0f;
    private float _rotato = 0f;

    private float _attackDistance;
    private float _attackTime;
    private float _sizeScale;
    private float _constantPush;
    private bool _rotatesLeft;

    public string _ownerTag;
    private GameObject _parent;

    private float _currentTime;
    private Transform _tr;

    public bool attack;

    private FiniteStateMachine<OrbState> _orbState;
    public enum OrbState
    {
        Orbiting, // get it? hehe
        Attacking
    }
    void Start()
    {
        _currentTime = 0f;
        _tr = GetComponent<Transform>();
    }

    // offset and rotation in degrees!! frequency in rev / s (= Hz)
    public void LoadOrb(OrbScriptableObject oData, GameObject parent)
    {
        _damage = oData.damage;
        _frequency = oData.frequency * 2*Mathf.PI;
        _xRadius = oData.xRadius;
        _yRadius = oData.yRadius;
        _offset = oData.offset * Mathf.PI/180;
        _rotato = oData.rotation * Mathf.PI/180;

        _attackDistance = oData.attackDistance;
        _attackTime = oData.attackTime;
        _sizeScale = oData.sizeScale;
        _constantPush = oData.constantPush;

        _parent = parent;
        _ownerTag = parent.tag;
        
        attack = false;

        _orbState = new FiniteStateMachine<OrbState>(OrbState.Orbiting);
        SetUpOrbState();
        _orbState.Start();
    }

    public void SetAngles(float offset, float rotation)
    {
        _offset = offset * Mathf.PI/180;
        _rotato = rotation * Mathf.PI/180;
    }

    void Update()
    {
        if (_orbState != null)
        {
            _currentTime += Time.deltaTime;
            _orbState.Update();
        }
    }

    Vector3 GetOrbitingPosition()
    {
        return Rotate(_rotato,
                new Vector3(
                        _xRadius * Mathf.Cos(_frequency * _currentTime + _offset),
                        _yRadius * Mathf.Sin(_frequency * _currentTime + _offset), 
                        0f)
                );
    }

    Vector3 GetAttackingPosition(float current)
    {
        return new Vector3(
            Mathf.Sign(_frequency) * 2 * _xRadius * current / _attackTime,
            _attackDistance - 4 * _attackDistance * current * current / _attackTime,
            0);
    }

    Vector3 GetMixedPosition(float current)
    {
        if (Math.Abs(current) <= _attackTime / 4) return GetAttackingPosition(current);
        float prop = GetAttackProportion(current);
        return prop * GetAttackingPosition(current) + (1 - prop) * GetOrbitingPosition();
    }

    float GetAttackProportion(float current)
    {
        if (Math.Abs(current) <= _attackTime / 4) return 1;
        if (current < -_attackTime / 4) return 2 + 4 * current / _attackTime;
        return 2 - 4 * current / _attackTime;
    }

    Vector3 Rotate(float angle, Vector3 original)
    {
        return new Vector3(
            Mathf.Cos(angle) * original.x - Mathf.Sin(angle) * original.y,
            Mathf.Sin(angle) * original.x + Mathf.Cos(angle) * original.y,
            0
        );
    }

    void UpdateOrderInLayer()
    {
        float currentAngle = _frequency * _currentTime + _offset;
        int currentVal = (int) Mathf.Floor(Math.Abs(currentAngle) / Mathf.PI) % 2;
        GetComponent<SpriteRenderer>().sortingOrder = currentVal * 2 + 1;
    }

    void SetUpOrbState()
    {
        float currentAttackTime = 0f;
        _orbState.SetNode(
            OrbState.Orbiting,
            () => { },
            () =>
            {
                _tr.localPosition = GetOrbitingPosition();
                UpdateOrderInLayer();
            },
            _ => attack,
            _ => OrbState.Attacking);

        _orbState.SetNode(
            OrbState.Attacking,
            () =>
            {
                attack = false;
                currentAttackTime = -_attackTime / 2;
            },
            () =>
            {
                currentAttackTime += Time.deltaTime;
                _tr.localPosition = GetMixedPosition(currentAttackTime);
                float scale = 1f + (_sizeScale - 1f) * GetAttackProportion(currentAttackTime);
                _tr.localScale = new Vector3(scale, scale, 1);
            },
            () => _tr.localScale = new Vector3(1, 1, 1),
            _ => currentAttackTime >= _attackTime / 2,
            _ => OrbState.Orbiting);
    }
    
    private void OnTriggerEnter2D(Collider2D col)
    {
        if (col.gameObject.GetComponent<DestroyableCharacter>() && !col.gameObject.CompareTag(_ownerTag))
        {
            col.gameObject.GetComponent<DestroyableCharacter>().Hurt(_damage);
            Vector2 push =  col.gameObject.GetComponent<Transform>().position - _parent.GetComponent<Transform>().position;
            col.gameObject.GetComponent<ShipController>().InelasticCollision(0.5f, _constantPush * push.normalized);
        }
    }
}
