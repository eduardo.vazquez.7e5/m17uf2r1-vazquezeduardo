using System.Collections;
using System.Collections.Generic;
using Scriptable_Objects.DataHolders;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Serialization;
using UnityEngine.UI;

public enum GameResult
{
    Victory,
    Defeat
}
public class ResultPanelScript : MonoBehaviour
{
    [SerializeField] private GameObject title;
    [SerializeField] private GameObject playerName;
    [SerializeField] private GameObject points;
    [SerializeField] private GameObject avatar;
    [SerializeField] private GameResult gameResult;
    
    void Start()
    {
        var playerData = Resources.Load<PlayerData>("ScriptableObjectsInstances/PlayerData");
        gameResult = playerData.gameResult;
        playerName.GetComponent<Text>().text = playerData.playerName;
        points.GetComponent<Text>().text = playerData.points + "pts";
        switch (gameResult)
        {
            case GameResult.Victory:
                title.GetComponent<Text>().text = "Victory";
                avatar.GetComponent<Image>().sprite = playerData.avatar[3];
                break;
            case GameResult.Defeat:
                title.GetComponent<Text>().text = "Defeat";
                avatar.GetComponent<Image>().sprite = playerData.avatar[4];
                break;
        }

        UpdatePersistentData();
        UpdateUIResults();
    }

    public void GoBackToMenu()
    {
        SceneManager.LoadScene(0);
    }

    private void UpdatePersistentData()
    {
        var playerData = Resources.Load<PlayerData>("ScriptableObjectsInstances/PlayerData");
        var persistentData = Resources.Load<PersistentDataSO>("ScriptableObjectsInstances/PersistentData");

        persistentData.Names.Add(playerData.playerName);
        persistentData.Scores.Add(playerData.points);
        bool isBigger = true;
        for (int i = persistentData.Scores.Count - 1; isBigger && i >= 1; i--)
        {
            isBigger = persistentData.Scores[i] > persistentData.Scores[i - 1];
            if (isBigger)
            {
                int score = persistentData.Scores[i];
                string name = persistentData.Names[i];
                persistentData.Scores[i] = persistentData.Scores[i - 1];
                persistentData.Names[i] = persistentData.Names[i - 1];
                persistentData.Scores[i - 1] = score;
                persistentData.Names[i - 1] = name;
            }
        }

        if (persistentData.Scores.Count > 8)
        {
            List<int> newScores = new List<int>();
            List<string> newNames = new List<string>();
            for (int i = 0; i < 8; i++)
            {
                newScores.Add(persistentData.Scores[i]);
                newNames.Add(persistentData.Names[i]);
            }
            persistentData.Scores = newScores;
            persistentData.Names = newNames;
        }
        
        persistentData.SaveSerializedData();
    }
    
    private void UpdateUIResults()
    {
        var persistentData = Resources.Load<PersistentDataSO>("ScriptableObjectsInstances/PersistentData");
        for(int i =0; i<persistentData.Scores.Count; i++)
        {
            var name = GameObject.Find("Name" + i);
            var score = GameObject.Find("Points" + i);
            name.GetComponent<Text>().text = persistentData.Names[i];
            score.GetComponent<Text>().text = persistentData.Scores[i].ToString();
        }
    }
}
