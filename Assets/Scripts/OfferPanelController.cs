using System.Collections.Generic;
using Managers;
using Scriptable_Objects.Market;
using UnityEngine;
using UnityEngine.UI;

public class OfferPanelController : MonoBehaviour
{
    [SerializeField] private GameObject offerFoto;
    [SerializeField] private GameObject textLeft;
    [SerializeField] private GameObject textRight;
    [SerializeField] private List<GameObject> optionDisplayers;

    private List<Option> _options;
    
    public void LoadOffer(Offer offer, bool hasTranslator)
    {
        _options = offer.options;
        offerFoto.GetComponent<Image>().sprite = offer.photo;
        if (offer.isTextLeft)
        {
            textRight.GetComponent<Text>().text = "";
            if(!hasTranslator || offer.englishText == "")
                textLeft.GetComponent<Text>().text = offer.defaultText
                    .Replace("{FIRST}", offer.firstPlaceholder)
                    .Replace("{SECOND}", offer.secondPlaceholder);
            else
                textLeft.GetComponent<Text>().text = offer.englishText
                    .Replace("{FIRST}", offer.firstPlaceholder)
                    .Replace("{SECOND}", offer.secondPlaceholder);
            textLeft.GetComponent<Text>().color = offer.textColor;
        }
        else
        {
            textLeft.GetComponent<Text>().text = "";
            if(!hasTranslator || offer.englishText == "")
                textRight.GetComponent<Text>().text = offer.defaultText
                    .Replace("{FIRST}", offer.firstPlaceholder)
                    .Replace("{SECOND}", offer.secondPlaceholder);
            else
                textRight.GetComponent<Text>().text = offer.englishText
                    .Replace("{FIRST}", offer.firstPlaceholder)
                    .Replace("{SECOND}", offer.secondPlaceholder);
            textRight.GetComponent<Text>().color = offer.textColor;
        }

        for(int i =0; i<optionDisplayers.Count; i++)
        {
            if(!hasTranslator || offer.options[i].englishText == "")
                optionDisplayers[i].GetComponent<Text>().text = offer.options[i].defaultText;
            else
                optionDisplayers[i].GetComponent<Text>().text = offer.options[i].englishText;
        }
    }

    void OnClickOption(int i)
    {
        bool overrideBehaviour = false;
        if (_options != null)
            _options[i].OnClickDo(out overrideBehaviour);
        if (!overrideBehaviour)
        {
            UIManager.Instance.shopPanel.GetComponent<MarketController>().UpdateOffers();
            UIManager.Instance.shopPanel.GetComponent<MarketController>().UpdateAvailability();
            UIManager.Instance.ToggleOffer();
            UIManager.Instance.ToggleShop();
        }
    }

    public void FirstOnClickOption() { OnClickOption(0); }
    public void SecondOnClickOption() { OnClickOption(1); }
    public void ThirdOnClickOption() { OnClickOption(2); }
}
