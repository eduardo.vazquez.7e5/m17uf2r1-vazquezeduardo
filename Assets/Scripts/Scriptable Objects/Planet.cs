using UnityEditor.Animations;
using UnityEngine;

namespace Scriptable_Objects
{
    [CreateAssetMenu(fileName= "New Planet", menuName = "Planet")]
    public class Planet : ScriptableObject
    {
        public float gravity;
        public float power;
        public AnimatorController animator;
        public float size;
        public float luminosity;
        public Color lightColor;
        public int lightSortingLayer = -1;
        public float lightScale = 10f;
        public float minDistance = 0.001f;
    }
}
