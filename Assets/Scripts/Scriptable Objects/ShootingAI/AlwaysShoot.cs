using Managers;
using UnityEngine;

namespace Scriptable_Objects.ShootingAI
{
    [CreateAssetMenu(fileName = "AlwaysShoot", menuName = "ShootingAI/AlwaysShoot")]
    public class AlwaysShoot : ShootingAI
    {
        public override bool Calculate(GameObject go)
        {
            go.GetComponent<ShootingController>().shootingDirection = go.GetComponent<ShipController>().orientation;
            return true;
        }
    }
}