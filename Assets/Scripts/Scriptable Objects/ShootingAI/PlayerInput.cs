using Managers;
using Scriptable_Objects.Weapons;
using UnityEngine;
using UnityEngine.InputSystem;

namespace Scriptable_Objects.ShootingAI
{
    [CreateAssetMenu(fileName = "PlayerInput", menuName= "ShootingAI/PlayerInput")]
    public class PlayerInput : ShootingAI
    {
        private bool _fire = false;
        private bool _autofire = false;
        public override bool Calculate(GameObject go)
        {
            if (!GameManager.Instance.IsGameActive) return false;
            
            go.GetComponent<ShootingController>().shootingDirection = go.GetComponent<ShipController>().orientation;
            Weapon weapon = go.GetComponent<ShootingController>().weapon;
            if (weapon == null) return false;
            //if (weapon.autofire) return InputManager.Instance.AutoFire;
            //return InputManager.Instance.Fire;
            switch (weapon.autofire)
            {
                case true:
                    return _autofire;
                case false:
                    if (_fire)
                    {
                        _fire = false;
                        return true;
                    }
                    return false;
            }
        }

        public void Fire(InputAction.CallbackContext context)
        {
            if(context.performed)
                _fire = true;
        }

        public void AutoFire(InputAction.CallbackContext context)
        {
            if(context.performed)
                _autofire = true;
            if (context.canceled)
                _autofire = false;
        }
    }
}
