using UnityEngine;

namespace Scriptable_Objects.ShootingAI
{
    public abstract class ShootingAI : ScriptableObject
    {
        public abstract bool Calculate(GameObject go);
    }
}