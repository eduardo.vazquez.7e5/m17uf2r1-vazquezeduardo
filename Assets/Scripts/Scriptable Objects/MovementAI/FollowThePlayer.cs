using Scriptable_Objects.DataHolders;
using UnityEngine;
using ListOfElements = Managers.ListOfElements;

namespace Scriptable_Objects.MovementAI
{
    [CreateAssetMenu(fileName = "FollowThePlayer", menuName = "MovementAI/FollowThePlayer")]
    public class FollowThePlayer : MovementAI
    {
        private Vector3 _playerPosition = new Vector3(0, 0, 0);
        
        public override bool Calculate(GameObject go)
        {
            if (go.GetComponent<ShipController>() == null) return false;
            ShipController behaviour = go.GetComponent<ShipController>();

            ListOfElements list = ListOfElements.Instance;
            if (list.player != null)
                _playerPosition = list.player.GetComponent<Transform>().position;
                
            Vector3 dist = _playerPosition - go.GetComponent<Transform>().position;
            Vector2 orientation = new Vector2(dist.x, dist.y);
            
            behaviour.orientation = orientation.normalized;
            behaviour.accelerate = orientation.magnitude > behaviour.distanceFromPlayer;
            behaviour.decelerate = orientation.magnitude < behaviour.distanceFromPlayer;

            return true;
        }
    }
}