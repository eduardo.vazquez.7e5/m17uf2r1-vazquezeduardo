using Classes;
using JetBrains.Annotations;
using UnityEngine;

namespace Scriptable_Objects.MovementAI
{
    [CreateAssetMenu(fileName = "PreventCrashingIntoAPlanet", menuName = "MovementAI/PreventCrashingIntoAPlanet")]
    public class PreventCrashingIntoAPlanet : MovementAI
    {
        public override bool Calculate(GameObject go)
        {
            if (go.GetComponent<ShipController>() == null) return false;
            ShipController behaviour = go.GetComponent<ShipController>();
            
            var planet = IsGoingToCrashIntoPlanet(go, behaviour);
            if (planet != null)
            {
                var dist = planet.GetComponent<Transform>().position - go.GetComponent<Transform>().position;
                behaviour.orientation = -new Vector2(dist.x, dist.y).normalized;
                behaviour.accelerate = true;
                return true;
            }
            return false;
        }
        
        [CanBeNull]
        private GameObject IsGoingToCrashIntoPlanet(GameObject go, ShipController behaviour)
        {
            Vector2 velocity = go.GetComponent<Rigidbody2D>().velocity;
            RaycastHit2D[] hits = Physics2D.RaycastAll(go.GetComponent<Transform>().position, velocity.normalized, velocity.magnitude * behaviour.timeToLookInFront);
            foreach (var obj in hits)
                if (obj.collider.gameObject.CompareTag("Ground") || obj.collider.gameObject.CompareTag("Asteroid"))
                    return obj.collider.gameObject;
            return null;
        }

    }
}