using Characters;
using Classes;
using JetBrains.Annotations;
using Scriptable_Objects.DataHolders;
using UnityEngine;
using ListOfElements = Managers.ListOfElements;

namespace Scriptable_Objects.MovementAI
{
    [CreateAssetMenu(fileName = "MissileAI", menuName = "MovementAI/MissileAI")]
    public class MissileAI : MovementAI
    {
        private Vector3 _enemyPosition = new Vector3(0, 0, 0);
        
        public override bool Calculate(GameObject go)
        {
            if (go.GetComponent<MissileScript>() == null) return false;
            MissileScript behaviour = go.GetComponent<MissileScript>();
            if (behaviour.rangedWeapon == null || behaviour.ownerTag == "") return false;
            if (behaviour.objective != null && behaviour.objective.CompareTag(go.tag)) behaviour.objective = null;
            
            if (behaviour.objective == null)
                behaviour.objective = FindObjective(go, behaviour);

            if (behaviour.objective == null) return false;
                
            Vector2 enemySpeed = new Vector2(0, 0);
            if (behaviour.objective != null)
            {
                if (!behaviour.objective.GetComponent<Rigidbody2D>())
                    return false;
                
                _enemyPosition = behaviour.objective.GetComponent<Transform>().position;
                enemySpeed = behaviour.objective.GetComponent<Rigidbody2D>().velocity;
            }
            
            float aispeed = go.GetComponent<Rigidbody2D>().velocity.magnitude;
            Vector3 dist = _enemyPosition - go.GetComponent<Transform>().position;
            float time = new Vector2(dist.x, dist.y).magnitude / aispeed;
            Vector2 displacement = enemySpeed * time;
            Vector3 newDist = _enemyPosition + new Vector3(displacement.x,displacement.y,0) - go.GetComponent<Transform>().position;
            Vector2 orientation = new Vector2(newDist.x, newDist.y);
            
            behaviour.orientation = orientation.normalized;
            behaviour.accelerate = orientation.magnitude > behaviour.distanceFromPlayer;
            behaviour.decelerate = orientation.magnitude < behaviour.distanceFromPlayer;

            return true;
        }

        [CanBeNull]
        GameObject FindObjective(GameObject go, MissileScript behaviour)
        {
            var list = ListOfElements.Instance;
            GameObject objective = null;
            float dist = -1f;
            foreach (var ship in list.listOfShips)
            {
                if (ship != null && !ship.CompareTag(behaviour.ownerTag) && !ship.CompareTag("Asteroid"))
                {
                    Vector3 dif = ship.GetComponent<Transform>().position - go.GetComponent<Transform>().position;
                    if (dist < 0 || new Vector2(dif.x, dif.y).magnitude < dist)
                    {
                        dist = new Vector2(dif.x, dif.y).magnitude;
                        objective = ship;
                    }
                }
            }
            return objective;
        }
    }
}