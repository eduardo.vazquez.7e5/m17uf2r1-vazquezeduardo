using UnityEngine;

namespace Scriptable_Objects.MovementAI
{
    public abstract class MovementAI : ScriptableObject
    {
        public abstract bool Calculate(GameObject go);
    }
}