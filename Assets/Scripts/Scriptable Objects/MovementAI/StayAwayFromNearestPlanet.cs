using Characters;
using Classes;
using Scriptable_Objects.DataHolders;
using UnityEngine;
using ListOfElements = Managers.ListOfElements;

namespace Scriptable_Objects.MovementAI
{
    [CreateAssetMenu(fileName = "StayAwayFromNearestPlanet", menuName = "MovementAI/StayAwayFromNearestPlanet")]
    public class StayAwayFromNearestPlanet : MovementAI
    {
        public override bool Calculate(GameObject go)
        {
            if (go.GetComponent<ShipController>() == null) return false;
            ShipController behaviour = go.GetComponent<ShipController>();

            var list = ListOfElements.Instance;
            float minDist = behaviour.distanceFromPlanet + 1;
            for(int i =0; i<list.listOfPlanets.Count; i++)
            {
                var planet = list.listOfPlanets[i];
                if (planet == null)
                    list.listOfPlanets.RemoveAt(i--);
                else
                {
                    Vector3 dir = planet.GetComponent<Transform>().position - go.GetComponent<Transform>().position;
                    Vector2 moonDist =  new Vector2(dir.x, dir.y);
                    float distFromSurface = moonDist.magnitude - planet.GetComponent<PlanetController>().size;
                    if (distFromSurface < behaviour.distanceFromPlanet && distFromSurface < minDist)
                    {
                        minDist = distFromSurface;
                        behaviour.orientation = -moonDist.normalized;
                        behaviour.accelerate = true;
                    }
                }
            }
            for(int i = 0; i<list.listOfMeteorites.Count; i++)
            {
                var meteo = list.listOfMeteorites[i];
                if (meteo == null)
                    list.listOfMeteorites.RemoveAt(i--);
                else
                {
                    Vector3 dir = meteo.GetComponent<Transform>().position - go.GetComponent<Transform>().position;
                    Vector2 moonDist =  new Vector2(dir.x, dir.y);
                    float distFromSurface = moonDist.magnitude - meteo.GetComponent<Transform>().localScale.x;
                    if (distFromSurface < behaviour.distanceFromPlanet && distFromSurface < minDist)
                    {
                        minDist = distFromSurface;
                        behaviour.orientation = -moonDist.normalized;
                        behaviour.accelerate = true;
                    }
                }
            }
            return minDist < behaviour.distanceFromPlanet;
        }
    }
}
