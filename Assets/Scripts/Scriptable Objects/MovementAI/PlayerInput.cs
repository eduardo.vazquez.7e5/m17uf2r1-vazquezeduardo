using Characters;
using Classes;
using Managers;
using Scriptable_Objects.DataHolders;
using UnityEngine;
using UnityEngine.InputSystem;
using ListOfElements = Managers.ListOfElements;

namespace Scriptable_Objects.MovementAI
{
    [CreateAssetMenu(fileName = "PlayerInput", menuName = "MovementAI/PlayerInput")]
    public class PlayerInput : MovementAI
    {
        private Vector2 _playerOrientation = new Vector2(1, 0);
        private bool _accelerate = false;
        private bool _decelerate = false;
        public override bool Calculate(GameObject go)
        {
            if (go.GetComponent<ShipController>() == null) return false;
            ShipController behaviour = go.GetComponent<ShipController>();
            
           // if(GameManager.Instance.IsGameActive)
           //     MouseOrientationToWorldOrientation();
            
            behaviour.orientation = _playerOrientation;
            behaviour.accelerate = _accelerate && GameManager.Instance.IsGameActive;
            behaviour.decelerate = _decelerate && GameManager.Instance.IsGameActive;
            
            return true;
        }

        public void Accelerate(InputAction.CallbackContext context)
        {
            if (context.performed)
            {
                _accelerate = true;
                _decelerate = false;
            }
            if (context.canceled)
                _accelerate = false;
        }
        
        public void Decelerate(InputAction.CallbackContext context)
        {
            if (context.performed)
            {
                _decelerate = true;
                _accelerate = false;
            }
            if (context.canceled)
                _decelerate = false;
        }
        
        public void MouseOrientationToWorldOrientation(InputAction.CallbackContext context)
        {
            var list = ListOfElements.Instance;
            if (list.player != null && context.performed && GameManager.Instance.IsGameActive)
            {
                Vector2 mouse = context.ReadValue<Vector2>();
                Vector2 playerPos = Vector2.zero;
                playerPos = Camera.main.WorldToScreenPoint(list.player.GetComponent<Transform>().position);
                mouse = new Vector2(mouse.x-playerPos.x, mouse.y-playerPos.y);
                if (mouse != Vector2.zero)
                    _playerOrientation = mouse.normalized;
            }
        }
        
        public void Aim(InputAction.CallbackContext context)
        {
            if (context.performed && context.ReadValue<Vector2>().magnitude > 0.4f)
                _playerOrientation = context.ReadValue<Vector2>().normalized;
        }
    }
}