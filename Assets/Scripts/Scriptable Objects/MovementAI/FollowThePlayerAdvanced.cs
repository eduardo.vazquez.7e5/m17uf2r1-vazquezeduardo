using UnityEngine;
using ListOfElements = Managers.ListOfElements;

namespace Scriptable_Objects.MovementAI
{
    [CreateAssetMenu(fileName = "FollowThePlayerAdvanced", menuName = "MovementAI/FollowThePlayerAdvanced")]
    public class FollowThePlayerAdvanced : MovementAI
    {
        private Vector3 _playerPosition = new Vector3(0, 0, 0);
        
        public override bool Calculate(GameObject go)
        {
            if (go.GetComponent<ShipController>() == null) return false;
            ShipController behaviour = go.GetComponent<ShipController>();

            Vector2 playerSpeed = new Vector2(0, 0);
            ListOfElements list = ListOfElements.Instance;
            if (list.player != null)
            {
                if (!list.player.GetComponent<Rigidbody2D>())
                    return CreateInstance<FollowThePlayer>().Calculate(go);
                
                GameObject player = list.player;
                _playerPosition = player.GetComponent<Transform>().position;
                playerSpeed = player.GetComponent<Rigidbody2D>().velocity;
            }
            
            float aispeed = go.GetComponent<Rigidbody2D>().velocity.magnitude;
            if (go.GetComponent<ShootingController>())
                aispeed = go.GetComponent<ShootingController>().weapon.bulletSpeed;
            Vector3 dist = _playerPosition - go.GetComponent<Transform>().position;
            float time = new Vector2(dist.x, dist.y).magnitude / aispeed;
            Vector2 displacement = playerSpeed * time;
            Vector3 newDist = _playerPosition + new Vector3(displacement.x,displacement.y,0) - go.GetComponent<Transform>().position;
            Vector2 orientation = new Vector2(newDist.x, newDist.y);
            
            behaviour.orientation = orientation.normalized;
            behaviour.accelerate = orientation.magnitude > behaviour.distanceFromPlayer;
            behaviour.decelerate = orientation.magnitude < behaviour.distanceFromPlayer;

            return true;
        }
    }
}