using Characters;
using Managers;
using Unity.Mathematics;
using UnityEngine;
using ListOfElements = Managers.ListOfElements;
using Vector2 = UnityEngine.Vector2;
using Vector3 = UnityEngine.Vector3;
using Random = UnityEngine.Random;

namespace Scriptable_Objects.SceneData
{
    [CreateAssetMenu(fileName = "New SpawnTwoPlanets", menuName = "SceneData/TimedScene/SpawnTwoPlanets")]
    public class SpawnTwoPlanets : TimedSceneData
    {
        public GameObject planetDisplayer;
        public Planet firstPlanetData;
        public Vector2 firstPlanetPosition;
        public Planet secondPlanetData;
        public Vector2 secondPlanetPosition;
        public Vector3 gameCenter;
        public override void Start()
        {
            gameCenter = new Vector3(0, 0, 0);
            base.Start();
            var list = ListOfElements.Instance;
            Vector2 speed = list.player!.GetComponent<Rigidbody2D>().velocity;
            if (speed == Vector2.zero)
                speed = Vector2.right;
            speed = speed.normalized;
            float orientation = Random.Range(0.0f, 360.0f);
            gameCenter = list.player.GetComponent<Transform>().position + 12 * new Vector3(-speed.y, speed.x, 0);
            GameManager.Instance.GetComponent<Transform>().position = gameCenter;
            GameObject.Find("Background").GetComponent<BackgroundScript>().UpdateBackgroundPosition();
            var first = Instantiate(planetDisplayer, gameCenter + (Vector3) Rotate(firstPlanetPosition, orientation), quaternion.identity);
            first.GetComponent<PlanetController>().LoadData(firstPlanetData);
            var second = Instantiate(planetDisplayer, gameCenter + (Vector3) Rotate(secondPlanetPosition, orientation), quaternion.identity);
            second.GetComponent<PlanetController>().LoadData(secondPlanetData);
        }

        private Vector2 Rotate(Vector2 v, float degrees)
        {
            float radians = degrees * Mathf.Deg2Rad;
            float sin = Mathf.Sin(radians);
            float cos = Mathf.Cos(radians);

            float tx = v.x;
            float ty = v.y;

            return new Vector2(cos * tx - sin * ty, sin * tx + cos * ty);
        }
    }
}