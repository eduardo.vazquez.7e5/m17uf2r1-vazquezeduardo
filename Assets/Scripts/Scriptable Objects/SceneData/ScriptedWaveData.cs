using System.Collections.Generic;
using Scriptable_Objects.ShipData;
using UnityEngine;

namespace Scriptable_Objects.SceneData
{
    [CreateAssetMenu(fileName = "New ScriptedWaveData", menuName = "SceneData/WaveData/ScriptedWaveData")]
    public class ScriptedWaveData : WaveData
    {
        public List<int> listOfPrios;
    }
}