using System.Collections.Generic;
using UnityEngine;

namespace Scriptable_Objects.SceneData
{
    [CreateAssetMenu(fileName = "New OrderedScenes", menuName = "SceneData/OrderedScenes")]
    public class OrderedScenesCollectorScriptableObject : ScriptableObject
    {
        public GameObject tutorialController;
        public GameObject timedController;
        public GameObject scriptedWaveController;
        public GameObject randomWaveController;
        public List<SceneData> listOfScenes;
    }
}