using Characters;
using Managers;
using Unity.Mathematics;
using UnityEngine;
using ListOfElements = Managers.ListOfElements;
using Vector2 = UnityEngine.Vector2;
using Vector3 = UnityEngine.Vector3;
using Random = UnityEngine.Random;
using System.Collections.Generic;

namespace Scriptable_Objects.SceneData
{
    [CreateAssetMenu(fileName = "New CreateRandomSystem", menuName = "SceneData/TimedScene/CreateRandomSystem")]
    public class CreateRandomSystem : TimedSceneData
    {
        public GameObject planetDisplayer;
        public List<Planet> availablePlanets;
        public Planet miniblackhole;
        public int numberOfMiniBlackHoles;
        public float distanceFromPlayer;
        public override void Start()
        {
            var gameCenter = new Vector3(0, 0, 0);
            base.Start();
            var list = ListOfElements.Instance;
            Vector2 speed = list.player!.GetComponent<Rigidbody2D>().velocity;
            if (speed == Vector2.zero)
                speed = Vector2.right;
            speed = speed.normalized;
            gameCenter = list.player.GetComponent<Transform>().position + distanceFromPlayer * new Vector3(-speed.y, speed.x, 0);
            GameManager.Instance.GetComponent<Transform>().position = gameCenter;
            GameObject.Find("Background").GetComponent<BackgroundScript>().UpdateBackgroundPosition();
            var first = Instantiate(planetDisplayer, gameCenter, quaternion.identity);
            int rand = Random.Range(0, availablePlanets.Count);
            first.GetComponent<PlanetController>().LoadData(availablePlanets[rand]);
            for(int i =0; i<numberOfMiniBlackHoles; i++)
            {
                Vector2 position;
                do
                {
                    position = Random.insideUnitCircle * (distanceFromPlayer - 1);
                } while (position.magnitude < 1.5 * availablePlanets[rand].size);
                var clone = Instantiate(planetDisplayer, gameCenter + (Vector3) position, Quaternion.Euler(0, 0, Random.Range(0, 360)));
                clone.GetComponent<PlanetController>().LoadData(miniblackhole);
            }
        }
    }
}