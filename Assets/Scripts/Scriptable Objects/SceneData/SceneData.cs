using Managers;
using UnityEngine;

public enum SceneType
{
    Tutorial,
    TimedScene,
    ScriptedWave,
    RandomWave
}

namespace Scriptable_Objects.SceneData
{
    public abstract class SceneData : ScriptableObject
    {
        public string title;
        public string[] hints;
        
        private float _currentTimeBetweenHints;
        private float _timeBetweenHints = 5f;
        private int _currentHint;

        public virtual void Start()
        {
            if(title != "")
                UIManager.Instance.PublicMessage(title);
            _currentTimeBetweenHints = 0f;
            _currentHint = 0;
        }

        public virtual void Update()
        {
            if (hints.Length != 0)
            {
                _currentTimeBetweenHints += Time.deltaTime;
                if (_currentTimeBetweenHints >= _timeBetweenHints)
                {
                    _currentTimeBetweenHints -= _timeBetweenHints;
                    _timeBetweenHints += 1f;

                    UIManager.Instance.PublicMessage(hints[_currentHint]);
                    _currentHint = ++_currentHint % hints.Length;
                }
            }
        }
        
        public virtual void End() { }
    }
}
