using UnityEngine;
using ListOfElements = Managers.ListOfElements;

namespace Scriptable_Objects.SceneData
{
    [CreateAssetMenu(fileName = "New ComeOnExploreABit", menuName = "SceneData/TutorialData/ComeOnExploreABit")]
    public class ComeOnExploreABit : TutorialData
    {
        public float distanceToExplore;
        private Vector2 _startingPosition;

        public override void Start()
        {
            var player = ListOfElements.Instance.player;
            if (player != null)
                _startingPosition = (Vector2) player.GetComponent<Transform>().position;
            base.Start();
        }

        public override bool ExpectedPlayerInput()
        {
            var player = ListOfElements.Instance.player;
            if (player == null)
                return true;
            return ((Vector2) player.GetComponent<Transform>().position - _startingPosition).magnitude >
                   distanceToExplore;
        }
    }
}