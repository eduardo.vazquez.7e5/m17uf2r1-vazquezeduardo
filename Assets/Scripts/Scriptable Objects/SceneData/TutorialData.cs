using UnityEngine;

namespace Scriptable_Objects.SceneData
{
    public abstract class TutorialData : SceneData
    {
        public abstract bool ExpectedPlayerInput();
    }
}