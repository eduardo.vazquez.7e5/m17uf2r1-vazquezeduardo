using System.Linq;
using Characters;
using Managers;
using UnityEngine;

namespace Scriptable_Objects.SceneData
{
    [CreateAssetMenu(fileName = "New DeSpawnPlanets", menuName = "SceneData/TimedScene/DeSpawnPlanets")]
    public class DeSpawnPlanets : TimedSceneData
    {
        public override void Start()
        {
            ListOfElements list = ListOfElements.Instance;
            while(list.listOfPlanets.Any())
                list.listOfPlanets[0].GetComponent<PlanetController>().Destroy();
        }
    }
}