using Characters;
using Scriptable_Objects.DataHolders;
using UnityEngine;
using ListOfElements = Managers.ListOfElements;

namespace Scriptable_Objects.SceneData
{
    [CreateAssetMenu(fileName = "New LookingAroundTutorial", menuName = "SceneData/TutorialData/LookingAroundTutorial")]
    public class LookingAroundTutorial : TutorialData
    {
        private Vector2 _initialOrientation;
        public override void Start()
        {
            base.Start();
            
            var list = ListOfElements.Instance;
            if (list.player != null)
                _initialOrientation = list.player.GetComponent<Player>().orientation;
            else
                _initialOrientation = new Vector2(1, 0);
        }

        public override bool ExpectedPlayerInput()
        {
            var list = ListOfElements.Instance;
            if (list.player == null) return false;
            return Vector2.Dot(list.player.GetComponent<Player>().orientation, _initialOrientation) < - 0.5;
        }
    }
}