using Managers;
using Unity.Mathematics;
using UnityEngine;
using Random = UnityEngine.Random;
using Vector3 = UnityEngine.Vector3;

namespace Scriptable_Objects.SceneData
{
    [CreateAssetMenu(fileName = "New SpawnTwoBlackHoles", menuName = "SceneData/TimedScene/SpawnTwoBlackHoles")]
    public class SpawnTwoBlackHoles : TimedSceneData
    {
        public GameObject blackholeDisplayer;
        public float radius;
        public override void Start()
        {
            base.Start();
            float angle = Random.Range(0, 2 * Mathf.PI);
            Vector3 pos = GameManager.Instance.GetComponent<Transform>().position + radius * new Vector3(Mathf.Cos(angle), Mathf.Sin(angle), 0);
            Instantiate(blackholeDisplayer, pos, quaternion.identity);
            angle = Random.Range(0, 2 * Mathf.PI);
            pos = GameManager.Instance.GetComponent<Transform>().position + radius * new Vector3(Mathf.Cos(angle), Mathf.Sin(angle), 0);
            Instantiate(blackholeDisplayer, pos, quaternion.identity);
        }
    }
}