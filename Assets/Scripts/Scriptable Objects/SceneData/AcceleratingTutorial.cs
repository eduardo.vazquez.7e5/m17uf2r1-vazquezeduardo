using Characters;
using Managers;
using Scriptable_Objects.DataHolders;
using UnityEngine;
using ListOfElements = Managers.ListOfElements;

namespace Scriptable_Objects.SceneData
{
    [CreateAssetMenu(fileName = "New AcceleratingTutorial", menuName = "SceneData/TutorialData/AcceleratingTutorial")]
    public class AcceleratingTutorial : TutorialData
    {
        private bool _hasAccelerated;
        private bool _hasDecelerated;

        public override void Start()
        {
            _hasAccelerated = false;
            _hasDecelerated = false;
            base.Start();
        }

        public override void Update()
        {
            base.Update();
            var list = ListOfElements.Instance;
            if (list.player != null)
            {
                if(!_hasAccelerated)
                    _hasAccelerated = list.player.GetComponent<Player>().accelerate;
                if(!_hasDecelerated)
                    _hasDecelerated = list.player.GetComponent<Player>().decelerate && ! list.player.GetComponent<Player>().accelerate;
            }
        }

        public override bool ExpectedPlayerInput()
        {
            return _hasAccelerated && _hasDecelerated;
        }
    }
}