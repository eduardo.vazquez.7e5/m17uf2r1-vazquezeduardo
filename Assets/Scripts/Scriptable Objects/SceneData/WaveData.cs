using System.Collections.Generic;
using Scriptable_Objects.ShipData;
using UnityEngine;

namespace Scriptable_Objects.SceneData
{
    public class WaveData : SceneData
    {
        public float minRadius;
        public float maxRadius;
        public int maxValue;

        public List<ShipData.ShipData> listOfEnemies;
        public List<GameObject> listOfPrefabs;
        public List<uint> listOfQuantities;
        public List<uint> listOfValues;
        public List<GameObject> listOfMeteorites;
        public List<MeteoriteData> listOfMeteoriteData;
        public float timeBetweenMeteorites;
    }
}