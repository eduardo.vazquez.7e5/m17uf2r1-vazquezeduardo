using System.Collections.Generic;
using Scriptable_Objects.ShipData;
using UnityEngine;

namespace Scriptable_Objects.SceneData
{
    [CreateAssetMenu(fileName = "New RandomWaveData", menuName = "SceneData/WaveData/RandomWaveData")]
    public class RandomWaveData : WaveData
    {
        public int totalValue;
    }
}