using System.Collections.Generic;
using System.Linq;
using Characters;
using Scriptable_Objects.ShipData;
using UnityEngine;
using ListOfElements = Managers.ListOfElements;

namespace Scriptable_Objects.SceneData
{
    [CreateAssetMenu(fileName = "New DangerousExploration", menuName = "SceneData/TutorialData/DangerousExploration")]
    public class DangerousExploration : TutorialData
    {
        public float distanceToExplore;
        public float timeBetweenAsteroids;
        public float timeBetweenBlackHoles;
        public List<GameObject> meteoriteDisplayer;
        public List<MeteoriteData> meteoriteData;
        public GameObject planetDisplayer;
        public Planet miniBlackholeData;
        
        private Vector2 _startingPosition;
        private List<GameObject> _listOfSpawned;
        private float _currentTimeBetweenAsteroids;
        private float _currentTimeBetweenBlackHoles;
        public override void Start()
        {
            _listOfSpawned = new List<GameObject>();
            var player = ListOfElements.Instance.player;
            if (player != null)
                _startingPosition = (Vector2) player.GetComponent<Transform>().position;
            base.Start();
        }

        public override void Update()
        {
            base.Update();
            _currentTimeBetweenAsteroids += Time.deltaTime;
            if (_currentTimeBetweenAsteroids >= timeBetweenAsteroids)
            {
                _currentTimeBetweenAsteroids -= timeBetweenAsteroids;
                SpawnAsteroid();
            }

            _currentTimeBetweenBlackHoles += Time.deltaTime;
            if (_currentTimeBetweenBlackHoles >= timeBetweenBlackHoles)
            {
                _currentTimeBetweenBlackHoles -= timeBetweenBlackHoles;
                SpawnBlackHole();
            }
        }

        void SpawnAsteroid()
        {
            int randAsteroid = Random.Range(0, meteoriteData.Count);
            var position = SelectRandomPosition();
            var clone = Instantiate(meteoriteDisplayer[randAsteroid], position, Quaternion.Euler(0, 0, Random.Range(0, 360)));
            clone.GetComponent<Meteorite>().LoadData(meteoriteData[randAsteroid]);
            _listOfSpawned.Add(clone);
        }

        void SpawnBlackHole()
        {
            var position = SelectRandomPosition();
            var clone = Instantiate(planetDisplayer, position, Quaternion.Euler(0, 0, Random.Range(0, 360)));
            clone.GetComponent<PlanetController>().LoadData(miniBlackholeData);
            _listOfSpawned.Add(clone);
        }

        Vector3 SelectRandomPosition()
        {
            float randDist = Random.Range(1f, 3f);
            var player = ListOfElements.Instance.player;
            if (player == null)
                return Vector3.zero;
            var velocity = (Vector3) player.GetComponent<Rigidbody2D>().velocity;
            var translation = velocity * randDist;
            if (translation == Vector3.zero)
                translation = Random.insideUnitCircle;
            if (translation.magnitude < 5f)
                translation = translation.normalized * 5f;
            var position = player.GetComponent<Transform>().position + translation;
            var orthogonal = new Vector2(velocity.y, -velocity.x).normalized;
            orthogonal *= Random.Range(-3f, 3f);
            position += (Vector3) orthogonal;
            return position;
        }

        public override void End()
        {
            while (_listOfSpawned.Any())
            {
                if (_listOfSpawned[0] != null)
                {
                    if(_listOfSpawned[0].TryGetComponent(out PlanetController pc))
                        pc.Destroy();
                    else
                        Destroy(_listOfSpawned[0]);
                }
                _listOfSpawned.RemoveAt(0);
            }
            base.End();
        }

        public override bool ExpectedPlayerInput()
        {
            var player = ListOfElements.Instance.player;
            if (player == null)
                return true;
            return ((Vector2) player.GetComponent<Transform>().position - _startingPosition).magnitude >
                   distanceToExplore;
        }
    }
}