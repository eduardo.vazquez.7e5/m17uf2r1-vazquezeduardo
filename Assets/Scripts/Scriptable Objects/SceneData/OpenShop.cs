using Managers;
using UnityEngine;

namespace Scriptable_Objects.SceneData
{
    [CreateAssetMenu(fileName = "New OpenShop", menuName = "SceneData/TutorialData/OpenShop")]
    public class OpenShop : TutorialData
    {
        private bool _areWeOpen;
        public override void Start()
        {
            _areWeOpen = true;
            base.Start();
            UIManager.Instance.shopPanel.GetComponent<MarketController>().RandomizeShop();
            UIManager.Instance.TogglePowerUp();
            UIManager.Instance.powerupPanel.GetComponent<PowerUpPanelController>().RandomizePowerUps();
        }

        public override bool ExpectedPlayerInput()
        {
            return !_areWeOpen;
        }

        public void CloseShop()
        {
            _areWeOpen = false;
            UIManager.Instance.ToggleShop();
        }
    }
}