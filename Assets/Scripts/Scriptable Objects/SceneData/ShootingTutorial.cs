using Managers;
using Scriptable_Objects.DataHolders;
using UnityEngine;
using ListOfElements = Managers.ListOfElements;

namespace Scriptable_Objects.SceneData
{
    [CreateAssetMenu(fileName = "New ShootingTutorial", menuName = "SceneData/TutorialData/ShootingTutorial")]
    public class ShootingTutorial : TutorialData
    {
        public override bool ExpectedPlayerInput()
        {
            var listOfElements = ListOfElements.Instance;
            if (listOfElements.player == null) return false;
            if (listOfElements.player.GetComponent<ShootingController>().shoot) return true;
            var obj = listOfElements.player.GetComponent<IMelleeAttacker>().AttackState;
            if (obj != null && obj.GetCurrentState() == AttackState.Cooldown) return true;
            return false;
        }
    }
}