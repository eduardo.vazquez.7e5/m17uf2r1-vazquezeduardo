using System;
using Characters;
using Managers;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.Serialization;
using UnityEngine.UIElements;
using ListOfElements = Managers.ListOfElements;

namespace Scriptable_Objects.SceneData
{
    [CreateAssetMenu(fileName = "New EjectFromSystem", menuName = "SceneData/TimedScene/EjectFromSystem")]
    public class EjectFromSystem : TimedSceneData
    {
        private Vector3 _initialPlayerPos;
        private Vector3 _prevPlayerPos;
        private Vector3 _finalPlayerPos;
        private GameObject _player;
        private float _currentTime;

        private Vector2 _newXdir;
        public float escapeRadius;
        public float escapeAngle;
        private Vector2 _velocity;

        private Vector2 _cubicCoefficient;
        private Vector2 _squaredCoefficient;
        private Vector2 _independentTerm;

        public override void Start()
        {
            var listOfElements = ListOfElements.Instance;
            _player = listOfElements.player;
            if (_player != null)
            {
                _initialPlayerPos = _player.GetComponent<Transform>().position;
                _prevPlayerPos = _initialPlayerPos;
                _currentTime = 0f;
                
                _newXdir = ((Vector2)(_initialPlayerPos - GameManager.Instance.GetComponent<Transform>().position)).normalized;
                _finalPlayerPos = GameManager.Instance.GetComponent<Transform>().position + escapeRadius * Rotate(escapeAngle, _newXdir);
                
                Vector2 distance = _finalPlayerPos - _initialPlayerPos;
                Vector2 finalSpeed = _player.GetComponent<Player>().maxSpeed * distance.normalized;
                _independentTerm = _initialPlayerPos;
                _squaredCoefficient = 3 * distance - finalSpeed * sceneDuration;
                _cubicCoefficient = -2 * distance  + finalSpeed * sceneDuration;
            }
        }

        public override void Update()
        {
            base.Update();
            if (_player != null)
            {
                _currentTime += Time.deltaTime;
                Vector3 newPlayerPos=  GetPosition(_currentTime/ sceneDuration);
                _velocity = GetVelocity(newPlayerPos, _currentTime/ sceneDuration);
                _player.GetComponent<ShipController>().orientation = _velocity;
                _prevPlayerPos = _player.GetComponent<Transform>().position;
                _player.GetComponent<Transform>().position = newPlayerPos;
            }
        }

        public override void End()
        {
            base.End();
            if (_player != null)
            {
                _player.GetComponent<Transform>().position =  GetPosition(1f);
                _player.GetComponent<BoxCollider2D>().enabled = true;
                _player.GetComponent<PlayerInput>().enabled = true;
                _player.GetComponent<Rigidbody2D>().isKinematic = false;
                _player.GetComponent<Rigidbody2D>().velocity = _velocity * _player.GetComponent<Player>().maxSpeed;
                _player.GetComponent<ShipController>().gravityScale = 1;
            }
        }

        // Spline Receives Value from 0 to 1
        private Vector3 GetPosition(float time)
        {
            //return Vector3.Lerp(_initialPlayerPos, _finalPlayerPos, time * time);
            return _cubicCoefficient * Mathf.Pow(time, 3) 
                   + _squaredCoefficient * Mathf.Pow(time, 2) 
                   + _independentTerm;
        }

        private Vector2 GetVelocity(Vector3 newPlayerPos, float time)
        {
            if(time == 0) 
                return Vector2.zero;
            return ((Vector2)(newPlayerPos - _prevPlayerPos)).normalized;

        }
        
        Vector3 Rotate(float angle, Vector3 original)
        {
            angle *= Mathf.Deg2Rad;
            return new Vector3(
                Mathf.Cos(angle) * original.x - Mathf.Sin(angle) * original.y,
                Mathf.Sin(angle) * original.x + Mathf.Cos(angle) * original.y,
                0
            );
        }
    }
}