using Managers;
using Scriptable_Objects.DataHolders;
using Scriptable_Objects.ShipData;
using UnityEditor;
using UnityEngine;

namespace Scriptable_Objects.SceneData
{
    [CreateAssetMenu(fileName = "New SetUpBossData", menuName = "SceneData/TimedScene/SetUpBossData")]
    public class SetUpBossData : TimedSceneData
    {
        public float radius;
        public override void Start()
        {
            base.Start();
            var bossData = Resources.Load<BossData>("ScriptableObjectsInstances/Ships/BossData");
            EditorUtility.SetDirty(bossData);
            bossData.movementRadius = radius;
            var player = ListOfElements.Instance.player;
            if (player != null)
            {
                Vector2 dist = (player.GetComponent<Transform>().position - GameManager.Instance.GetComponent<Transform>().position);
                dist = dist.normalized;
                var angle = Mathf.Atan2(dist.x, dist.y);
                bossData.angleOffset = (angle + 180f) % 360f;
            }
        }
    }
}