using Managers;
using Scriptable_Objects.DataHolders;
using UnityEngine;
using ListOfElements = Managers.ListOfElements;

namespace Scriptable_Objects.SceneData
{
    [CreateAssetMenu(fileName = "New UsingYourSkillTutorial", menuName = "SceneData/TutorialData/UsingYourSkillTutorial")]
    public class UsingYourSkillTutorial : TutorialData
    {
        public override void Start()
        {
            base.Start();
        }

        public override bool ExpectedPlayerInput()
        {
            var listOfElements = ListOfElements.Instance;
            if (listOfElements.player == null) return false;
            return listOfElements.player.GetComponent<ISkillUser>().UseSkill;
        }
    }
}