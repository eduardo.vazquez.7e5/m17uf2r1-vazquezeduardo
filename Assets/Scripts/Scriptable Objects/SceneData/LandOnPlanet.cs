using System;
using Characters;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.Serialization;
using ListOfElements = Managers.ListOfElements;

namespace Scriptable_Objects.SceneData
{
    [CreateAssetMenu(fileName = "New LandOnPlanet", menuName = "SceneData/TimedScene/LandOnPlanet")]
    public class LandOnPlanet : TimedSceneData
    {
        public int planetToLand;
        private Vector3 _initialPlayerPos;
        private Vector3 _prevPlayerPos;
        private Vector3 _finalPlayerPos;
        private GameObject _player;
        private float _currentTime;

        private Vector2 _newXdir;
        private Vector2 _newYdir;
        private float _distance;
        private float _reductionConstant;
        private float _frequencyConstant;
        public float timeToArrive = 0.5f;

        public override void Start()
        {
            var listOfElements = ListOfElements.Instance;
            _player = listOfElements.player;
            if (_player != null)
            {
                _player.GetComponent<BoxCollider2D>().enabled = false;
                _player.GetComponent<PlayerInput>().enabled = false;
                _player.GetComponent<Rigidbody2D>().velocity = Vector2.zero;
                _player.GetComponent<Rigidbody2D>().isKinematic = true;
                _player.GetComponent<ShipController>().gravityScale = 0;
                _player.GetComponent<ShipController>().orientation = Vector2.zero;
                _player.GetComponent<ISkillUser>().SkillState.SetTrigger(SkillState.InUse, SkillState.Cooldown);
                _initialPlayerPos = _player.GetComponent<Transform>().position;
                _prevPlayerPos = _initialPlayerPos;
                _finalPlayerPos = listOfElements.listOfPlanets[planetToLand].GetComponent<Transform>().position;
                _currentTime = 0f;
                
                _newXdir = ((Vector2)(_initialPlayerPos - _finalPlayerPos)).normalized;
                _newYdir = new Vector2(_newXdir.y, - _newXdir.x);
                _distance = ((Vector2)(_initialPlayerPos - _finalPlayerPos)).magnitude;
                _reductionConstant = (float) Math.Pow((listOfElements.listOfPlanets[planetToLand].GetComponent<PlanetController>().size)/_distance, 1/timeToArrive);
                _frequencyConstant = (float) Math.PI /timeToArrive;
            }
        }

        public override void Update()
        {
            base.Update();
            if (_player != null)
            {
                _currentTime += Time.deltaTime;
                Vector3 newPlayerPos=  GetPosition(_currentTime/ sceneDuration);
                _player.GetComponent<ShipController>().orientation = GetVelocity(newPlayerPos, _currentTime/ sceneDuration);
                _prevPlayerPos = _player.GetComponent<Transform>().position;
                _player.GetComponent<Transform>().position = newPlayerPos;
            }
        }

        public override void End()
        {
            base.End();
            if (_player != null)
            {
                _player.GetComponent<Transform>().position =  GetPosition(1f);
            }
        }

        // Spline Receives Value from 0 to 1
        private Vector3 GetPosition(float time)
        {
            //return Vector3.Lerp(_initialPlayerPos, _finalPlayerPos, time);
            return _finalPlayerPos + (Vector3) ( 
                    _newXdir * (float) Math.Cos(_frequencyConstant*time) + 
                    _newYdir * (float) Math.Sin(_frequencyConstant * time) 
                    ) 
                * _distance * (float) Math.Pow(_reductionConstant, time );
        }

        private Vector2 GetVelocity(Vector3 newPlayerPos, float time)
        {
            if(time == 0) 
                return Vector2.zero;
            return ((Vector2)(newPlayerPos - _prevPlayerPos) / Time.deltaTime).normalized;

        }
    }
}