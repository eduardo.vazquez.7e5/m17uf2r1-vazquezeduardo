using UnityEngine;

namespace Scriptable_Objects.SceneData
{
    public abstract class TimedSceneData : SceneData
    {
        public float sceneDuration;
    }
}