using UnityEngine;

namespace Scriptable_Objects.SkillUserAI
{
    public abstract class SkillUserAI : ScriptableObject
    {
        public abstract bool Calculate(GameObject go);
    }
}