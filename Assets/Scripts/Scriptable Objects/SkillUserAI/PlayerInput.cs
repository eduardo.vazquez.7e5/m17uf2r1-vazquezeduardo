using Characters;
using Managers;
using UnityEngine;
using UnityEngine.InputSystem;

namespace Scriptable_Objects.SkillUserAI
{
    [CreateAssetMenu(fileName = "PlayerInput", menuName = "SkillAI/PlayerInput")]
    public class PlayerInput : SkillUserAI
    {
        private bool _useSkill = false;
        public override bool Calculate(GameObject go)
        {
            if (go.GetComponent<ISkillUser>() == null) return false;
            ISkillUser behaviour = go.GetComponent<ISkillUser>();

            behaviour.UseSkill = _useSkill && GameManager.Instance.IsGameActive;
            if(behaviour.UseSkill)
                EventManager.CallOnPlayerEnergyChange();
            _useSkill = false;
            
            return true;
        }
        
        public void UseSkill(InputAction.CallbackContext context)
        {
            if(context.performed)
                _useSkill = true;
        }
    }
}