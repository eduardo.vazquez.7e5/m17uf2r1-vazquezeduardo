using Characters;
using Managers;
using UnityEngine;
using UnityEngine.Serialization;

namespace Scriptable_Objects.SkillUserAI
{
    [CreateAssetMenu(fileName = "BasicSkilledAI", menuName = "SkillAI/BasicSkilledAI")]
    public class BasicSkilledAI : SkillUserAI
    {
        public float distanceFromPlayer;
        
        private Vector3 _playerPosition = new Vector3(0, 0, 0);
        public bool playerHasShot = false;
        public override bool Calculate(GameObject go)
        {
            if (go.GetComponent<ISkillUser>() == null) return false;
            ISkillUser behaviour = go.GetComponent<ISkillUser>();
            ListOfElements list = ListOfElements.Instance;
            if (list.player == null)
                return false;
            
            GameObject player = list.player;
            _playerPosition = player.GetComponent<Transform>().position;

            var dist = ((Vector2) go.GetComponent<Transform>().position - (Vector2) _playerPosition).magnitude;

            if (dist > distanceFromPlayer)
            {
                playerHasShot = false;
                behaviour.UseSkill = false;
                return true;
            }

            if (playerHasShot)
            {
                playerHasShot = false;
                behaviour.UseSkill = true;
                return true;
            }

            behaviour.UseSkill = false;
            return true;
        }
    }
}