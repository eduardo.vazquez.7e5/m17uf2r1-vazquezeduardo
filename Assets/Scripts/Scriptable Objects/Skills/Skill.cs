using UnityEngine;

namespace Scriptable_Objects.Skills
{
    public abstract class Skill : ScriptableObject
    {
        public string skillName;
        public int cost;
        public float energyCost;
        public float skillDuration;
        public float skillCooldown;
        public bool canBeCanceled;
        public Sprite inventoryIcon;

        internal abstract void SkillStart(GameObject go);
        internal abstract void SkillUpdate(GameObject go, float currentTime);
        internal abstract void SkillEnd(GameObject go);
    }
}