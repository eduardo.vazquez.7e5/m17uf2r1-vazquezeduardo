using UnityEngine;

namespace Scriptable_Objects.Skills
{
    [CreateAssetMenu(fileName = "Dash", menuName= "Skill/Dash")]
    public class Dash : Skill
    {
        public GameObject afterImage;
        public float afterImageCooldown = 0.15f;
        public float proportionOfMaxSpeedStartingDash = 1.3f;
        public float proportionOfMaxSpeedAfterDash = 0.5f;
        private static readonly int IsAccelerating = Animator.StringToHash("isAccelerating");

        internal override void SkillStart(GameObject go)
        {
            ShipController ship = go.GetComponent<ShipController>();
            ISkillUser skillUser = go.GetComponent<ISkillUser>();
            if (ship)
            {
                ship.gravityScale = 0f;
                go.GetComponent<BoxCollider2D>().enabled = false;
                skillUser.SkillDirection = ship.orientation;
                skillUser.CurrentAfterImageCooldown = afterImageCooldown;
            }
        }

        internal override void SkillUpdate(GameObject go, float current)
        {
            ISkillUser skillUser = go.GetComponent<ISkillUser>();
            ShipController ship = go.GetComponent<ShipController>();
            if (ship)
            {
                ship.accelerate = false;
                ship.decelerate = false;
                ship.orientation = skillUser.SkillDirection;
                go.GetComponent<Animator>().SetBool(IsAccelerating, true);
                go.GetComponent<Rigidbody2D>().velocity = ship.maxSpeed * skillUser.SkillDirection * GetProportionOfTime(current);
                skillUser.CurrentAfterImageCooldown += Time.deltaTime;
                if (skillUser.CurrentAfterImageCooldown >= afterImageCooldown)
                {
                    skillUser.CurrentAfterImageCooldown -= afterImageCooldown;
                    Instantiate(afterImage, go.GetComponent<Transform>().position, Quaternion.FromToRotation(new Vector3(0,1,0), skillUser.SkillDirection));
                }
            }
        }

        internal override void SkillEnd(GameObject go)
        {
            ShipController ship = go.GetComponent<ShipController>();
            if (ship)
            {
                ship.gravityScale = 1f;
                go.GetComponent<BoxCollider2D>().enabled = true;
            }
        }
        
        private float GetProportionOfTime(float current)
        {
            return proportionOfMaxSpeedStartingDash*(1 - current / skillDuration) + proportionOfMaxSpeedAfterDash * current / skillDuration;
        }
    }
}