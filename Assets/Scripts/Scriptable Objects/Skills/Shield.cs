using UnityEngine;
using UnityEngine.Serialization;

namespace Scriptable_Objects.Skills
{
    [CreateAssetMenu(fileName = "Shield", menuName= "Skill/Shield")]
    public class Shield : Skill
    {
        public GameObject shieldPrefab;
        public float energyCostBySecond = 15f;
        internal override void SkillStart(GameObject go)
        {
            var copy = Instantiate(shieldPrefab, go.GetComponent<Transform>());
            copy.tag = go.tag;
            go.GetComponent<ISkillUser>().PlaceHolderForSkills = copy;
        }

        internal override void SkillUpdate(GameObject go, float current)
        {
            go.GetComponent<ISkillUser>().CurrentEnergy -= energyCostBySecond * Time.deltaTime;
            if (go.GetComponent<ISkillUser>().CurrentEnergy <= 0f)
            {
                go.GetComponent<ISkillUser>().CurrentEnergy = 0f;
                go.GetComponent<ISkillUser>().SkillState.SetTrigger(SkillState.InUse, SkillState.Cooldown);
            }
        }

        internal override void SkillEnd(GameObject go)
        {
            Destroy(go.GetComponent<ISkillUser>().PlaceHolderForSkills);
        }
        
    }
}