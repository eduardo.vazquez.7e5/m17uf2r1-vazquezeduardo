using UnityEngine;

namespace Scriptable_Objects.Skills
{
    
    [CreateAssetMenu(fileName = "DamagingDash", menuName= "Skill/DamagingDash")]
    public class DamagingDash : Dash
    {
        public GameObject damagingArea;
        internal override void SkillStart(GameObject go)
        {
            base.SkillStart(go);
            go.GetComponent<ISkillUser>().PlaceHolderForSkills = Instantiate(damagingArea, go.GetComponent<Transform>());
            go.GetComponent<ISkillUser>().PlaceHolderForSkills.tag = go.tag;
        }

        internal override void SkillEnd(GameObject go)
        {
            base.SkillEnd(go);
            Destroy(go.GetComponent<ISkillUser>().PlaceHolderForSkills);
        }
    }
}
