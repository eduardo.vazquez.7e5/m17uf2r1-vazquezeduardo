using UnityEngine;

namespace Scriptable_Objects.Market
{
    [CreateAssetMenu(fileName = "New NotInterested", menuName = "Market/Option/NotInterested")]
    public class OptionNotInterested : Option
    {
        public override void OnClickDo(out bool overrideBehaviour)
        {
            overrideBehaviour = false;
        }
    }
}