using System.Collections.Generic;
using Scriptable_Objects.DataHolders;
using Scriptable_Objects.Weapons;
using UnityEngine;

namespace Scriptable_Objects.Market
{
    [CreateAssetMenu(fileName = "New BuyWeapon", menuName = "Market/Offer/BuyWeapon")]
    public class OfferBuyWeapon : Offer
    {
        internal Weapon weapon;
        [SerializeField] public List<Weapon> availableWeapons;
        
        public override bool IsAvailable()
        {
            if (foreverRejected) 
                return false;
            
            var inventory = Resources.Load<Inventory>("ScriptableObjectsInstances/Inventory");
            if (inventory.weapons.Count == inventory.maxNumberOfWeapons)
                return false;
            if (inventory.weapons.Contains(weapon))
                return false;
            var playerData = Resources.Load<PlayerData>("ScriptableObjectsInstances/PlayerData");
            if (playerData.coins < weapon.cost)
                return false;
            return true;
        }

        public override void SetUp()
        {
            base.SetUp();
            var inventory = Resources.Load<Inventory>("ScriptableObjectsInstances/Inventory");
            do
            {
                weapon = availableWeapons[Random.Range(0, availableWeapons.Count)];
            } while(inventory.weapons.Contains(weapon));

            firstPlaceholder = weapon.weaponName;
            secondPlaceholder = weapon.cost + " $";
        }
    }
}