using Scriptable_Objects.DataHolders;
using Scriptable_Objects.Weapons;
using UnityEngine;

namespace Scriptable_Objects.Market
{
    [CreateAssetMenu(fileName = "New SellWeapon", menuName = "Market/Offer/SellWeapon")]
    public class OfferSellWeapon : Offer
    {
        internal Weapon weapon;
        
        public override bool IsAvailable()
        {
            if (foreverRejected) 
                return false;
            
            var inventory = Resources.Load<Inventory>("ScriptableObjectsInstances/Inventory");
            if (inventory.weapons.Count == 1)
                return false;
            return true;
        }

        public override void SetUp()
        {
            base.SetUp();
            var inventory = Resources.Load<Inventory>("ScriptableObjectsInstances/Inventory");
            weapon = inventory.weapons[Random.Range(0, inventory.weapons.Count)];

            firstPlaceholder = weapon.weaponName;
            secondPlaceholder = weapon.cost + " $";
        }
    }
}