using Managers;
using Scriptable_Objects.DataHolders;
using UnityEngine;

namespace Scriptable_Objects.Market
{
    [CreateAssetMenu(fileName = "New BuyPowerUp", menuName = "Market/Option/BuyPowerUp")]
    public class OptionBuyPowerUp : Option
    {
        public override void OnClickDo(out bool overrideBehaviour)
        {
            var playerData = Resources.Load<PlayerData>("ScriptableObjectsInstances/PlayerData");
            playerData.coins -= ((OfferPowerUp) offer).cost;
            EventManager.CallOnPlayerPointChange();
            offer.foreverRejected = false;
            UIManager.Instance.shopPanel.GetComponent<MarketController>().UpdateOffers();
            UIManager.Instance.shopPanel.GetComponent<MarketController>().UpdateAvailability();
            UIManager.Instance.ToggleOffer();
            UIManager.Instance.powerupPanel.GetComponent<PowerUpPanelController>().RandomizePowerUps();
            UIManager.Instance.TogglePowerUp();
            overrideBehaviour = true;
        }
    }
}