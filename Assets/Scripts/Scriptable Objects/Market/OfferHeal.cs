using Scriptable_Objects.DataHolders;
using UnityEngine;

namespace Scriptable_Objects.Market
{
    [CreateAssetMenu(fileName = "New Heal", menuName = "Market/Offer/Heal")]
    public class OfferHeal : Offer
    {
        public float extraHull;
        public int cost;
        public override bool IsAvailable()
        {
            if (foreverRejected) 
                return false;
            
            var playerData = Resources.Load<PlayerData>("ScriptableObjectsInstances/PlayerData");
            if (playerData.coins < cost)
                return false;
            
            return true;
        }
        public override void SetUp()
        {
            base.SetUp();
            
            firstPlaceholder = extraHull.ToString();
            secondPlaceholder = cost + " $";
        }
    }
}