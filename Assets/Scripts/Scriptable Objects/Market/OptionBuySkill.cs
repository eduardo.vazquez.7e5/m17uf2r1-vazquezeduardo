using Characters;
using Managers;
using Scriptable_Objects.DataHolders;
using UnityEngine;

namespace Scriptable_Objects.Market
{
    [CreateAssetMenu(fileName = "New BuySkill", menuName = "Market/Option/BuySkill")]
    public class OptionBuySkill : Option
    {
        public override void OnClickDo(out bool overrideBehaviour)
        {
            overrideBehaviour = false;
            var playerData = Resources.Load<PlayerData>("ScriptableObjectsInstances/PlayerData");
            playerData.coins -= ((OfferBuySkill) offer).skill.cost;
            EventManager.CallOnPlayerPointChange();
            var inventory = Resources.Load<Inventory>("ScriptableObjectsInstances/Inventory");
            var player = ListOfElements.Instance.player;
            if (player == null)
                return;
            inventory.skill = ((OfferBuySkill) offer).skill;
            player.GetComponent<Player>().LoadSkillFromInventory();
            offer.foreverRejected = true;
        }
    }
}