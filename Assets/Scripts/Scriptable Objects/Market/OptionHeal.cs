using Characters;
using Managers;
using Scriptable_Objects.DataHolders;
using UnityEngine;

namespace Scriptable_Objects.Market
{
    [CreateAssetMenu(fileName = "New Heal", menuName = "Market/Option/Heal")]
    public class OptionHeal : Option
    {
        public override void OnClickDo(out bool overrideBehaviour)
        {
            overrideBehaviour = false;
            var playerData = Resources.Load<PlayerData>("ScriptableObjectsInstances/PlayerData");
            playerData.coins -= ((OfferHeal) offer).cost;
            EventManager.CallOnPlayerPointChange();
            playerData.maxHealth += ((OfferHeal) offer).extraHull;
            var player = ListOfElements.Instance.player;
            if (player == null)
                return;
            player.GetComponent<Player>().maxHull += ((OfferHeal) offer).extraHull;
            player.GetComponent<Player>().Heal(1000);
            offer.foreverRejected = true;
        }
    }
}