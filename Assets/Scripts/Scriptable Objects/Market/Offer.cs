using System.Collections.Generic;
using Scriptable_Objects.Weapons;
using UnityEngine;

namespace Scriptable_Objects.Market
{
    public abstract class Offer : ScriptableObject
    {
        public string titleDefault;
        public string titleEnglish;
        public Color textColor;
        public bool isTextLeft = true;
        public Sprite banner;
        public Sprite photo;
        [TextArea(10, 100)]
        public string defaultText;
        [TextArea(10, 100)]
        public string englishText;
        public List<Option> options;
        internal bool foreverRejected;
        internal string firstPlaceholder;
        internal string secondPlaceholder;

        public virtual bool IsAvailable()
        {
            return !foreverRejected;
        }

        public virtual void SetUp()
        {
            foreverRejected = false;
        }
    }
}