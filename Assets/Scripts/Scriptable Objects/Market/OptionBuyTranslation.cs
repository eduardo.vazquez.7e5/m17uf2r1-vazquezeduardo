using Managers;
using Scriptable_Objects.DataHolders;
using UnityEngine;

namespace Scriptable_Objects.Market
{
    [CreateAssetMenu(fileName = "New BuyTranslation", menuName = "Market/Option/BuyTranslation")]
    public class OptionBuyTranslation : Option
    {
        public override void OnClickDo(out bool overrideBehaviour)
        {
            var playerData = Resources.Load<PlayerData>("ScriptableObjectsInstances/PlayerData");
            playerData.coins -= ((OfferTranslation) offer).cost;
            playerData.hasTranslator = true;
            EventManager.CallOnPlayerPointChange();
            offer.foreverRejected = true;
            overrideBehaviour = false;
        }
    }
}