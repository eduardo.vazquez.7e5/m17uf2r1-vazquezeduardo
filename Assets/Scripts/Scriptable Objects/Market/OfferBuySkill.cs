using System.Collections.Generic;
using Scriptable_Objects.DataHolders;
using Scriptable_Objects.Skills;
using Scriptable_Objects.Weapons;
using UnityEngine;

namespace Scriptable_Objects.Market
{
    [CreateAssetMenu(fileName = "New BuySkill", menuName = "Market/Offer/BuySkill")]
    public class OfferBuySkill : Offer
    {
        internal Skill skill;
        [SerializeField] public List<Skill> availableSkills;
        
        public override bool IsAvailable()
        {
            if (foreverRejected) 
                return false;
            
            var inventory = Resources.Load<Inventory>("ScriptableObjectsInstances/Inventory");
            if (inventory.skill == skill)
                return false;
            var playerData = Resources.Load<PlayerData>("ScriptableObjectsInstances/PlayerData");
            if (playerData.coins < skill.cost)
                return false;
            
            return true;
        }

        public override void SetUp()
        {
            base.SetUp();
            var inventory = Resources.Load<Inventory>("ScriptableObjectsInstances/Inventory");
            do
            {
                skill = availableSkills[Random.Range(0, availableSkills.Count)];
            } while(inventory.skill == skill);

            firstPlaceholder = skill.skillName;
            secondPlaceholder = skill.cost + " $";
        }
    }
}