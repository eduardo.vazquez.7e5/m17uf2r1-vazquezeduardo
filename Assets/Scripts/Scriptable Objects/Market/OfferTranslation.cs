using Scriptable_Objects.DataHolders;
using UnityEngine;

namespace Scriptable_Objects.Market
{
    [CreateAssetMenu(fileName = "New Translation", menuName = "Market/Offer/Translation")]
    public class OfferTranslation : Offer
    {
        public int cost;
        public override bool IsAvailable()
        {
            if (foreverRejected) 
                return false;
            
            var playerData = Resources.Load<PlayerData>("ScriptableObjectsInstances/PlayerData");
            if (playerData.coins < cost)
                return false;
            
            return !playerData.hasTranslator;
        }

        public override void SetUp()
        {
            base.SetUp();
            firstPlaceholder = cost + " $";
        }
    }
}