using Managers;
using Scriptable_Objects.DataHolders;
using UnityEngine;

namespace Scriptable_Objects.Market
{
    [CreateAssetMenu(fileName = "New BuyWeapon", menuName = "Market/Option/BuyWeapon")]
    public class OptionBuyWeapon : Option
    {
        public override void OnClickDo(out bool overrideBehaviour)
        {
            overrideBehaviour = false;
            var playerData = Resources.Load<PlayerData>("ScriptableObjectsInstances/PlayerData");
            playerData.coins -= ((OfferBuyWeapon) offer).weapon.cost;
            EventManager.CallOnPlayerPointChange();
            var inventory = Resources.Load<Inventory>("ScriptableObjectsInstances/Inventory");
            var player = ListOfElements.Instance.player;
            if (player == null)
                return;
            inventory.AddWeapon(((OfferBuyWeapon) offer).weapon, player);
            offer.foreverRejected = true;
        }
    }
}