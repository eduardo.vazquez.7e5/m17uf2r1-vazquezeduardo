using UnityEngine;

namespace Scriptable_Objects.Market
{
    [CreateAssetMenu(fileName = "New BeRude", menuName = "Market/Option/BeRude")]
    public class OptionBeRude : Option
    {
        public override void OnClickDo(out bool overrideBehaviour)
        {
            offer.foreverRejected = true;
            overrideBehaviour = false;
        }
    }
}