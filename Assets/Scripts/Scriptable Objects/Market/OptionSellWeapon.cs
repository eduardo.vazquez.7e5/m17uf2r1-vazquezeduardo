using Managers;
using Scriptable_Objects.DataHolders;
using UnityEngine;

namespace Scriptable_Objects.Market
{
    [CreateAssetMenu(fileName = "New SellWeapon", menuName = "Market/Option/SellWeapon")]
    public class OptionSellWeapon : Option
    {
        public override void OnClickDo(out bool overrideBehaviour)
        {
            var playerData = Resources.Load<PlayerData>("ScriptableObjectsInstances/PlayerData");
            playerData.coins += ((OfferSellWeapon) offer).weapon.cost;
            EventManager.CallOnPlayerPointChange();
            var inventory = Resources.Load<Inventory>("ScriptableObjectsInstances/Inventory");
            inventory.RemoveWeapon(((OfferSellWeapon) offer).weapon);
            offer.foreverRejected = true;
            overrideBehaviour = false;
        }
    }
}