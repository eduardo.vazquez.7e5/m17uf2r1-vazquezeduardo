using UnityEngine;

namespace Scriptable_Objects.Market
{
    public abstract class Option : ScriptableObject
    {
        public string defaultText;
        public string englishText;
        public Offer offer;

        public abstract void OnClickDo(out bool overrideBehaviour);
    }
}