using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Scriptable_Objects.Weapons
{
    [CreateAssetMenu(fileName= "New EMPWeapon", menuName = "Weapon/EMPWeapon")]
    public class EMPWeapon: MeleeWeapon
    {
        public GameObject empDisplayer;
        public override void SetUp(GameObject go)
        {
            var emp = Instantiate(empDisplayer, go.GetComponent<Transform>());
            emp.tag = go.tag;
            go.GetComponent<IMelleeAttacker>().WeaponInstances = new List<GameObject>();
            go.GetComponent<IMelleeAttacker>().WeaponInstances.Add(emp);
        }

        public override void Attack(GameObject go)
        {
            base.Attack(go);
            go.GetComponent<IMelleeAttacker>().WeaponInstances[0].GetComponent<EMPController>().attack = true;
        }

        public override void End(GameObject go)
        {
            while (go.GetComponent<IMelleeAttacker>().WeaponInstances.Any())
            {
                Destroy(go.GetComponent<IMelleeAttacker>().WeaponInstances[0]);
                go.GetComponent<IMelleeAttacker>().WeaponInstances.RemoveAt(0);
            }
        }
    }
}