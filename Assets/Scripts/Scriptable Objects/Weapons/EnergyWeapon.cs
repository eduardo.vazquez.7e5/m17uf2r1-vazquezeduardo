using UnityEngine;

namespace Scriptable_Objects.Weapons
{
    [CreateAssetMenu(fileName= "New EnergyWeapon", menuName = "Weapon/EnergyWeapon")]
    public class EnergyWeapon : RangedWeapon
    {
        public float energyCostByBurst;
    }
}
