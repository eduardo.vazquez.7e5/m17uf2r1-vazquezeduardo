using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Scriptable_Objects.Weapons
{
    [CreateAssetMenu(fileName= "New FlamethrowerWeapon", menuName = "Weapon/FlamethrowerWeapon")]
    public class FlamethrowerWeapon: MeleeWeapon
    {
        public GameObject flamethrower;
        public override void SetUp(GameObject go)
        {
            var flameT = Instantiate(flamethrower, go.GetComponent<Transform>());
            flameT.tag = go.tag;
            go.GetComponent<IMelleeAttacker>().WeaponInstances = new List<GameObject>();
            go.GetComponent<IMelleeAttacker>().WeaponInstances.Add(flameT);
        }

        public override void Attack(GameObject go)
        {
            base.Attack(go);
            go.GetComponent<IMelleeAttacker>().WeaponInstances[0].GetComponent<FlamethrowerController>().attack = true;
        }

        public override void End(GameObject go)
        {
            while (go.GetComponent<IMelleeAttacker>().WeaponInstances.Any())
            {
                Destroy(go.GetComponent<IMelleeAttacker>().WeaponInstances[0]);
                go.GetComponent<IMelleeAttacker>().WeaponInstances.RemoveAt(0);
            }
        }
    }
}