using UnityEngine;

namespace Scriptable_Objects.Weapons
{
    [CreateAssetMenu(fileName = "New MeleeWeapon", menuName = "Weapon/MeleeWeapon")]
    public abstract class MeleeWeapon : Weapon
    {
        public float cooldownTime;

        public abstract void SetUp(GameObject go);

        public virtual void Attack(GameObject go)
        {
            go.GetComponent<AudioSource>().Play();
        }
        public abstract void End(GameObject go);
    }
}