using UnityEngine;

namespace Scriptable_Objects.Weapons
{
    [CreateAssetMenu(fileName= "New Firearm", menuName = "Weapon/Firearm")]
    public class Firearm : RangedWeapon
    {
        public int numberOfMagazinesByDrop;
    }
}