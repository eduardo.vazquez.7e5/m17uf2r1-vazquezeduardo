using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Scriptable_Objects.Weapons
{
    [CreateAssetMenu(fileName= "New OrbWeapon", menuName = "Weapon/OrbWeapon")]
    public class OrbWeapon : MeleeWeapon
    {
        public OrbScriptableObject orbData1;
        public OrbScriptableObject orbData2;
        public GameObject orbDisplayer;
        public override void SetUp(GameObject go)
        {
            go.GetComponent<IMelleeAttacker>().WeaponInstances = new List<GameObject>();
            var orb = Instantiate(orbDisplayer, go.transform);
            orb.GetComponent<OrbController>().LoadOrb(orbData1, go);
            orb.GetComponent<OrbController>().SetAngles(0f, orbData1.rotation);
            go.GetComponent<IMelleeAttacker>().WeaponInstances.Add(orb);
            orb = Instantiate(orbDisplayer, go.transform);
            orb.GetComponent<OrbController>().LoadOrb(orbData2, go);
            orb.GetComponent<OrbController>().SetAngles(0f, orbData2.rotation);
            go.GetComponent<IMelleeAttacker>().WeaponInstances.Add(orb);
            orb = Instantiate(orbDisplayer, go.transform);
            orb.GetComponent<OrbController>().LoadOrb(orbData1, go);
            orb.GetComponent<OrbController>().SetAngles(180f, orbData1.rotation);
            go.GetComponent<IMelleeAttacker>().WeaponInstances.Add(orb);
            orb = Instantiate(orbDisplayer, go.transform);
            orb.GetComponent<OrbController>().LoadOrb(orbData2, go);
            orb.GetComponent<OrbController>().SetAngles(180f, orbData2.rotation);
            go.GetComponent<IMelleeAttacker>().WeaponInstances.Add(orb);
        }

        public override void Attack(GameObject go)
        {
            base.Attack(go);
            go.GetComponent<IMelleeAttacker>().WeaponInstances[0].GetComponent<OrbController>().attack = true;
            go.GetComponent<IMelleeAttacker>().WeaponInstances.Add(go.GetComponent<IMelleeAttacker>().WeaponInstances[0]);
            go.GetComponent<IMelleeAttacker>().WeaponInstances.RemoveAt(0);
        }

        public override void End(GameObject go)
        {
            while (go.GetComponent<IMelleeAttacker>().WeaponInstances.Any())
            {
                Destroy(go.GetComponent<IMelleeAttacker>().WeaponInstances[0]);
                go.GetComponent<IMelleeAttacker>().WeaponInstances.RemoveAt(0);
            }
        }
    }
}