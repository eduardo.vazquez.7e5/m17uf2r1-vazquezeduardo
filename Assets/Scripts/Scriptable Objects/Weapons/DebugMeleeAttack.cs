using UnityEngine;

namespace Scriptable_Objects.Weapons
{
    [CreateAssetMenu(fileName= "New DebugMeleeAttack", menuName = "Weapon/DebugMeleeAttack")]
    public class DebugMeleeAttack : MeleeWeapon
    {
        public int numberOfPimPam;
        public override void SetUp(GameObject go)
        {
            Debug.Log("debug melee start");
            numberOfPimPam = 0;
        }

        public override void Attack(GameObject go)
        {
            base.Attack(go);
            Debug.Log(numberOfPimPam++ + "PIM PAM TOMA LACASITOS!");
        }

        public override void End(GameObject go)
        {
            Debug.Log("ta luego lucas!");
        }
    }
}