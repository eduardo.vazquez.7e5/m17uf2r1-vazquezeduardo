using UnityEngine;

namespace Scriptable_Objects
{
    [CreateAssetMenu(fileName= "New OrbScriptableObject", menuName = "Weapon/OrbScriptableObject")]
    public class OrbScriptableObject : ScriptableObject
    {
        public float damage;
        public float frequency = 0f;
        public float xRadius = 0f;
        public float yRadius = 0f;
        public float offset = 0f;
        public float rotation = 0f;

        public float attackDistance;
        public float attackTime;
        public float sizeScale;
        public float constantPush;
    }
}