using UnityEngine;

namespace Scriptable_Objects.Weapons
{
    [CreateAssetMenu(fileName = "New RangedWeapon", menuName = "Weapon/RangedWeapon")]
    public class RangedWeapon : Weapon
    {
        public float bulletSpeed;
        public Sprite bullet;
        public GameObject bulletprefab;
        public float bulletLifeTime;
        public float bulletWeight;

        public float timeBetweenBullets;
        public int bulletsByBurst;
        public float timeBetweenBursts;
        public int burstByMagazine;
        public float timeOfReload;

        public float accuracy;
    }
}
