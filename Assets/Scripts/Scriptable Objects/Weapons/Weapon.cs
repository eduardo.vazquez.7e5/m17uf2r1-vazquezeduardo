using UnityEngine;

namespace Scriptable_Objects.Weapons
{
    [CreateAssetMenu(fileName = "New Weapon", menuName = "Weapon/BasicWeapon")]
    public class Weapon : ScriptableObject
    {
        public string weaponName;
        public float damage;
        public bool autofire;
        public int cost;
        public Sprite weaponSprite;
        public AudioClip attackingSound;
    }
}
