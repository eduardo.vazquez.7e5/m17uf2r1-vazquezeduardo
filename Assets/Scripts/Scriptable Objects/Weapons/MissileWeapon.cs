using UnityEngine;

namespace Scriptable_Objects.Weapons
{
    [CreateAssetMenu(fileName= "New MissileWeapon", menuName = "Weapon/MissileWeapon")]
    public class MissileWeapon : Firearm
    {
        public float constantPush = 15f;
    }
}