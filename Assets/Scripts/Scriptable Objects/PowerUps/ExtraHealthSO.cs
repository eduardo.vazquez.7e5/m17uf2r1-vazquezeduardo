using UnityEngine;
using Managers;
using Characters;
using Scriptable_Objects.DataHolders;
using UnityEditor;

namespace Scriptable_Objects.PowerUps
{
    [CreateAssetMenu(fileName = "New PowerUpExtraHealth", menuName = "Power Up/Extra Health")]
    public class ExtraHealthSO : PowerUpSO
    {
        public float extraHealth;
        public override void OnObtainDo()
        {
            ListOfElements list = ListOfElements.Instance;
            if(list.player != null)
            {
                var player = list.player.GetComponent<Player>();
                player.maxHull += extraHealth;
                player.currentHull = player.maxHull;
                var pData = Resources.Load<PlayerData>("ScriptableObjectsInstances/PlayerData");
                EditorUtility.SetDirty(pData);
                pData.maxHealth += extraHealth;
                EventManager.CallOnPlayerHealthChange();
            }
        }
    }
}