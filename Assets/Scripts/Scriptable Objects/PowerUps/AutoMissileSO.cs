using UnityEngine;
using Managers;
using Characters;

namespace Scriptable_Objects.PowerUps
{
    [CreateAssetMenu(fileName = "New PowerUpAutoMissile", menuName = "Power Up/AutoMissile")]
    public class AutoMissileSO : PowerUpSO
    {
        public override void OnObtainDo()
        {
            ListOfElements list = ListOfElements.Instance;
            if (list.player != null)
            {
                var player = list.player.GetComponent<Player>();
                player.missileUpgrades++;
            }
        }
    }
}