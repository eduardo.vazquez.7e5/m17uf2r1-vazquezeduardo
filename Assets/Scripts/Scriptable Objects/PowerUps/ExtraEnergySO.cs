using UnityEngine;
using Managers;
using Scriptable_Objects.DataHolders;
using UnityEditor;

namespace Scriptable_Objects.PowerUps
{
    [CreateAssetMenu(fileName = "New PowerUpExtraEnergy", menuName = "Power Up/Extra Energy")]
    public class ExtraEnergySO : PowerUpSO
    {
        public float extraEnergy;
        public override void OnObtainDo()
        {
            ListOfElements list = ListOfElements.Instance;
            if (list.player != null)
            {
                var player = list.player.GetComponent<ISkillUser>();
                player.MaxEnergy += extraEnergy;
                player.CurrentEnergy = player.MaxEnergy;
                var pData = Resources.Load<PlayerData>("ScriptableObjectsInstances/PlayerData");
                EditorUtility.SetDirty(pData);
                pData.maxEnergy += extraEnergy;
                EventManager.CallOnPlayerEnergyChange();
            }
        }
    }
}