using UnityEngine;

namespace Scriptable_Objects.PowerUps
{
    public abstract class PowerUpSO : ScriptableObject
    {
        [SerializeField] public string description;
        [SerializeField] public Sprite icon;
        public abstract void OnObtainDo();
    }
}