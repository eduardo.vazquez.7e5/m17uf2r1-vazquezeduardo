using System.Collections.Generic;
using UnityEngine;

namespace Scriptable_Objects.DataHolders
{
    [CreateAssetMenu(fileName = "New PersistentData", menuName = "DataHolders/PersistentData")]
    public class PersistentDataSO : ScriptableObject
    {
        public List<int> Scores;
        public List<string> Names;
        public int NumberOfKilledEnemies;

        // https://www.youtube.com/watch?v=uD7y4T4PVk0

        public string GetSerializedData()
        {
            TokenPersistentPlayerData tppd = new TokenPersistentPlayerData(this);
            return JsonUtility.ToJson(tppd);
        }
        
        public void SaveSerializedData()
        {
            string serialized = GetSerializedData();
            FileManager.WriteToFile("USR.json", serialized);
        }

        public void LoadSerializedData()
        {
            string jsonData;
            if (FileManager.LoadFromFile("USR.json", out jsonData))
            {
                TokenPersistentPlayerData loaded = JsonUtility.FromJson<TokenPersistentPlayerData>(jsonData.ToString());
                Scores = loaded.Scores;
                Names = loaded.Names;
                NumberOfKilledEnemies = loaded.NumberOfKilledEnemies;
            }
        }
    }

    [System.Serializable]
    public class TokenPersistentPlayerData
    {
        public List<int> Scores;
        public List<string> Names;
        public int NumberOfKilledEnemies;
        public string LastNameUsed;

        public TokenPersistentPlayerData(PersistentDataSO ppdso)
        {
            Scores = ppdso.Scores;
            Names = ppdso.Names;
            NumberOfKilledEnemies = ppdso.NumberOfKilledEnemies;
        }
    }
}