using System.Collections.Generic;
using Characters;
using Managers;
using Scriptable_Objects.Skills;
using Scriptable_Objects.Weapons;
using UnityEngine;

namespace Scriptable_Objects.DataHolders
{
    [CreateAssetMenu(fileName= "New Inventory", menuName = "DataHolders/Inventory")]
    public class Inventory : ScriptableObject
    {
        public List<Weapon> weapons = new ();
        public int[] ammunition = new int[5];
        public int maxNumberOfWeapons = 5;
        public Skill skill;

        public void ResetValues()
        {
            weapons = new();
            Weapon weapon = Resources.Load<Weapon>("ScriptableObjectsInstances/Weapons/EnergyWeapon/BasePlayerWeapon");
            weapons.Add(weapon);
            ammunition = new int[5]; 
            skill = Resources.Load<Skill>("ScriptableObjectsInstances/Skills/PlayerDash");
        }

        public void LoadBrokenWeapon()
        {
            weapons = new();
            Weapon weapon = Resources.Load<Weapon>("ScriptableObjectsInstances/Weapons/EnergyWeapon/BrokenWeapon");
            weapons.Add(weapon);
            ammunition = new int[5]; 
            skill = Resources.Load<Skill>("ScriptableObjectsInstances/Skills/DamagingDash");
        }
        
        public void AddWeapon(Weapon weapon, GameObject go)
        {
            int index = FindIndex(weapon);
            bool isAlreadyInInventory = index != -1;
            if (!isAlreadyInInventory && weapons.Count < maxNumberOfWeapons)
            {
                weapons.Add(weapon);
                if (weapon is Firearm firearm)
                    ammunition[weapons.Count-1] = firearm.numberOfMagazinesByDrop;
                EventManager.CallOnPlayerWeaponChange();
                
            }
            else if (isAlreadyInInventory)
            {
                if(weapon is Firearm firearm)
                    ammunition[index] += firearm.numberOfMagazinesByDrop;
                if (weapon is EnergyWeapon)
                {
                    go.GetComponent<ISkillUser>().FullEnergyRecharge();
                    EventManager.CallOnPlayerEnergyChange();
                }
                EventManager.CallOnPlayerWeaponChange();
            }
            else
            {
                // Need to overwrite a weapon
            }
        }

        public void RemoveWeapon(Weapon weapon)
        {
            int index = FindIndex(weapon);
            if (index != -1)
            {
                var player = ListOfElements.Instance.player;
                if (player != null)
                {
                    var controller = player.GetComponent<Player>();
                    if (controller.weaponIndex == index)
                    {
                        controller.weaponIndex = (controller.weaponIndex+1) % weapons.Count;
                        controller.LoadWeaponFromInventory();
                    }
                    if (controller.weaponIndex > index)
                        controller.weaponIndex--;
                }
                weapons.RemoveAt(index);
                EventManager.CallOnPlayerWeaponChange();
            }
        }

        public int GetAmmunition(Weapon weapon)
        {
            int index = FindIndex(weapon);
            if (index != -1 && ammunition[index] != -1)
                return ammunition[index];
            return 0;
        }

        private int FindIndex(Weapon weapon)
        {
            for (int i = 0; i < weapons.Count; i++)
            {
                if (weapons[i] != null && weapons[i] == weapon)
                    return i;
            }
            return -1;
        }
    }
}