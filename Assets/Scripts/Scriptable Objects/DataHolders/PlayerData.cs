using UnityEngine;

public enum Gender
{
    Bangs,
    Glasses
}

public enum PlayerState
{
    Default,
    Worried,
    VeryWorried,
    Confident,
    Defeated
}

namespace Scriptable_Objects.DataHolders
{
    [CreateAssetMenu(fileName= "New PlayerData", menuName = "DataHolders/PlayerData")]
    public class PlayerData : ScriptableObject
    {
        public string playerName;
        public Gender gender;
        public Sprite[] avatar;
        public float currentHealth;
        public float maxHealth;
        public float maxEnergy;
        public int missileUpgrades;
        public int coins;
        public bool hasTranslator;
        public int points;
        public GameResult gameResult;

        public void ResetValues()
        {
            playerName = "";
            gender = Gender.Bangs;
            avatar = new Sprite[5];
            currentHealth = 80f;
            maxHealth = 80f;
            maxEnergy = 80f;
            missileUpgrades = 0;
            coins = 0;
            hasTranslator = false;
            points = 0;
        }
        
        public void LoadSprites()
        {
            string path = "Sprites/";
            switch (gender)
            {
                case Gender.Bangs:
                    path += "Bangs/";
                    break;
                case Gender.Glasses:
                    path += "Glasses/";
                    break;
            }

            avatar = new Sprite[5];
            for (int i = 0; i < 5; i++)
                avatar[i] = Resources.Load<Sprite>(path + i);
        }

        public Sprite GetAvatar(PlayerState playerState)
        {
            switch (playerState)
            {
                case PlayerState.Default:
                    return avatar[0];
                case PlayerState.Worried:
                    return avatar[1];
                case PlayerState.VeryWorried:
                    return avatar[2];
                case PlayerState.Confident:
                    return avatar[3];
                case PlayerState.Defeated:
                    return avatar[4];
            }

            return avatar[0];
        }
    }
}