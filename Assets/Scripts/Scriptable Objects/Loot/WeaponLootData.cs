using Scriptable_Objects.Weapons;
using UnityEngine;

namespace Scriptable_Objects.Loot
{
    [CreateAssetMenu(fileName = "New WeaponLootData", menuName = "LootData/WeaponLootData")]
    public class WeaponLootData : LootData
    {
        public Weapon weapon;
    }
}