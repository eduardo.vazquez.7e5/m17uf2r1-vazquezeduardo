using Scriptable_Objects.Skills;
using UnityEngine;

namespace Scriptable_Objects.Loot
{
    [CreateAssetMenu(fileName = "New SkillLootData", menuName = "LootData/SkillLootData")]
    public class SkillLootData : LootData
    {
        public Skill skill;
    }
}