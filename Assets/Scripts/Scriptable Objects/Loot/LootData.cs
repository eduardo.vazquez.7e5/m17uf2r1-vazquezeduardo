using UnityEngine;

namespace Scriptable_Objects.Loot
{
    [CreateAssetMenu(fileName = "New LootData", menuName = "LootData/Basic")]
    public class LootData : ScriptableObject
    {
        public GameObject lootDisplayer;
        public Sprite sprite;
        public float timeToDespawn = 5f;
    }
}