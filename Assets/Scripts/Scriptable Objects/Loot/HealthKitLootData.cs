using Scriptable_Objects.Weapons;
using UnityEngine;

namespace Scriptable_Objects.Loot
{
    [CreateAssetMenu(fileName = "New HealthKitLootData", menuName = "LootData/HealthKitLootData")]
    public class HealthKitLootData : LootData
    {
        public float health;
    }
}