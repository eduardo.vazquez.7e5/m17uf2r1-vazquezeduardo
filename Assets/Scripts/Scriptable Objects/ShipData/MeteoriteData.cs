using System.Collections.Generic;
using Characters;
using Scriptable_Objects.Loot;
using UnityEngine;

namespace Scriptable_Objects.ShipData
{
    [CreateAssetMenu(fileName = "New MeteoriteData", menuName = "DestroyableCharacterData/MeteoriteData")]
    public class MeteoriteData : ShipData
    {
        public float maxSpawnSpeed = 2f;
        public GameObject childrenSpawned;
        public MeteoriteData childrenData;
        public int numberOfSpawnedChildren;
        public float minSize;
        public float maxSize;
        public int coins;
        public List<LootData> lootData;
        public List<int> probabilityWeight;
    }
}