using Scriptable_Objects.Weapons;
using UnityEngine;

namespace Scriptable_Objects.ShipData
{
    [CreateAssetMenu(fileName = "New ShipData", menuName = "DestroyableCharacterData/Shooter")]
    public class ShooterData : ShipData
    {
        public EnergyWeapon weapon;
        public ShootingAI.ShootingAI shootingAi;
    }
}
