using System.Collections.Generic;
using Scriptable_Objects.Loot;
using UnityEngine;

namespace Scriptable_Objects.ShipData
{
    [CreateAssetMenu(fileName = "New EnemyShooterData", menuName = "DestroyableCharacterData/EnemyShooterData")]
    public class EnemyShooterData : ShooterData
    {
        public int coins;
        public List<LootData> lootData;
        public List<int> probabilityWeight;
    }
}