using System.Collections.Generic;
using Scriptable_Objects.Loot;
using Scriptable_Objects.Skills;
using UnityEngine;
using UnityEngine.Serialization;

namespace Scriptable_Objects.ShipData
{
    [CreateAssetMenu(fileName = "New SkilledEnemySO", menuName = "DestroyableCharacterData/SkilledEnemySO")]
    public class SkilledEnemySO : ShooterData
    {
        public int coins;
        public List<LootData> lootData;
        public List<int> probabilityWeight;
        public float maxEnergy;
        public Skill skill;
        public SkillUserAI.SkillUserAI skillUserAi;
        public float timeToRecharge;
        public float energyRecharged;
    }
}