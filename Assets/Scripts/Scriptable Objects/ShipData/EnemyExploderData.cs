using System.Collections.Generic;
using Scriptable_Objects.Loot;
using UnityEngine;

namespace Scriptable_Objects.ShipData
{
    [CreateAssetMenu(fileName = "New EnemyExploderData", menuName = "DestroyableCharacterData/EnemyExploderData")]
    public class EnemyExploderData : ShipData
    {
        public int coins;
        public List<LootData> lootData;
        public List<int> probabilityWeight;
    }
}