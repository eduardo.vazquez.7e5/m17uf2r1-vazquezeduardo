using System.Collections.Generic;
using Scriptable_Objects.Loot;
using UnityEditor.Animations;
using UnityEngine;

namespace Scriptable_Objects.ShipData
{
    [CreateAssetMenu(fileName = "New BossData", menuName = "DestroyableCharacterData/BossData")]
    public class BossData : ShipData
    {
        public float movementRadius;
        public float angleOffset;
        public int coins;
        public List<LootData> lootData;
        public List<int> probabilityWeight;
    }
}