using UnityEditor.Animations;
using UnityEngine;

namespace Scriptable_Objects.ShipData
{
    [CreateAssetMenu(fileName = "New DestroyableCharacterData", menuName = "DestroyableCharacterData/Basic")]
    public class DestroyableCharacterData : ScriptableObject
    {
        public float maxHull;
        public int rewardPoints = 0;
        public GameObject onDeathExplosion;
    }
}