using UnityEditor.Animations;
using UnityEngine;

namespace Scriptable_Objects.ShipData
{
    [CreateAssetMenu(fileName = "New ShipData", menuName = "DestroyableCharacterData/ShipData")]
    public class ShipData : DestroyableCharacterData
    {
        public float acceleration;
        public float deceleration;
        public float maxSpeed;
        public float mass;
        public float crashDamage;
        public float gravityScale = 1f;
        public AnimatorController animator;
        public MovementAI.MovementAI[] movementAIs;
        public float timeToLookInFront;
        public float distanceFromPlanet;
        public float distanceFromPlayer;
    }
}
