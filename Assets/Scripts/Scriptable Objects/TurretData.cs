using Scriptable_Objects.Weapons;
using UnityEngine;

namespace Scriptable_Objects
{
    [CreateAssetMenu(fileName= "New TurretData", menuName = "TurretData")]
    public class TurretData : ScriptableObject
    {
        [SerializeField]
        public MeleeWeapon meleeWeapon;
        public float maxRotation;
        public float aimingTime;
        public float aimingSpeed; // In degrees / second !!
    }
}