using System;
using System.Collections.Generic;
using Classes;
using Managers;
using Scriptable_Objects.Loot;
using Scriptable_Objects.ShipData;
using Scriptable_Objects.Skills;
using Scriptable_Objects.SkillUserAI;
using UnityEngine;
using UnityEngine.Serialization;

namespace Characters
{
    public class SkilledEnemyController : ShootingController, ILootableCharacter, ISkillUser
    {
        private List<LootData> _loot;
        public int Coins { get; set; }
        public List<LootData> Loot { get => _loot; set => _loot = value; }
        private List<int> _probabilityWeight;
        private float _currentEnergy;
        public float maxEnergy;
        public float timeToRecharge;
        private float _currentTimeToRecharge;
        public float energyRecharged;
        private Skill _currentSkill;
        private FiniteStateMachine<SkillState> _skillState;
        public List<int> ProbabilityWeight { get => _probabilityWeight; set => _probabilityWeight = value; }

        protected override void Start()
        {
            base.Start();
            GetComponent<ISkillUser>().StartSkillUser(gameObject);
            EventManager.OnPlayerHasShot += PlayerHasShot;
        }

        protected override void Update()
        {
            base.Update();
            (this as ISkillUser).UpdateSkillUser(gameObject);
        }

        protected void OnDestroy()
        {
            EventManager.OnPlayerHasShot -= PlayerHasShot;
        }

        internal override void LoadData(DestroyableCharacterData shooterData)
        {
            base.LoadData(shooterData);
            _probabilityWeight = new List<int>();
            if (shooterData is SkilledEnemySO esData)
            {
                Coins = esData.coins;
                _loot = esData.lootData;
                _probabilityWeight = esData.probabilityWeight;
                maxEnergy = esData.maxEnergy;
                timeToRecharge = esData.timeToRecharge;
                energyRecharged = esData.energyRecharged;
                _currentEnergy = maxEnergy;
                (this as ISkillUser).SkillUserAI = esData.skillUserAi;
                _currentSkill = esData.skill;
                GetComponent<ISkillUser>().LoadSkill(_currentSkill, this.gameObject);
            }
        }

        private void PlayerHasShot()
        {
            if (SkillUserAI is BasicSkilledAI basic)
                basic.playerHasShot = true;
        }

        float ISkillUser.CurrentEnergy
        {
            get => _currentEnergy;
            set => _currentEnergy = value;
        }

        float ISkillUser.MaxEnergy
        {
            get => maxEnergy;
            set => maxEnergy = value;
        }

        float ISkillUser.TimeToRecharge
        {
            get => timeToRecharge;
            set => timeToRecharge = value;
        }

        float ISkillUser.CurrentTimeToRecharge
        {
            get => _currentTimeToRecharge;
            set => _currentTimeToRecharge = value;
        }

        float ISkillUser.EnergyRecharged
        {
            get => energyRecharged;
            set => energyRecharged = value;
        }

        public bool UseSkill { get; set; }
        public SkillUserAI SkillUserAI { get; set; }

        Skill ISkillUser.CurrentSkill
        {
            get => _currentSkill;
            set => _currentSkill = value;
        }

        public Vector2 SkillDirection { get; set; }
        public float CurrentAfterImageCooldown { get; set; }
        public bool HasFinishedSettingUp { get; set; }
        public GameObject PlaceHolderForSkills { get; set; }

        FiniteStateMachine<SkillState> ISkillUser.SkillState
        {
            get => _skillState;
            set => _skillState = value;
        }

        public void OnEneryRecharge() { }
    }
}