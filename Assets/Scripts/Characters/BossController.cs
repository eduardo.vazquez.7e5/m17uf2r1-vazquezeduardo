using System.Collections.Generic;
using Managers;
using Scriptable_Objects.Loot;
using Scriptable_Objects.ShipData;
using UnityEngine;

namespace Characters
{
    public class BossController : ShipController, ILootableCharacter
    {
        public int Coins { get; set; }
        public List<LootData> Loot { get; set; }
        public List<int> ProbabilityWeight { get; set; }
        private float _movementRadius;
        private float _angleOffset;
        private float _frequencySpeed;
        private float _bossTime;
        private Vector3 _gameCenter;

        protected override void Start()
        {
            _bossTime = 0f;
            base.Start();
        }

        protected override void Update()
        {
            _bossTime += Time.deltaTime;
            base.Update();
        }

        internal override void LoadData(DestroyableCharacterData dcData)
        {
            base.LoadData(dcData);
            if (dcData is BossData bData)
            {
                _movementRadius = bData.movementRadius;
                _angleOffset = bData.angleOffset;
                _gameCenter = GameManager.Instance.GetComponent<Transform>().position;
                _frequencySpeed = 1.01f * maxSpeed / _movementRadius;
                gameObject.GetComponent<Transform>().position = GetDesiredPosition();
                Coins = bData.coins;
                Loot = bData.lootData;
                ProbabilityWeight = bData.probabilityWeight;
            }
        }

        protected override void MovementControl()
        {
            if (!rb.isKinematic)
            {
                orientation = (GetDesiredPosition() - (Vector2) gameObject.GetComponent<Transform>().position).normalized;
                accelerate = true;
            }
        }

        internal Vector2 GetDesiredPosition()
        {
            return new Vector2(
                _gameCenter.x + _movementRadius * Mathf.Cos(_frequencySpeed * _bossTime + _angleOffset),
                _gameCenter.y + _movementRadius * Mathf.Sin(_frequencySpeed * _bossTime + _angleOffset)
            );
        }
    }
}