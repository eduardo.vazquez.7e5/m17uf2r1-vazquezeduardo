using Scriptable_Objects.Weapons;
using UnityEngine;

namespace Characters
{
    public class BulletController : MonoBehaviour
    {
        private string _ownerTag;
        private float _damage;
        private float _lifeTime = 3f;
        private float _currentLifeTime;

        void Update()
        {
            _currentLifeTime += Time.deltaTime;
            if(_currentLifeTime >= _lifeTime)
                Destroy(this.gameObject);
        }
    
        public void LoadData(RangedWeapon wp, Vector2 direction, string ownerTag)
        {
            GetComponent<Rigidbody2D>().velocity = wp.bulletSpeed * direction;
            _ownerTag = ownerTag;
            tag = ownerTag;
            _damage = wp.damage;
            GetComponent<Rigidbody2D>().mass = wp.bulletWeight;
            GetComponent<SpriteRenderer>().sprite = wp.bullet;
            _lifeTime = wp.bulletLifeTime;
        }

        private void OnTriggerEnter2D(Collider2D col)
        {
            if (col.gameObject.CompareTag("Ground"))
                Destroy(this.gameObject);
            else if (col.gameObject.GetComponent<DestroyableCharacter>() && !col.gameObject.CompareTag(_ownerTag))
            {
                col.gameObject.GetComponent<DestroyableCharacter>().Hurt(_damage);
                Rigidbody2D rb = GetComponent<Rigidbody2D>();
                col.gameObject.GetComponent<ShipController>().InelasticCollision(rb.mass, rb.velocity * rb.mass);
                Destroy(this.gameObject);
            }
            else if(!col.gameObject.CompareTag(_ownerTag) && col.GetComponent<BulletController>() == null && col.GetComponent<LootController>() == null && col.GetComponent<OrbController>() == null)
                Destroy(this.gameObject);
        }
    }
}
