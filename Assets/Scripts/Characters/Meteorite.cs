using System;
using System.Collections.Generic;
using Scriptable_Objects.DataHolders;
using Scriptable_Objects.Loot;
using Scriptable_Objects.ShipData;
using Unity.Mathematics;
using UnityEngine;
using ListOfElements = Managers.ListOfElements;
using Random = UnityEngine.Random;

namespace Characters
{
    public class Meteorite : ShipController, ILootableCharacter
    {
        private float _maxSpawnSpeed = 2f;
        private GameObject _childrenSpawned;
        private int _numberOfSpawnedChildren;
        private float _minSize;
        private float _maxSize;
        private List<LootData> _loot;
        public int Coins { get; set; }
        List<LootData> ILootableCharacter.Loot { get => _loot; set => _loot = value; }
        private List<int> _probabilityWeight;
        List<int> ILootableCharacter.ProbabilityWeight { get => _probabilityWeight; set => _probabilityWeight = value; }

        protected override void Start()
        {
            _probabilityWeight = new List<int>();
            base.Start();
            float spawnSpeed = Random.Range(0f, _maxSpawnSpeed);
            Vector2 vel = new Vector2(Random.Range(0f, 1f), Random.Range(0f, 1f));
            if (vel != Vector2.zero)
                vel = vel.normalized * spawnSpeed;
            rb.velocity = vel;
            rb.angularVelocity = Random.Range(30f, 180f) * (Random.Range(0, 2)*2 -1);
            float size = Random.Range(_minSize, _maxSize);
            GetComponent<Transform>().localScale = new Vector3(size, size, 1);
            var list = ListOfElements.Instance;
            list.listOfMeteorites.Add(this.gameObject);
        }
        internal override void LoadData(DestroyableCharacterData sData)
        {
            base.LoadData(sData);
            if (sData is MeteoriteData meteoriteData)
            {
                _maxSpawnSpeed = meteoriteData.maxSpawnSpeed; 
                _childrenSpawned = meteoriteData.childrenSpawned;
                _numberOfSpawnedChildren = meteoriteData.numberOfSpawnedChildren;
                _minSize = meteoriteData.minSize;
                _maxSize = meteoriteData.maxSize;
                Coins = meteoriteData.coins;
                _loot = meteoriteData.lootData;
                _probabilityWeight = meteoriteData.probabilityWeight;
            }
        }

        protected override void OrientShip() { }

        protected override void OnDeathDo()
        {
            (this as ILootableCharacter)!.DropLoot(this.gameObject);
            if (_childrenSpawned != null)
            {
                for (int i = 0; i < _numberOfSpawnedChildren; i++)
                {
                    var copy = Instantiate(_childrenSpawned,
                        GetComponent<Transform>().position + new Vector3(Random.Range(0.3f, 1f), Random.Range(0.3f, 1f),0),
                        quaternion.identity);
                    copy.GetComponent<Meteorite>().LoadData(((MeteoriteData) dcData).childrenData);
                }
            }
            Instantiate(onDeathExplosion, GetComponent<Transform>().position, new Quaternion(0, 0, 0, 0));
            var list = ListOfElements.Instance;
            list.listOfMeteorites.Remove(this.gameObject);
            list.listOfShips.Remove(this.gameObject);
            list.listOfCharacters.Remove(this.gameObject);
            Destroy(this.gameObject);
        }
        
        public override void Hurt(float damage)
        {
            base.Hurt(damage);
            dazedTime = 0.1f;
        }
    }
}
