using System.Collections.Generic;
using Classes;
using JetBrains.Annotations;
using Managers;
using Scriptable_Objects;
using Scriptable_Objects.Weapons;
using UnityEngine;
using UnityEngine.Animations;

namespace Characters
{
    public enum TurretState
    {
        LoadingData,
        Aiming,
        Shooting
    }
    
    public class TurretController : MonoBehaviour, IMelleeAttacker
    {
        private MeleeWeapon _meleeWeapon;
        private float _maxRotation;
        private float _aimingTime;
        private float _aimingSpeed; // In degrees / second !!
        public TurretData turretData;
        private Vector3 _playerPosition;
        private float _startingRotation;
        private FiniteStateMachine<AttackState> _attackState;
        private List<GameObject> _weaponInstances;

        private FiniteStateMachine<TurretState> _turretState;

        void Start()
        {
            _turretState = new FiniteStateMachine<TurretState>(TurretState.LoadingData);
            SetUpTurretState();
            _turretState.Start();
            
            if(turretData != null)
                LoadData(turretData);
        }

        void Update()
        {
            (this as IMelleeAttacker).UpdateMelee();
            _turretState?.Update();
        }

        public void LoadData(TurretData tData)
        {
            _maxRotation = tData.maxRotation;
            _meleeWeapon = tData.meleeWeapon;
            _aimingSpeed = tData.aimingSpeed;
            _aimingTime = tData.aimingTime;
            turretData = tData;
            (this as IMelleeAttacker).LoadWeapon(_meleeWeapon, this.gameObject);
            _turretState.SetTrigger(TurretState.LoadingData, TurretState.Aiming);
            _startingRotation = GetComponent<Transform>().localEulerAngles.z;
            if (_startingRotation > 180)
                _startingRotation -= 360;
        }

        void SetUpTurretState()
        {
            float currentAimingTime = 0;
            float currentShootingTime = 0;
            
            _turretState.SetNode(
                TurretState.LoadingData,
                () => { },
                () => { }
                );
            
            _turretState.SetNode(
                TurretState.Aiming,
                () => currentAimingTime = 0,
                () =>
                {
                    currentAimingTime += Time.deltaTime;
                    Aim();
                },
                _ => currentAimingTime > _aimingTime,
                _ => TurretState.Shooting);
            
            _turretState.SetNode(
                TurretState.Shooting,
                () =>
                {
                    (this as IMelleeAttacker).MeleeAttack(this.gameObject);
                    currentShootingTime = 0;
                },
                () => currentShootingTime += Time.deltaTime,
                _ => currentShootingTime > _meleeWeapon.cooldownTime,
                _ => TurretState.Aiming);
        }

        void Aim()
        {
            if (ListOfElements.Instance.player != null)
                _playerPosition = ListOfElements.Instance.player.GetComponent<Transform>().position;
            float currentAngle = GetComponent<Transform>().eulerAngles.z;
            float currentLocalAngle = GetComponent<Transform>().localEulerAngles.z;
            Vector3 orientation = new Vector3(-Mathf.Sin(currentAngle * Mathf.Deg2Rad), Mathf.Cos(currentAngle * Mathf.Deg2Rad), 0);
            Vector3 dist = _playerPosition - GetComponent<Transform>().position;
            dist.z = 0;
            float crossZ = Vector3.Cross(orientation, dist).z;
            if (crossZ > 0) crossZ = 1;
            else crossZ = -1;
            currentLocalAngle += crossZ * _aimingSpeed * Time.deltaTime;
            if (currentLocalAngle > 180) currentLocalAngle -= 360;
            currentLocalAngle = Mathf.Clamp(currentLocalAngle, _startingRotation-_maxRotation, _startingRotation+ _maxRotation);
            GetComponent<Transform>().localEulerAngles = new Vector3(0, 0, currentLocalAngle);
        }
        
        MeleeWeapon IMelleeAttacker.MeleeWeapon
        {
            get => _meleeWeapon;
            set => _meleeWeapon = value;
        }

        FiniteStateMachine<AttackState> IMelleeAttacker.AttackState
        {
            get => _attackState;
            set => _attackState = value;
        }

        List<GameObject> IMelleeAttacker.WeaponInstances
        {
            get => _weaponInstances;
            set => _weaponInstances = value;
        }
    }
}