using System.Collections.Generic;
using Scriptable_Objects.Loot;
using Scriptable_Objects.ShipData;

namespace Characters
{
    public class Shooter : ShootingController,ILootableCharacter
    {
        private List<LootData> _loot;
        public int Coins { get; set; }
        public List<LootData> Loot { get => _loot; set => _loot = value; }
        private List<int> _probabilityWeight;
        public List<int> ProbabilityWeight { get => _probabilityWeight; set => _probabilityWeight = value; }

        internal override void LoadData(DestroyableCharacterData shooterData)
        {
            base.LoadData(shooterData);
            _probabilityWeight = new List<int>();
            if (shooterData is EnemyShooterData esData)
            {
                Coins = esData.coins;
                _loot = esData.lootData;
                _probabilityWeight = esData.probabilityWeight;
            }
        }
    }
}
