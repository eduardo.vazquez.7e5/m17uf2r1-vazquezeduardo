using System.Collections.Generic;
using JetBrains.Annotations;
using Scriptable_Objects;
using Scriptable_Objects.DataHolders;
using UnityEngine;
using ListOfElements = Managers.ListOfElements;

namespace Characters
{
    public class PlanetController : MonoBehaviour
    {
        [SerializeField] [CanBeNull] 
        private Planet planet;
        [SerializeField] 
        private GameObject lightPrefab;
    
        public float gravity;
        public float power;
        public float minDistance;
        internal float size;
        internal float luminosity;
        private Color _lightColor;
        private int _lightSortingLayer;
        private float _lightScale;

        private GameObject _lightSpawned;

        protected virtual void  Start()
        {
            if(planet != null)
                LoadData(planet);
        }

        internal void Destroy()
        {
            var list = ListOfElements.Instance;
            list.RemovePlanet(this.gameObject);
            Destroy(this.gameObject);
            // Destroy(_lightSpawned);
        }

        internal virtual void LoadData(Planet pl)
        {
            gravity = pl.gravity;
            power = pl.power;
            size = pl.size;
            luminosity = pl.luminosity;
            _lightColor = pl.lightColor;
            _lightSortingLayer = pl.lightSortingLayer;
            _lightScale = pl.lightScale;
            minDistance = pl.minDistance;
            GetComponent<Animator>().runtimeAnimatorController = pl.animator;
            if (pl.size < 0.001)
                GetComponent<CircleCollider2D>().enabled = false;
            else
                GetComponent<CircleCollider2D>().radius = pl.size;
            gameObject.tag = "Ground";       
            _lightSpawned = Instantiate(lightPrefab, GetComponent<Transform>());
            _lightSpawned.GetComponent<SpriteRenderer>().color = _lightColor;
            _lightSpawned.GetComponent<SpriteRenderer>().sortingOrder = _lightSortingLayer;
            _lightSpawned.GetComponent<Transform>().localScale = new Vector3(_lightScale, _lightScale, 1);
        
            var list = ListOfElements.Instance;
            list.AddPlanet(this.gameObject);
        }
    }
}
