using Managers;
using Scriptable_Objects;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Characters
{
    public class BlackHoleScript : PlanetController
    {
        private bool _hasBeenLoaded;
        private float _gravityIncreaseSpeed = 0.5f;
        
        protected override void Start()
        {
            _hasBeenLoaded = false;
            base.Start();
        }
        
        internal override void LoadData(Planet pl)
        {
            base.LoadData(pl);
            _gravityIncreaseSpeed = Random.Range(0.2f, 0.5f);
            _hasBeenLoaded = true;
        }

        private void Update()
        {
            if (_hasBeenLoaded)
                gravity += _gravityIncreaseSpeed * Time.deltaTime;
        }

        private void OnTriggerEnter2D(Collider2D col)
        {
            if (col.CompareTag("Player") && col.gameObject.GetComponent<Player>() != null)
                GameManager.Instance.WinGame();
            else if(col.gameObject.GetComponent<DestroyableCharacter>() != null)
                col.gameObject.GetComponent<DestroyableCharacter>().InstaKill();
            Destroy(col.gameObject);
        }
    }
}
