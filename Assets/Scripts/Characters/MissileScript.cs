using System;
using Scriptable_Objects.Weapons;
using UnityEngine;

namespace Characters
{
    public class MissileScript : ShipController
    {
        [SerializeField] 
        internal RangedWeapon rangedWeapon;
        internal string ownerTag;
        private float _damage;
        private float _lifeTime = 3f;
        private float _currentLifeTime;
        private float _constantPush = 15f;
        internal GameObject objective;

        protected override void Start()
        {
            base.Start();
            gravityScale = 0;
        }

        protected override void Update()
        {
            base.Update();
            _currentLifeTime += Time.deltaTime;
            if (_currentLifeTime >= _lifeTime)
            {
                OnDeathDo();
                Destroy(this.gameObject);
            }
        }
        
        public void LoadData(RangedWeapon wp, Vector2 direction, string ownerTag)
        {
            GetComponent<Rigidbody2D>().velocity = wp.bulletSpeed * direction;
            this.ownerTag = ownerTag;
            tag = ownerTag;
            _damage = wp.damage;
            GetComponent<Rigidbody2D>().mass = wp.bulletWeight;
            GetComponent<SpriteRenderer>().sprite = wp.bullet;
            _lifeTime = wp.bulletLifeTime;
            if (wp is MissileWeapon mWeapon)
                _constantPush = mWeapon.constantPush;
        }

        protected override void OrientShip()
        {
            Vector2 or = rb.velocity.normalized;
            double zAngle = 0;
            if (or != Vector2.zero)
            {
                if (or.y > 0)
                    zAngle = 180 / Math.PI * Math.Asin(-or.x);
                else if (or.y < 0)
                    zAngle = 180 - 180 / Math.PI * Math.Asin(-or.x);
                else if (or.x > 0)
                    zAngle = 0;
                else
                    zAngle = 180;
                rb.transform.eulerAngles = new Vector3(0, 0, (float)zAngle);
            }
        }

        private void OnTriggerEnter2D(Collider2D col)
        {
            if (col.gameObject.CompareTag("Ground"))
                Destroy(this.gameObject);
            else if (col.gameObject.GetComponent<DestroyableCharacter>() && !col.gameObject.CompareTag(ownerTag))
            {
                GameObject enemy = col.gameObject;
                enemy.GetComponent<DestroyableCharacter>().Hurt(_damage);
                enemy.GetComponent<Rigidbody2D>().velocity = Vector2.zero;
                Vector3 vecDif = enemy.GetComponent<Transform>().position - GetComponent<Transform>().position;
                enemy.GetComponent<ShipController>().InelasticCollision(rb.mass, _constantPush * new Vector2(vecDif.x, vecDif.y).normalized);
            
                Instantiate(onDeathExplosion, GetComponent<Transform>().position, new Quaternion(0, 0, 0, 0));
                OnDeathDo();
                Destroy(this.gameObject);
            }
            else if (!col.gameObject.CompareTag(ownerTag))
            {
                OnDeathDo();
                Destroy(this.gameObject);
            }
        }
    }
}
