using System;
using System.Collections.Generic;
using System.Linq;
using Classes;
using Managers;
using Scriptable_Objects.DataHolders;
using Scriptable_Objects.Skills;
using Scriptable_Objects.SkillUserAI;
using Scriptable_Objects.Weapons;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.Serialization;
using PlayerInput = Scriptable_Objects.ShootingAI.PlayerInput;
using Random = UnityEngine.Random;

public enum DashState //I'm not using this anymore but the joke is too good to be erased :(
{
    Available,
    Dashing, // through the snow
    Cooldown
}

namespace Characters
{
    public class Player : ShootingController, ISkillUser, IMelleeAttacker
    {
        internal Inventory inventory;
        internal int weaponIndex;
        internal int missileUpgrades;
        [SerializeField]
        private GameObject missileObject;
        private float _currentEnergy;
        float ISkillUser.CurrentEnergy { get => _currentEnergy; set => _currentEnergy = value; }
        [SerializeField]
        private float maxEnergy;
        float ISkillUser.MaxEnergy { get => maxEnergy; set => maxEnergy = value; }
        [SerializeField]
        private float timeToRecharge;
        float ISkillUser.TimeToRecharge { get => timeToRecharge; set => timeToRecharge = value; }
        private float _currentTimeToRecharge;
        float ISkillUser.CurrentTimeToRecharge { get => _currentTimeToRecharge; set => _currentTimeToRecharge = value; }
        [SerializeField]
        private float energyRecharged;
        float ISkillUser.EnergyRecharged { get => energyRecharged; set => energyRecharged = value; }
        private bool _useSkill;
        bool ISkillUser.UseSkill { get => _useSkill; set => _useSkill = value; }
        internal Skill currentSkill;
        Skill ISkillUser.CurrentSkill { get => currentSkill; set => currentSkill = value; }
        [SerializeField]
        private SkillUserAI skillUserAI;
        SkillUserAI ISkillUser.SkillUserAI { get => skillUserAI; set => skillUserAI = value; }
        private Vector2 _skillDirection;
        Vector2 ISkillUser.SkillDirection { get => _skillDirection; set => _skillDirection = value; }
        private float _currentAfterImageCooldown;
        float ISkillUser.CurrentAfterImageCooldown { get => _currentAfterImageCooldown; set => _currentAfterImageCooldown = value; }
        private GameObject _placeHolderForSkills;
        GameObject ISkillUser.PlaceHolderForSkills { get => _placeHolderForSkills; set => _placeHolderForSkills = value; }
        private FiniteStateMachine<SkillState> _skillState;
        FiniteStateMachine<SkillState> ISkillUser.SkillState { get => _skillState; set => _skillState = value; }
        private bool _hasFinishedSettingUp;
        private MeleeWeapon _meleeWeapon;
        MeleeWeapon IMelleeAttacker.MeleeWeapon { get => _meleeWeapon; set => _meleeWeapon = value; }
        private List<GameObject> _weaponInstances;
        List<GameObject> IMelleeAttacker.WeaponInstances { get => _weaponInstances; set => _weaponInstances = value; }
        private FiniteStateMachine<AttackState> _attackState;
        FiniteStateMachine<AttackState> IMelleeAttacker.AttackState { get => _attackState; set => _attackState = value; }
        
        bool ISkillUser.HasFinishedSettingUp { get => _hasFinishedSettingUp; set => _hasFinishedSettingUp = value; }

        protected override void Start()
        {
            base.Start();
            GetComponent<ISkillUser>().StartSkillUser(gameObject);
            inventory = Resources.Load<Inventory>("ScriptableObjectsInstances/Inventory");

            var pData = Resources.Load<PlayerData>("ScriptableObjectsInstances/PlayerData");
            maxHull = pData.maxHealth;
            currentHull = pData.currentHealth;
            maxEnergy = pData.maxEnergy;
            missileUpgrades = pData.missileUpgrades;
            if (inventory.weapons.Any())
            {
                weaponIndex = 0;
                LoadWeaponFromInventory();
            }

            if (inventory.skill != null)
                LoadSkillFromInventory();

            EventManager.OnPlayerHasShot += HandleAutoMissileLauncher;

        }
        protected override void Update()
        {
            base.Update();
            if (_hasFinishedSettingUp)
                (this as ISkillUser).UpdateSkillUser(gameObject);
            (this as IMelleeAttacker).UpdateMelee();
        }

        protected void OnDestroy()
        {
            EventManager.OnPlayerHasShot -= HandleAutoMissileLauncher;
        }

        private void WeaponChange(int n)
        {
            if (n != 0 && inventory.weapons.Count > 1 && GameManager.Instance.IsGameActive)
            {
                if (inventory.weapons[weaponIndex] is Firearm && IsMagazineUsed())
                    inventory.ammunition[weaponIndex]--;
                if(inventory.weapons[weaponIndex] is MeleeWeapon )
                    (this as IMelleeAttacker).End(this.gameObject);
                weaponIndex = (weaponIndex + n) % inventory.weapons.Count;
                if (weaponIndex < 0)
                    weaponIndex += inventory.weapons.Count;
                LoadWeaponFromInventory();
            }
        }
        
        public void WeaponChange(InputAction.CallbackContext context)
        {
            if (context.performed)
            {
                if(context.ReadValue<Vector2>().y > 0) WeaponChange(1);
                if(context.ReadValue<Vector2>().y < 0) WeaponChange(-1);
            }
        }
        
        public void WeaponUp(InputAction.CallbackContext context)
        {
            if (context.performed)
                WeaponChange(1);
        }
        
        public void WeaponDown(InputAction.CallbackContext context)
        {
            if (context.performed)
                WeaponChange(-1);
        }
        
        internal void LoadWeaponFromInventory()
        {
            EventManager.CallOnPlayerWeaponChange();
            if (inventory.weapons[weaponIndex] is RangedWeapon)
                LoadRangedWeaponData((RangedWeapon)inventory.weapons[weaponIndex]);
            if(inventory.weapons[weaponIndex] is MeleeWeapon)
                (this as IMelleeAttacker).LoadWeapon(inventory.weapons[weaponIndex] as MeleeWeapon, this.gameObject);
        }

        internal void LoadSkillFromInventory()
        {
            EventManager.CallOnPlayerSkillChange();
            currentSkill = inventory.skill;
            GetComponent<ISkillUser>().LoadSkill(currentSkill, this.gameObject);
        }
        
        protected override bool HasAmmunition()
        {
            return !(weapon is Firearm) || inventory.GetAmmunition(weapon) >= 1;
        }
        
        protected override void OnMagazineReload()
        {
            if(weapon is Firearm)
                inventory.ammunition[weaponIndex]--;
        }

        public void OnEneryRecharge()
        {
            EventManager.CallOnPlayerEnergyChange();
        }
        public override void Hurt(float damage)
        {
            base.Hurt(damage);
            EventManager.CallOnPlayerHealthChange();
        }

        public override void Heal(float heal)
        {
            base.Heal(heal);
            EventManager.CallOnPlayerHealthChange();
        }

        internal override void InstaKill()
        {
            base.InstaKill();
            EventManager.CallOnPlayerHealthChange();
        }

        public void Fire(InputAction.CallbackContext context)
        {
            if(inventory.weapons[weaponIndex] is RangedWeapon && shootingAI is PlayerInput playerShoot)
                playerShoot.Fire(context);
            if(inventory.weapons[weaponIndex] is MeleeWeapon)
                (this as IMelleeAttacker).MeleeAttack(this.gameObject);
        }

        public void AutoFire(InputAction.CallbackContext context)
        {
            if(inventory.weapons[weaponIndex] is RangedWeapon && shootingAI is PlayerInput playerShoot)
                playerShoot.AutoFire(context);
            if(inventory.weapons[weaponIndex] is MeleeWeapon)
                (this as IMelleeAttacker).MeleeAttack(this.gameObject);
        }

        protected override bool HasEnergyToShoot()
        {
            var energyWeapon = weapon as EnergyWeapon;
            if(energyWeapon is not null)
                return GetComponent<ISkillUser>().CurrentEnergy >= energyWeapon.energyCostByBurst;
            return true;
        }

        protected override void OnBurstStart()
        {
            base.OnBurstStart();
            EventManager.CallOnPlayerHasShot();
            var energyWeapon = weapon as EnergyWeapon;
            if(energyWeapon is not null)
            {
                GetComponent<ISkillUser>().CurrentEnergy -= energyWeapon.energyCostByBurst;
                EventManager.CallOnPlayerEnergyChange();
            }
        }

        protected internal void HandleAutoMissileLauncher()
        {
            int rand = Random.Range(0, 10);
            if (rand < missileUpgrades)
            {
                var rand2 = Random.Range(0, 360);
                var clone = Instantiate(missileObject, GetComponent<Transform>().position, Quaternion.identity);
                var velocity = Deviate(rand2, shootingDirection);
                clone.GetComponent<MissileScript>().LoadData(weapon, velocity, gameObject.tag);
            }
        }

        protected override void OrientShip()
        {
            if(Time.timeScale > 0.001f)
                base.OrientShip();
        }

        protected override void OnDeathDo()
        {
            base.OnDeathDo();
            GameManager.Instance.LoseGame();
        }
        
        public void Pause(InputAction.CallbackContext context)
        {
            if (context.performed)
                UIManager.Instance.Pause();
        }
    }
}
