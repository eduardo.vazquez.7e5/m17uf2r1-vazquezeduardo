using System.Collections.Generic;
using Scriptable_Objects.Loot;
using Scriptable_Objects.ShipData;
using UnityEngine;
using UnityEngine.Serialization;

namespace Characters
{
    public class Exploder : ShipController,ILootableCharacter
    {
        private static readonly int Explode = Animator.StringToHash("Explode");

        private List<LootData> _loot;
        public int Coins { get; set; }
        public List<LootData> Loot { get => _loot; set => _loot = value; }
        private List<int> _probabilityWeight;
        public List<int> ProbabilityWeight { get => _probabilityWeight; set => _probabilityWeight = value; }

        private float _explosionDamage = 30f;
        private float _constantPush = 15f;

        internal override void LoadData(DestroyableCharacterData exploderData)
        {
            base.LoadData(exploderData);
            _probabilityWeight = new List<int>();
            if (exploderData is EnemyExploderData esData)
            {
                Coins = esData.coins;
                _loot = esData.lootData;
                _probabilityWeight = esData.probabilityWeight;
            }
        }
        
        protected override void OnCollisionWithEnemy(GameObject enemy)
        {
            enemy.GetComponent<DestroyableCharacter>().Hurt(_explosionDamage);
            enemy.GetComponent<Rigidbody2D>().velocity = Vector2.zero;
            Vector3 vecDif = enemy.GetComponent<Transform>().position - GetComponent<Transform>().position;
            enemy.GetComponent<ShipController>().InelasticCollision(rb.mass, _constantPush * new Vector2(vecDif.x, vecDif.y).normalized);
            
            Instantiate(onDeathExplosion, GetComponent<Transform>().position, new Quaternion(0, 0, 0, 0));
            OnDeathDo();
            Destroy(this.gameObject);
        }
    }
}
