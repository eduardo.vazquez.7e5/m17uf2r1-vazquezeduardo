using System;
using Characters;
using Scriptable_Objects;
using Scriptable_Objects.DataHolders;
using Scriptable_Objects.Loot;
using UnityEngine;
using UnityEngine.Serialization;

public class LootController : MonoBehaviour
{
    [FormerlySerializedAs("_lootData")] [SerializeField]
    private LootData lootData;
    private Sprite _sprite;
    private float _timeToDespawn = 5f;
    private float _currentTimeToDespawn;
    private float _blinkingTime = 0.1f;
    private float _currentBlinkingTime;
    private float _shiningTime = 0.3f;
    private float _alpha;
    void Start()
    {
        if (lootData != null)
            LoadLootData(lootData);
        _currentTimeToDespawn = 0f;
        _currentBlinkingTime = 0f;
        _alpha = 1f;
    }

    internal void LoadLootData(LootData lData)
    {
        lootData = lData;
        GetComponent<SpriteRenderer>().sprite = lData.sprite;
        _timeToDespawn = lData.timeToDespawn;
    }
    
    void Update()
    {
        _currentTimeToDespawn += Time.deltaTime;
        if (_currentTimeToDespawn >= _timeToDespawn)
            Destroy(this.gameObject);
        float blueProp = GetBlueProportion();
        if (_currentTimeToDespawn > 0.8 * _timeToDespawn)
        {
            _currentBlinkingTime += Time.deltaTime;
            if (_currentBlinkingTime >= _blinkingTime)
            {
                _currentBlinkingTime -= _blinkingTime;
                _alpha = 1f - _alpha;
            }
        }
        GetComponent<SpriteRenderer>().color = new Color(1f, 1f, GetBlueProportion(), _alpha);
    }

    float GetBlueProportion()
    {
        float prop = (_currentTimeToDespawn % (2f * _shiningTime)) / _shiningTime;
        if (prop > 1f) prop = 2 - prop;
        return prop;
    }

    private void OnTriggerEnter2D(Collider2D col)
    {
        if (col.gameObject.CompareTag("Player") && col.gameObject.GetComponent<Player>() != null)
        {
            if (lootData is WeaponLootData weaponLootData)
            {
                var inventory = Resources.Load<Inventory>("ScriptableObjectsInstances/Inventory");
                inventory.AddWeapon(weaponLootData.weapon, col.gameObject);
            }
            if(lootData is HealthKitLootData healthData)
                col.GetComponent<Player>().Heal(healthData.health);
            if (lootData is SkillLootData skillData)
            {
                
                var inventory = Resources.Load<Inventory>("ScriptableObjectsInstances/Inventory");
                inventory.skill = skillData.skill;
                if(col.gameObject.GetComponent<ISkillUser>().HasFinishedSettingUp)
                    col.gameObject.GetComponent<ISkillUser>().SkillState.SetTrigger(SkillState.InUse, SkillState.Cooldown);
                col.gameObject.GetComponent<Player>().LoadSkillFromInventory();
            }
            Destroy(this.gameObject);
                
        }
    }
}
