using Characters;
using Classes;
using Scriptable_Objects.ShipData;
using Scriptable_Objects.ShootingAI;
using Scriptable_Objects.Weapons;
using UnityEngine;

public enum ShootingState
{
    Available,
    BetweenBullets,
    Burst,
    BetweenBursts,
    Reloading
}

public class ShootingController : ShipController
{
    internal RangedWeapon weapon;
    private GameObject _weaponDisplayer;
    protected ShootingAI shootingAI;

    public bool shoot;
    
    private float _timeBetweenBullets;
    internal int bulletsByBurst;
    private float _timeBetweenBursts;
    internal int burstByMagazine;
    private float _timeOfReload;

    protected float _accuracy;
    
    private int _currentBulletOfBurst;
    private int _currentBurstOfMagazine;
    public int CurrentBulletsByMagazine => (_currentBurstOfMagazine - 1) * bulletsByBurst + _currentBulletOfBurst;

    internal Vector2 shootingDirection;
   

    public int BulletsByMagazine => bulletsByBurst * burstByMagazine;

    private FiniteStateMachine<ShootingState> _shootingState;

    protected override void Start()
    {
        base.Start();
        shoot = false;
    }

    protected override void Update()
    {
        base.Update();
        if(shipState != null)
            shoot = shipState.GetCurrentState()!= ShipState.Dead && shootingAI.Calculate(this.gameObject);
        if(_shootingState != null)
            _shootingState.Update();
    }

    internal override void LoadData(DestroyableCharacterData shooterData)
    {
        base.LoadData(shooterData);
        if (shooterData is ShooterData sData)
        {
            shootingAI = sData.shootingAi;
            if(sData.weapon != null){}
                LoadRangedWeaponData(sData.weapon);
        }
    }

    protected void LoadRangedWeaponData(RangedWeapon wp)
    {
        weapon = wp;
        _timeBetweenBullets = weapon.timeBetweenBullets;
        bulletsByBurst = weapon.bulletsByBurst;
        _timeBetweenBursts = weapon.timeBetweenBursts;
        burstByMagazine = weapon.burstByMagazine;
        _timeOfReload = weapon.timeOfReload;
        _weaponDisplayer = weapon.bulletprefab;
        _accuracy = weapon.accuracy;
        GetComponent<AudioSource>().clip = weapon.attackingSound;
        _shootingState = new FiniteStateMachine<ShootingState>(ShootingState.Reloading);
        SetUpShootingState();
        _shootingState.Start();
    }
    
    protected virtual void FireBullet()
    {
        var rand = Random.Range(-_accuracy, _accuracy);
        var velocity = Deviate(rand, shootingDirection);
        var clone = Instantiate(_weaponDisplayer, GetComponent<Transform>().position, Quaternion.FromToRotation(new Vector3(1,0,0), velocity));
        if(clone.GetComponent<BulletController>() != null)
            clone.GetComponent<BulletController>().LoadData(weapon, velocity, gameObject.tag);
        else if(clone.GetComponent<MissileScript>() != null)
            clone.GetComponent<MissileScript>().LoadData(weapon, velocity, gameObject.tag);
        //InelasticCollision(-weapon.bulletWeight, -shootingDirection*weapon.bulletSpeed * weapon.bulletWeight);
        GetComponent<AudioSource>().Play();
    }

    protected Vector2 Deviate(float rand, Vector2 vector)
    {
        return new Vector2(Mathf.Cos(rand*Mathf.PI/180) * vector.x - Mathf.Sin(rand*Mathf.PI/180) * vector.y,
                           Mathf.Sin(rand*Mathf.PI/180) * vector.x + Mathf.Cos(rand*Mathf.PI/180) * vector.y);
    }

    private ShootingState BulletControl()
    {
        _currentBulletOfBurst--;
        if (_currentBulletOfBurst <= 0)
        {
            _currentBulletOfBurst = bulletsByBurst;
            _currentBurstOfMagazine--;
            if (_currentBurstOfMagazine <= 0)
            {
                OnMagazineReload();
                return ShootingState.Reloading;
            }
            return ShootingState.BetweenBursts;
        }
        return ShootingState.BetweenBullets;
    }

    private void SetUpShootingState()
    {
        float currentTimeBetweenBullets = 0f;
        float currentTimeBetweenBursts = 0f;
        float currentTimeOfReload = 0f;
        
        _currentBulletOfBurst = 0;
        _currentBurstOfMagazine = 0; 
        
        _shootingState.SetNode(
            ShootingState.Available,
            () => { },
            () => { },
            _ => shoot && HasEnergyToShoot(),
            _ =>
            {
                OnBurstStart();
                FireBullet();
                return BulletControl();
            },
            _ => !HasAmmunition(), 
            _ => ShootingState.Reloading
        );
        _shootingState.SetNode(
            ShootingState.Burst,
            FireBullet,
            () => { },
            _ => true,
            _ => BulletControl()
        );
        _shootingState.SetNode(
            ShootingState.BetweenBullets,
            () => currentTimeBetweenBullets = 0f,
            () => currentTimeBetweenBullets+=Time.deltaTime,
            _ => currentTimeBetweenBullets >= _timeBetweenBullets,
            _ => ShootingState.Burst
        );
        _shootingState.SetNode(
            ShootingState.BetweenBursts,
            () => currentTimeBetweenBursts = 0f,
            () => currentTimeBetweenBursts += Time.deltaTime,
            _ => currentTimeBetweenBursts >= _timeBetweenBursts,
            _ => ShootingState.Available
        );
        _shootingState.SetNode(
            ShootingState.Reloading,
            () => currentTimeOfReload = 0f,
            () => currentTimeOfReload += Time.deltaTime,
            _ => currentTimeOfReload >= _timeOfReload && HasAmmunition(),
            _ =>
            {
                _currentBulletOfBurst = bulletsByBurst;
                _currentBurstOfMagazine = burstByMagazine; 
                return ShootingState.Available;
            });

    }

    protected virtual bool HasEnergyToShoot() { return true; }
    protected virtual void OnBurstStart() { }
    protected virtual bool HasAmmunition() { return true; }
    protected virtual void OnMagazineReload() { }

    protected bool IsMagazineUsed()
    {
        if (_currentBulletOfBurst == bulletsByBurst && _currentBurstOfMagazine == burstByMagazine) return false;
        if (_shootingState.GetCurrentState() == ShootingState.Reloading) return false;
        return true;
    }

}
