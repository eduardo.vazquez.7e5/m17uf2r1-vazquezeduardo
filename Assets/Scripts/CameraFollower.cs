using UnityEngine;
using ListOfElements = Managers.ListOfElements;

public class CameraFollower : MonoBehaviour
{
    private Transform _tr;
    void Start()
    {
        _tr = GetComponent<Transform>();
        FollowPlayer();
    }

    // Update is called once per frame
    void Update()
    {
        FollowPlayer();
    }

    void FollowPlayer()
    {
        var list = ListOfElements.Instance;
        if (list.player != null)
        {
            GameObject panel = GameObject.Find("UIPanel");
            float panelWidth = 0f;
            if (panel != null)
            {
                Vector3[] panelCorners = new Vector3[4];
                panel.GetComponent<RectTransform>().GetWorldCorners(panelCorners);
                Vector3 topleft = Camera.main!.ScreenToWorldPoint(panelCorners[1]);
                Vector3 topright = Camera.main.ScreenToWorldPoint(panelCorners[2]);
                panelWidth = topright.x - topleft.x;
            }
            Vector2 pos = list.player.GetComponent<Transform>().position;
            _tr.position = new Vector3(pos.x+panelWidth/2, pos.y, _tr.position.z);
        }
    }
}
