using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Audio;

public class SoundMenuController : MonoBehaviour
{
    [SerializeField] public GameObject soundSlider;
    [SerializeField] public GameObject musicSlider;
    [SerializeField] public AudioMixer soundMixer;
    [SerializeField] public AudioMixer musicMixer;

    private void Start()
    {
        ResetSliders();
    }

    void OnSliderChange(GameObject slider, AudioMixer mixer)
    {
        float val = slider.GetComponent<Slider>().value;
        SetMixerValue(val, mixer);
        PlayerPrefs.SetFloat(slider.name, slider.GetComponent<Slider>().value);
    }

    public static void SetMixerValue(float sliderValue, AudioMixer mixer)
    {
        float valIndB = -80 + 40 * Mathf.Pow(2, sliderValue);
        if (sliderValue < 0.01)
            valIndB = -120;
        mixer.SetFloat("Volume", valIndB);
    }

    public void OnSoundVolumeChange()
    {
        OnSliderChange(soundSlider, soundMixer);
    }

    public void OnMusicVolumeChange()
    {
        OnSliderChange(musicSlider, musicMixer);
    }

    public void HideSoundMenu()
    {
        this.gameObject.SetActive(false);
    }

    public void ResetSliders()
    {
        soundSlider.GetComponent<Slider>().value = PlayerPrefs.GetFloat("SoundSlider", 1);
        musicSlider.GetComponent<Slider>().value = PlayerPrefs.GetFloat("MusicSlider", 1);
    }
}
