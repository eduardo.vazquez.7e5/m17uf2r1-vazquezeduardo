using System.Collections.Generic;
using System.Linq;
using Characters;
using Managers;
using Scriptable_Objects.DataHolders;
using Scriptable_Objects.SceneData;
using Scriptable_Objects.ShipData;
using Unity.Mathematics;
using UnityEngine;
using ListOfElements = Managers.ListOfElements;
using Object = UnityEngine.Object;
using Random = UnityEngine.Random;

namespace SceneController
{
    public class WaveController : SceneController
    {
        private float _minRadius;
        private float _maxRadius;
        private int _maxValue;
        protected List<ScriptableObject> _listOfEnemies;
        protected List<GameObject> _listOfPrefabs;
        protected List<uint> _listOfQuantities;
        protected List<uint> listOfValues;
        private List<GameObject> _listOfMeteorites;
        private List<MeteoriteData> _listOfMeteoritesDatas;
        private float _timeBetweenMeteorites;

        private int _currentValue;
        private bool _hasFinishedOrdering;
        private float _currentTimeBetweenMeteorites;
        private List<KeyValuePair<GameObject, uint>> _listOfSpawnedShips;
        private Transform _tr;

        internal override void LoadData(SceneData sceneData)
        {
            _hasFinishedOrdering = false;
            _tr = GetComponent<Transform>();
            _listOfSpawnedShips = new List<KeyValuePair<GameObject, uint>>();
            _currentValue = 0;
            EventManager.OnCharacterDestroyed += RecountValue;
            _currentTimeBetweenMeteorites = 0f;
            base.LoadData(sceneData);
            if (sceneData is WaveData wvData)
            {
                _minRadius = wvData.minRadius;
                _maxRadius = wvData.maxRadius;
                _maxValue = wvData.maxValue;
                _listOfEnemies = new List<ScriptableObject>();
                foreach (var enemy in wvData.listOfEnemies)
                    _listOfEnemies.Add(enemy);
                _listOfPrefabs = new List<GameObject>();
                foreach (var enemy in wvData.listOfPrefabs)
                    _listOfPrefabs.Add(enemy);
                _listOfQuantities = new List<uint>();
                foreach (var quantity in wvData.listOfQuantities)
                    _listOfQuantities.Add(quantity);
                listOfValues = new List<uint>();
                foreach (var value in wvData.listOfValues)
                    listOfValues.Add(value);
                _listOfMeteorites = new List<GameObject>();
                foreach (var meteo in wvData.listOfMeteorites)
                    _listOfMeteorites.Add(meteo);
                _listOfMeteoritesDatas = new List<MeteoriteData>();
                foreach (var data in wvData.listOfMeteoriteData)
                    _listOfMeteoritesDatas.Add(data);
                _timeBetweenMeteorites = wvData.timeBetweenMeteorites;

                WaveControllerInit();

                _hasFinishedOrdering = true;
            }
        }

        protected override void Update()
        {
            base.Update();
            if (_hasFinishedOrdering)
            {
                if (_listOfMeteorites.Count != 0)
                    HandleMeteorites();

                if (!IsWaveOver())
                {
                    while (!IsWaveOver() && _currentValue + (int) listOfValues[GetNextEnemy()] <= _maxValue)
                    {
                        SpawnNextEnemy();
                        CalculateNextEnemy();
                    }
                }
            }
        }

        internal override bool IsSceneOver()
        {
            return IsWaveOver() && _currentValue == 0;
        }

        protected void OnDestroy()
        {
            EventManager.OnCharacterDestroyed -= RecountValue;
        }

        protected virtual void WaveControllerInit() { }

        protected virtual bool IsWaveOver() { return true; }

        protected virtual int GetNextEnemy() { return 0; }

        protected virtual void CalculateNextEnemy() { }

        protected virtual void SpawnNextEnemy()
        {
            int index = GetNextEnemy();
            Vector3 position = SelectRandomPosition();
            var copy = Object.Instantiate(_listOfPrefabs[index], position, quaternion.identity);
            if (copy.GetComponent<ShipController>())
                copy.GetComponent<ShipController>().LoadData((ShipData) _listOfEnemies[index]);
            _currentValue += (int) listOfValues[index];
            _listOfSpawnedShips.Add(new KeyValuePair<GameObject, uint>(copy, listOfValues[index]));
        }

        private Vector3 SelectRandomPosition()
        {
            Vector3 position;
            do
            {
                float x = _tr.position.x + Random.Range(-_maxRadius, _maxRadius);
                float y = _tr.position.y + Random.Range(-_maxRadius, _maxRadius);
                float z = 0;
                position = new Vector3(x, y, z);
            } while (!IsValidPosition(position));

            return position;
        }

        private bool IsValidPosition(Vector3 position)
        {
            if (new Vector2(position.x - _tr.position.x, position.y - _tr.position.y).magnitude < _minRadius)
                return false;
            float width = Screen.width;
            float height = Screen.height;
            Vector2 positionOnScreen = Camera.main.WorldToScreenPoint(position);
            if (positionOnScreen.x >= 0 && positionOnScreen.x <= width &&
                positionOnScreen.y >= 0 && positionOnScreen.y <= height) return false;
            return true;
        }

        private void RecountValue()
        {
            var list = ListOfElements.Instance;
            for (int i = 0; i < _listOfSpawnedShips.Count; i++)
            {
                if (!list.listOfCharacters.Contains(_listOfSpawnedShips[i].Key))
                    _listOfSpawnedShips.RemoveAt(i--);
            }

            _currentValue = _listOfSpawnedShips.Select(pair => (int) pair.Value).Sum();
        }

        private void HandleMeteorites()
        {
            _currentTimeBetweenMeteorites += Time.deltaTime;
            if (_currentTimeBetweenMeteorites >= _timeBetweenMeteorites)
            {
                _currentTimeBetweenMeteorites -= _timeBetweenMeteorites;
                int x = Random.Range(0, _listOfMeteorites.Count);
                var copy = Object.Instantiate(_listOfMeteorites[x], SelectRandomPosition(), Quaternion.identity);
                copy.GetComponent<Meteorite>().LoadData(_listOfMeteoritesDatas[x]);
            }
        }
    }
}