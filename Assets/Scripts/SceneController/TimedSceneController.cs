using Scriptable_Objects.SceneData;
using UnityEngine;

namespace SceneController
{
    public class TimedSceneController : SceneController
    {
        private TimedSceneData _timedSceneData;
        private bool _hasFinishedLoading;
        private float _currentTime;

        internal override void LoadData(SceneData sData)
        {
            _hasFinishedLoading = false;
            base.LoadData(sData);
            if (sData is TimedSceneData tData)
            {
                _currentTime = 0;
                _timedSceneData = tData;
                _hasFinishedLoading = true;
            }
        }

        protected override void Update()
        {
            base.Update();
            _currentTime += Time.deltaTime;
        }

        internal override bool IsSceneOver()
        {
            return _timedSceneData != null && _hasFinishedLoading && _currentTime >= _timedSceneData.sceneDuration;
        }
    }
}