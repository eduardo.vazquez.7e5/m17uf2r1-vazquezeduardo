using Scriptable_Objects.SceneData;
using UnityEngine;

namespace SceneController
{
    public class SceneController : MonoBehaviour
    {
        [SerializeField] private SceneData sceneData;
        private bool _hasLoaded;

        internal virtual void LoadData(SceneData sData)
        {
            _hasLoaded = false;
            if (!_hasLoaded)
            {
                sceneData = sData;
                sData.Start();
                _hasLoaded = true;
            }
        }

        protected virtual void Update()
        {
            if (_hasLoaded)
            {
                sceneData.Update();
                if (IsSceneOver())
                {
                    sceneData.End();
                    Destroy(this.gameObject);
                }
            }
        }

        internal virtual bool IsSceneOver() { return false; }
    }
}