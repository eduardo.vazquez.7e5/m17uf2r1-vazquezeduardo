using System;
using System.Collections.Generic;
using System.Linq;
using Scriptable_Objects.SceneData;
using Random = UnityEngine.Random;

namespace SceneController
{
    public class ScriptedWaveController : WaveController
    {
        private List<int> _orderedEnemies;
        private int _nextEnemy;
        protected List<int> _listOfPrios;

        internal override void LoadData(SceneData sceneData)
        {
            if (sceneData is ScriptedWaveData wvData)
            {
                _listOfPrios = new List<int>();
                foreach (var prio in wvData.listOfPrios)
                    _listOfPrios.Add(prio);
            }
            base.LoadData(sceneData);
        }
        
        protected override bool IsWaveOver()
        {
            return _nextEnemy >= _orderedEnemies.Count;
        }

        protected override int GetNextEnemy()
        {
            return _orderedEnemies[_nextEnemy];
        }

        protected override void CalculateNextEnemy()
        {
            _nextEnemy++;
        }

        protected override void WaveControllerInit()
        {
            _orderedEnemies = new List<int>();
            _nextEnemy = 0;
            int maxPrio = _listOfPrios.Select(prio => prio)
                .DefaultIfEmpty(-1)
                .Max();
            List<int>[] classesOfEnemies = new List<int>[maxPrio + 2];
            for (int i = 0; i < maxPrio + 2; i++)
                classesOfEnemies[i] = new List<int>();

            int listSize = Math.Min(
                Math.Min(_listOfEnemies.Count, _listOfPrefabs.Count),
                Math.Min(_listOfQuantities.Count, listOfValues.Count));
            int priosIndex = Math.Min(listSize, _listOfPrios.Count);
            for (int i = 0; i < priosIndex; i++)
            {
                if (_listOfPrios[i] >= 0)
                    classesOfEnemies[i].Add(i);
                else
                    classesOfEnemies[maxPrio + 1].Add(i);
            }

            for (int i = priosIndex; i < listSize; i++)
                classesOfEnemies[maxPrio + 1].Add(i);

            for (int i = 0; i < maxPrio + 2; i++)
            {
                int numberOfEnemies = classesOfEnemies[i].Count;
                while (numberOfEnemies > 0)
                    SelectRandomEnemy(ref numberOfEnemies, classesOfEnemies[i]);
            }
        }

        private void SelectRandomEnemy(ref int numbOfEnemies, List<int> listOfEnemies)
        {
            int sum = listOfEnemies.Select(index => (int) _listOfQuantities[index]).Sum();
            int index = Random.Range(0, sum);
            int indexInList = 0;
            while (index >= _listOfQuantities[indexInList])
            {
                index -= (int) _listOfQuantities[indexInList];
                indexInList++;
            }

            _orderedEnemies.Add(indexInList);
            _listOfQuantities[indexInList]--;
            numbOfEnemies = sum - 1;
        }
    }
}