using System.Linq;
using Scriptable_Objects.SceneData;
using UnityEngine;
using Random = UnityEngine.Random;

namespace SceneController
{
    public class RandomWaveController : WaveController
    {
        private int _totalValueSpawned;
        private int _precalculatedNextEnemy;
        private int _totalValue;
        
        internal override void LoadData(SceneData sceneData)
        {
            if(sceneData is RandomWaveData wvData)
                _totalValue = wvData.totalValue;
            _totalValueSpawned = 0;
            base.LoadData(sceneData);
        }
        protected override bool IsWaveOver()
        {
            return _totalValueSpawned >= _totalValue;
        }

        protected override int GetNextEnemy()
        {
            return _precalculatedNextEnemy;
        }
        
        protected override void CalculateNextEnemy()
        {
            SelectRandomEnemy();
        }

        protected override void WaveControllerInit()
        {
            SelectRandomEnemy();
        }

        protected override void SpawnNextEnemy()
        {
            base.SpawnNextEnemy();
            _totalValueSpawned += (int) listOfValues[_precalculatedNextEnemy];
        }
        
        private void SelectRandomEnemy()
        {
            int sum = _listOfQuantities.Select(value => (int) value).Sum();
            int index = Random.Range(0, sum);
            int indexInList = 0;
            while (index >= _listOfQuantities[indexInList])
            {
                index -= (int) _listOfQuantities[indexInList];
                indexInList++;
            }
            _precalculatedNextEnemy = indexInList;
        }
    }
}
