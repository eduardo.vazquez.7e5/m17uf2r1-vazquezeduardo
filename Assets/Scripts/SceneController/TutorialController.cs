using Scriptable_Objects.SceneData;

namespace SceneController
{
    public class TutorialController : SceneController
    {
        private TutorialData _tutorialData;
        private bool _hasPlayerDoneTheThing;
        private bool _hasFinishedLoading;

        internal override void LoadData(SceneData sData)
        {
            _hasFinishedLoading = false;
            _hasPlayerDoneTheThing = false;
            base.LoadData(sData);
            if (sData is TutorialData tData)
            {
                _tutorialData = tData;
                _hasFinishedLoading = true;
            }
        }

        protected override void Update()
        {
            base.Update();
            if (_hasFinishedLoading && _tutorialData.ExpectedPlayerInput())
                _hasPlayerDoneTheThing = true;
        }

        internal override bool IsSceneOver()
        {
            return _hasPlayerDoneTheThing;
        }
    }
}