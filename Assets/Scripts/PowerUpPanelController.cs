using System;
using System.Collections.Generic;
using Managers;
using Scriptable_Objects.PowerUps;
using Scriptable_Objects.SceneData;
using UnityEngine;
using UnityEngine.UI;
using Random = UnityEngine.Random;

public class PowerUpPanelController : MonoBehaviour
{
    [SerializeField] private List<PowerUpSO> listOfPowerUps;
    [SerializeField] private GameObject powerUp1;
    [SerializeField] private GameObject powerUp2;
    [SerializeField] private GameObject powerUp3;
    [SerializeField] private List<int> chosenPowerUps;

    internal void RandomizePowerUps()
    {
        List<int> selectedPowerUps = new List<int>();
        do
        {
            int rand = 0;
            do
            {
                rand = Random.Range(0, listOfPowerUps.Count);
            } while (selectedPowerUps.Contains(rand));
            selectedPowerUps.Add(rand);
        } while (selectedPowerUps.Count < 3);
        chosenPowerUps = selectedPowerUps;
        AssignPowerUp(listOfPowerUps[selectedPowerUps[0]], powerUp1);
        AssignPowerUp(listOfPowerUps[selectedPowerUps[1]], powerUp2);
        AssignPowerUp(listOfPowerUps[selectedPowerUps[2]], powerUp3);
    }

    private void AssignPowerUp(PowerUpSO powerUpSo, GameObject go)
    {
        go.GetComponent<Transform>().Find("Icon").GetComponent<Image>().sprite = powerUpSo.icon;
        go.GetComponent<Transform>().Find("Description").GetComponent<Text>().text = powerUpSo.description;
    }
    void OnClickDo(int i)
    {
        listOfPowerUps[chosenPowerUps[i]].OnObtainDo();
        UIManager.Instance.TogglePowerUp();
        UIManager.Instance.ToggleShop();
    }

    public void FirstOnClickDo() { OnClickDo(0); }
    public void SecondOnClickDo() { OnClickDo(1); }
    public void ThirdOnClickDo() { OnClickDo(2); }
}
