using Managers;
using Scriptable_Objects.DataHolders;
using UnityEditor;
using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using ListOfElements = Managers.ListOfElements;

public enum MenuChoice
{
    None,
    Bangs,
    Glasses
}

public class MenuPanelScript : MonoBehaviour
{
    [SerializeField] private GameObject bangsImg;
    [SerializeField] private GameObject glassesImg;
    [SerializeField] private GameObject playerName;
    [SerializeField] private GameObject warning;
    [SerializeField] private GameObject soundPanel;
    [SerializeField] public AudioMixer soundMixer;
    [SerializeField] public AudioMixer musicMixer;

    private MenuChoice _menuChoice;

    private PlayerData _playerData;
    private Inventory _inventory;

    void Start()
    {
        AudioManager.Instance.Play("AmbientMusic", true);
        _menuChoice = MenuChoice.None;
        var _ = ListOfElements.Instance;
        _inventory = Resources.Load<Inventory>("ScriptableObjectsInstances/Inventory");
        EditorUtility.SetDirty(_inventory);
        _inventory.ResetValues();
        _playerData = Resources.Load<PlayerData>("ScriptableObjectsInstances/PlayerData");
        EditorUtility.SetDirty(_playerData);
        _playerData.ResetValues();
        UpdateImages();
        ResetMixers();

        PersistentDataSO initial = Resources.Load<PersistentDataSO>("ScriptableObjectsInstances/InitialPersistentData");
        PersistentDataSO persistentData = Resources.Load<PersistentDataSO>("ScriptableObjectsInstances/PersistentData");
        FileManager.IfNoDataThenCreateNewData("USR.json", initial.GetSerializedData());
        persistentData.LoadSerializedData();
        EditorUtility.SetDirty(persistentData);
    }

    void UpdateImages()
    {
        if (_menuChoice != MenuChoice.Bangs)
            bangsImg.GetComponent<Image>().color = new Color(1, 1, 1, 64f / 256f);
        else
            bangsImg.GetComponent<Image>().color = new Color(1, 1, 1, 1);
        
        if (_menuChoice != MenuChoice.Glasses)
            glassesImg.GetComponent<Image>().color = new Color(1, 1, 1, 64f / 256f);
        else
            glassesImg.GetComponent<Image>().color = new Color(1, 1, 1, 1);
        
        switch (_menuChoice)
        {
            case MenuChoice.Bangs:
                _playerData.gender = Gender.Bangs;
                break;
            case MenuChoice.Glasses:
                _playerData.gender = Gender.Glasses;
                break;
        }
        _playerData.LoadSprites();
    }

    private void Update()
    {
        _playerData.playerName = playerName.GetComponent<Text>().text;
        if (_playerData.playerName == "CHEATER")
            _inventory.LoadBrokenWeapon();
    }

    public void SelectBangs()
    {
        _menuChoice = MenuChoice.Bangs;
        UpdateImages();
    }
    
    public void SelectGlasses()
    {
        _menuChoice = MenuChoice.Glasses;
        UpdateImages();
    }

    public void StartGame()
    {
        if (_menuChoice != MenuChoice.None && playerName.GetComponent<Text>().text != "")
            SceneManager.LoadScene(1);
        else
            warning.SetActive(true);

    }

    public void OpenSoundMenu()
    {
        soundPanel.SetActive(true);
    }

    public void ResetMixers()
    {
        SoundMenuController.SetMixerValue(PlayerPrefs.GetFloat("SoundSlider", 1), soundMixer);
        SoundMenuController.SetMixerValue(PlayerPrefs.GetFloat("SoundSlider", 1), musicMixer);
    }
}
