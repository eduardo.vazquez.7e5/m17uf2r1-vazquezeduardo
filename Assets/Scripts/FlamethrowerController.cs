using System.Collections;
using System.Collections.Generic;
using Scriptable_Objects.Weapons;
using UnityEngine;

public class FlamethrowerController : MonoBehaviour
{
    
    private ParticleSystem _part;
    private List<ParticleCollisionEvent> _collisionEvents;
    public bool attack;
    public bool canAttack;
    public FlamethrowerWeapon weapon;

    void Start()
    {
        _part = GetComponent<ParticleSystem>();
        _collisionEvents = new List<ParticleCollisionEvent>();
        attack = false;
        canAttack = true;
    }

    void Update()
    {
        if (canAttack && attack)
        {
            canAttack = false;
            attack = false;
            this.GetComponent<ParticleSystem>().Play();
            StartCoroutine(Waiter());
        }
    }

    IEnumerator Waiter()
    {
        yield return new WaitForSeconds(weapon.cooldownTime);
        canAttack = true;
    }

    void OnParticleCollision(GameObject other)
    {
        int numCollisionEvents = _part.GetCollisionEvents(other, _collisionEvents);

        if (!other.CompareTag(this.gameObject.tag) && other.gameObject.GetComponent<DestroyableCharacter>())
        {
            var dc = other.gameObject.GetComponent<DestroyableCharacter>(); 
            dc.Hurt(weapon.damage * numCollisionEvents);
        }
    }
}