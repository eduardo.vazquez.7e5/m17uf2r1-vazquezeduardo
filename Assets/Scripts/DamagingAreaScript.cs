using UnityEngine;

public class DamagingAreaScript : MonoBehaviour
{
    [SerializeField] private float areaDamage = 10f;
    private void OnTriggerEnter2D(Collider2D col)
    {
        if (!CompareTag(col.tag) && col.gameObject.GetComponent<DestroyableCharacter>() != null)
        {
            col.gameObject.GetComponent<DestroyableCharacter>().Hurt(areaDamage);
        }
    }
}
