# Untitled Space Roguelike

<table style="
  border: 0px solid #a2a9b1;
  border-spacing: 0px;
background-color: #f8f9fa;
  color: black;
  margin: 0.5em 0 0.5em 1em;
  padding: 0em;
  clear: right;
  font-size: 88%;
  line-height: 1.5em;
  width: fit-content;
font-family: sans-serif;
  .infobox{
border-spacing: 3px;
color: black;
font-size: 88%;
line-height: 1.5em;
  }
  .infobox-image .infobox-caption{
text-align: center;
margin: auto;
}
">
<tbody>
<tr><th colspan="2" class="infobox-above fn" style="
  vertical-align: middle;"><img alt="The Moon" src="Assets/Sprites/Planets/Moon/1.png"/>Untitled Space Roguelike<img alt="The Moon" src="Assets/Sprites/Planets/Moon/1.png"/></th></tr>
<tr><th scope="row" class="infobox-label"><a href="https://github.com/eduardovazquezespin/" title="Video game developer">Developer</a></th>
<td class="infobox-data">Eduardo VE</td></tr>
<tr><th scope="row" class="infobox-label">Genre</th>
<td class="infobox-data">Roguelike<br/>Bullet Hell</td></tr>
<tr><th scope="row" class="infobox-label">User Skills</th>
<td class="infobox-data">Dexterity<br/>Reaction time<br/>Physics Intuition</td>
</tr><tr><th scope="row" class="infobox-label">User Enjoyment</th>
<td class="infobox-data">Overcoming difficulty<br/>Exploration of Mechanics</td></tr>
<tr><th scope="row" class="infobox-label">Theme</th><td class="infobox-data">Sci-fi<br/>Action</td></tr>
<tr><th scope="row" class="infobox-label">Style</th><td class="infobox-data">Pixel Art</td></tr>
<tr><th scope="row" class="infobox-label">Sequentality</th><td class="infobox-data">Real Time Action Game</td></tr>
<tr><th scope="row" class="infobox-label">Number of Players</th><td class="infobox-data">Single Player</td></tr></tbody></table>

## Table Of Contents

- [Controls](#controls)
- [Introduction](#introduction)
	- [Weapons](#weapons)
	- [Skills](#skills)
	- [Planets](#planets)
- [Scriptable Objects Structure](#scriptable-object-structure)
	- [AI](#ai)
	- [Scene Data](#scene-data)
	- [Ordered Scenes Collector](#ordered-scenes-collector)

## Controls

Beware that the new Input System of Unity is sometimes faulty and needs a reboot to work properly. In case of failure close and open Unity.

<table style="	border-collapse: collapse;
    font-family: Tahoma, Geneva, sans-serif;">
	<tbody>
		<tr>
			<td style="background-color: #54585d;
	color: #ffffff;
	font-weight: bold;
	font-size: 13px;
	border: 1px solid #54585d;">Aim</td>
			<td>Mouse</td>
			<td>Right Joystick</td>
		</tr>
		<tr>
			<td style="background-color: #54585d;
	color: #ffffff;
	font-weight: bold;
	font-size: 13px;
	border: 1px solid #54585d;">Accelerate</td>
			<td>Up Arrow<br/>W key</td>
			<td>Left Joystick Up<br/>D-pad Up</td>
		</tr>
		<tr>
			<td style="background-color: #54585d;
	color: #ffffff;
	font-weight: bold;
	font-size: 13px;
	border: 1px solid #54585d;">Decelerate</td>
			<td>Down Arrow<br/>S key</td>
			<td>Left Joystick Down<br/>D-pad Down</td>
		</tr>
		<tr>
			<td		style="background-color: #54585d;
	color: #ffffff;
	font-weight: bold;
	font-size: 13px;
	border: 1px solid #54585d;">Fire/Attack</td>
			<td>Mouse Left Button</td>
			<td>Gamepad South Button<br/>Right Shoulder</td>
		</tr>
		<tr>
			<td		style="background-color: #54585d;
	color: #ffffff;
	font-weight: bold;
	font-size: 13px;
	border: 1px solid #54585d;">Use Skill</td>
			<td>Spacebar</td>
			<td>Gamepad East Button<br/>Left Shoulder</td>
		</tr>
		<tr>
			<td		style="background-color: #54585d;
	color: #ffffff;
	font-weight: bold;
	font-size: 13px;
	border: 1px solid #54585d;">Change Weapon</td>
			<td>Mouse Scroll<br/>C key<br/>X key</td>
			<td>Right/Left Trigger</td>
		</tr>
		<tr>
			<td		style="background-color: #54585d;
	color: #ffffff;
	font-weight: bold;
	font-size: 13px;
	border: 1px solid #54585d;">Toggle Pause</td>
			<td>P key</td>
			<td>Start Button</td>
		</tr>
	</tbody>
</table>

## Introduction

You decide to skip school to navigate space with your new spaceship. But oh noes! It seems you have been caught by the evil army of robots! You dislike it but have no choice but to fight! 

### Weapons

The weapon you will start with does barely any damage but you'll be able to upgrade it by obtaining new options:

<table style="border: none;
    font-family: Tahoma, Geneva, sans-serif;">
	<tbody>
		<tr>
			<td><img alt="Base Weapon" src="./Assets/Sprites/WeaponIcons/icons-14.png" style="width: 3rem; height: 3rem;"/></td>
			<td>Basic Weapon</td>
			<td>Energy Weapon</td>
			<td>Starting Weapon for the Player</td>
		</tr>
		<tr>
			<td><img alt="Basic Weapon+" src="./Assets/Sprites/WeaponIcons/icons-50.png" style="width: 3rem; height: 3rem;"/></td>
			<td>Basic Weapon+</td>
			<td>Energy Weapon</td>
			<td>Advanced version of the Basic</td>
		</tr>
		<tr>
			<td><img alt="Machine Gun" src="./Assets/Sprites/WeaponIcons/icons-91.png" style="width: 3rem; height: 3rem;"/></td>
			<td>Machine Gun</td>
			<td>Energy Weapon</td>
			<td>Alternativa Energy Weapon</td>
		</tr>
		<tr>
			<td><img alt="Basic Firearm" src="./Assets/Sprites/WeaponIcons/icons-132.png" style="width: 3rem; height: 3rem;"/></td>
			<td>Basic Firearm</td>
			<td>Firearm</td>
			<td>The only weapon with Autofire</td>
		</tr>
		<tr>
			<td><img alt="Missile" src="./Assets/Sprites/WeaponIcons/icons-150.png" style="width: 3rem; height: 3rem;"/></td>
			<td>Missile</td>
			<td>Firearm</td>
			<td>Shoots seeking missiles</td>
		</tr>
		<tr>
			<td><img alt="Orb" src="./Assets/Sprites/WeaponIcons/icons-74.png" style="width: 3rem; height: 3rem;"/></td>
			<td>Orbiting orbs</td>
			<td>Melee Weapon</td>
			<td>4 orbs orbit the player<br/>They can be thrown</td>
		</tr>
		<tr>
			<td><img alt="Broken" src="./Assets/Sprites/WeaponIcons/icons-78.png" style="width: 3rem; height: 3rem;"/></td>
			<td>Test-only</td>
			<td>Energy Weapon</td>
			<td>One shots every enemy<br/>For testing purposes only</td>
		</tr>
	</tbody>
</table>

This last weapon can only be used if you are a CHEATER and input that as your name in the Menu Screen.

Take into account that while Energy Weapons don't use ammunition, they will use energy. This resources is shared with:

### Skills

The player will also be granted the option of using skills that consume great amounts of energy in exchange of significant powers.

<table style="border: none;
    font-family: Tahoma, Geneva, sans-serif;">
	<tbody>
		<tr>
			<td><img alt="Base Dash" src="./Assets/Sprites/WeaponIcons/icons-102.png" style="width: 3rem; height: 3rem;"/></td>
			<td>Basic Dash</td>
			<td>Starting Skill for the Player<br/>Grants you Intangibility and fast movement<br/> by clipping into the 4th dimension</td>
		</tr>
		<tr>
			<td><img alt="Base Dash+" src="./Assets/Sprites/WeaponIcons/ExtraDash.png" style="width: 3rem; height: 3rem;"/></td>
			<td>Basic Dash+</td>
			<td>Improved version of the basic dash<br/>Moves faster and hurts any enemy traspassed</td>
		</tr>
		<tr>
			<td><img alt="Shield" src="./Assets/Sprites/WeaponIcons/icons-21.png" style="width: 3rem; height: 3rem;"/></td>
			<td>Shield</td>
			<td>Alternative skill that protects the user from projectiles<br/>Consumes energy by time used</td>
		</tr>
	</tbody>
</table>

### Planets

The game is set in space and so planets, moons and other stellar bodies:

<img alt="The Moon" src="Assets/Sprites/Planets/Moon/1.png" style="width: 30rem; height: 30rem;"/>

As much as it may surprise you, crashing into them will inevitably result in instant death to anyone.

## Scriptable Object Structure

There are the following categories of elements that can be constructed from the unity editor using scriptable objects (in parenthesis the GO which directly reads these properties):

- Enemies
	- Shooter (_shooterDisplayer_)
	- Exploder (_exploder_)
- Meteorites (_bigMeteoritedisplayer_ or _smallMeteoriteDisplayer_, depending on sprite and hitbox)
- Planets (_planetDisplayer_)
- Weapons
	- Ranged Weapons
		- Energy Weapons
		- Firearm Weapons
	- Melee Weapons
		- Orb Data (_orb_)
- Skills
- Loot (_lootDisplayer_)
	- Healthkit
	- Weapon
	- Skill
- AI
	- Movement AI (controlled by _ShipController_)
	- Shooting AI (controlled by _ShootingController_)
	- Skill Using AI (controlled by _ISkillUser_)
- Scene Data
	- Timed Scene (_timedSceneDisplayer_)
	- Tutorial (_tutorialDisplayer_)
	- Wave
		- ScriptedWave (_scriptedWaveController_)
		- RandomWave (_randomWaveController_)
- Ordered Scenes Collector (_GameManager_)

As for the most basic types are simple enough, no further explanation will be given. For the most complex ones here's their internal workings:

### AI

Movement AI are a function that calculates the orientation of the ship and if it should accelerate or not. For enemies there are the following basic categories:

- Staying away from planets
	- If an ship gets too close to a planet it looks away from it and accelerates full force
- Prevent crashing into a planet
	- If a ship is going straight ahead to a planet and is going to crash in the immediate future, it looks away from it and accelerates
- Following the Player
	- The ship looks in the direction of the player. If it is further  away than a set distance (which can be 0 for exploders) it accelerates. If it is closer than said distance it decelerates.
- Following the Player Advanced
	- Same as the previous one but now it uses first order approximation for better behaviour

![Shooters following the player](Assets/Sprites/Example1.png)

Shooting AI is much more basic. Right now there is only one enemy AI which is always shooting. If weapons with inherent knockback where implemented it would be interesting to do a more strategic use of the shooting weapon.

Finally Using Skills is reserved for the player right now. But an enemy ship that uses skills could potentially be implemented, and it would need strategic use of their energy.

### Scene Data

The level is divided in **scenes** (which is in conflict with Unity scenes nomenclature, yes. sue me.). Each can be of one of the following types:

- Tutorial Scene
	- Has some in world condition to end
- Timed Scene
	- It does some action and finishes in a set amount of time
- Wave Scene
	- Scripted Wave Scene
		- Enemies will spawn in an scripted way. The amount of them will be fixed, but their order can be randomized (or not).
	- Random Wave Scene
		- Enemies are spawned from a list. Each enemy has a probability and value, and a maximum amount of enemy value is allowed concurrently on screen. Once a total value of enemies has spawned (and dealt with), the scene is over.

Each scene has some kind of title (sentence that appears on the chat) and a behaviour. The Wave scenes will spawn enemies from a list, while the timed and tutorial scenes will manage other kind of transitions. These scenes are grouped in one Scriptable Object:

### Ordered Scenes Collector

I'm terrible with names but you get the idea. It contains an ordered lists of scenes, and defines the general structure of the level. A final scene must always be added for the game to end in Victory.

![Spoilers](Assets/Sprites/Spoilers.png)